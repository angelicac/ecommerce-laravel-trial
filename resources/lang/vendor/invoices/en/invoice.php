<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Invoice Language Lines
    |--------------------------------------------------------------------------
    */

    'invoice'                => 'Invoice',
    'serial'                 => 'Order No.',
    'date'                   => 'Invoice date',
    'seller'                 => 'Seller',
    'buyer'                  => 'Buyer',
    'address'                => 'Address',
    'code'                   => 'Code',
    'vat'                    => 'VAT code',
    'phone'                  => 'Phone',
    'description'            => 'Description',
    'units'                  => 'Units',
    'quantity'               => 'Qty',
    'price'                  => 'Price',
    'discount'               => 'Discount',
    'tax'                    => 'Tax',
    'sub_total'              => 'Sub total',
    'total_discount'         => 'Misc discount',
    'coupon_discount'        => 'Coupon discount',
    'taxable_amount'         => 'Taxable amount',
    'total_taxes'            => 'Total taxes',
    'tax_rate'               => 'Tax rate',
    'original_total'         => 'Total',
    'total_amount'           => 'Grand Total',
    'pay_until'              => 'Please pay until',
    'amount_in_words'        => 'Amount in words',
    'amount_in_words_format' => '%s %s and %s %s',
    'notes'                  => 'Notes',
    'shipping'               => 'Shipping',
    'paid'                   => 'Paid',
    'due'                    => 'Due',
];
