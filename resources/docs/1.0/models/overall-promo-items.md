# Overall Promo Items

Promos that are automatically applied.

## Fields

### id

Unique identifier.

### sku

Sku of the [ItemVariant](/{{route}}/{{version}}/models/item-variants)

### price

Promo price

## Methods

### get_item_active_promo($sku)

Returns an instance of `OverallPromoItem` given the sku. It will only return an instance that is still active, i.e. the `start_date` is before the current date and the `end_date` is after the current date. It will only return the first instance, so if there are overlapping promos, there is no guarantee of what will be returned.