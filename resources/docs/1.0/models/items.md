# Items

## Fields

### id `unsigned big integer`

Unique identifier.

### name

Name of the item. Unique.

### description

Description.

### brand_id

Brand id. References [Brands](/{{route}}/{{version}}/models/brands)

### stock

Stock. Currently has no effect on the system.

## Relations

### images

The images of the item. [ItemImage]({{route}}/{{version}}/models/item-images)

### categories

The categories of the item. [Category]({{route}}/{{version}}/models/categories)

### variants

The variants of the item.

## Methods

### metaFields

Retrieves distinct meta fields.

items table:
| ID | Name |
|----|------|
| 1  | Item |

item_variants table:
| sku   | item_id | price |
|-------|---------|-------|
| sku-1 | 1       | 10    |
| sku-2 | 1       | 10    |

item_variant_meta table:

| id | sku   | name  | value |
|----|-------|-------|-------|
| 1  | sku-1 | color | blue  |
| 2  | sku-2 | color | red   |
| 3  | sku-1 | size  | small |
| 4  | sku-2 | size  | big   |

```php
$item = Item::find(1);
$item->metaFields(); // ['color', 'size'];
```


## Properties

### $cachedMetaFields

Saves the output of `metaFields()` so that the database is only queried once.