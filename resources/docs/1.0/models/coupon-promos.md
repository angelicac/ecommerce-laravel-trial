# Coupon Promos

## Fields

### id

Unique identifer

### code

Public unique identifier. This is the code that will be entered to use a promo code.

### type

[CouponPromoType](/{{route}}/{{version}}/models/coupon-promo-type)

### rate_threshold

For `$type === CouponPromoType::REDUCTION`, this is how much the user must spend first to get a discount.

### rate_reduction

For `$type === CouponPromoType::REDUCTION`, this is the discount the user will receive upon meeting the `$rate_threshold`

### start_date

This is the earliest date that the coupon promo can be used.

### end_date

This is the latest date that the coupon promo can be used.

## Relations

### items

A collection of [ItemVariants](/{{route}}/{{version}}/models/item-variants). The pivot table has an extra field `price`.

`coupon_promo_items` table:

| id | sku | coupon_promo_id | price |
|----|-----|-----------------|-------|
| -  | -   | -               | -     |