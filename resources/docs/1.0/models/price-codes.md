# Price Codes

Price Codes are collections of items with lower prices. A user can be assigned with a single Price Code. In the future, it might be worthwhile to allow a user to have multiple Price Codes.

## Fields

### id

Unique identifier

### name

Name of the Price Code

## Relations

### items

A collection of [ItemVariants](/{{route}}/{{version}}/models/item-variants). The pivot table has an extra field `price`.

`price_code_items` table:

| id | sku | price_code_id | price |
|----|-----|---------------|-------|
| -  | -   | -             | -     |