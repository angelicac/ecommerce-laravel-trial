# Item Variants

## Fields

### sku

Unique identifier. User-defined.

### price

Price. Decimal(19, 2)

### item_id

References the original [Item](/{{route}}/{{version}}/models/items)

## Relations

### meta

The meta fields

### item

The origin [Item](/{{route}}/{{version}}/models/items)