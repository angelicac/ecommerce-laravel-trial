# Coupon Promo Type

A coupon promo can be one of the following types:

1. Flat
2. Reduction

For a flat discount, items are specified a flat price to be changed to.

For a reduction discount, the user will receive a discount on minimum spend.

## Fields

### id

Unique identifer

### name

Name of the coupon promo type