# Item Images

The images associated with an [Item](/{{route}}/{{version}}/admin/item)

## Fields

### id

### item_id

### file_name

## Methods

### getFileNameAttribute($value)

Modifies the vlaue of `$file_name` to use the `APP_URL` environment variable and the [PUBLIC_DIRECTORY](#public_directory) property.

```php
<?php
/**
 * ...
 */
class ItemImage extends Model
{
    // ...

    public const PUBLIC_DIRECTORY = 'storage/img/items/';

    // ...

}
```
```php
$item_image = new ItemImage(['file_name' => 'test.png']);
$item_image->file_name; // localhost:8000/storage/img/items/test.png
```

## Properties

### UPLOAD_DIRECTORY

The directory that files will be uploaded/saved to.

<a name="public_directory"></a>
### PUBLIC_DIRECTORY

The publicly accessible directory.