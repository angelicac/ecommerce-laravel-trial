# Packages

This project uses multiple libraries and frameworks

## [Laravel 8](https://laravel.com/docs/8.x)
<img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/4%20PNG/3%20RGB/1%20Full%20Color/laravel-logolockup-rgb-red.png" alt="Laravel Logo" style="height: 50px"/>

Laravel is the main backend framework the project uses.
## [Laravel Dusk](https://laravel.com/docs/8.x)
<img src="https://raw.githubusercontent.com/laravel/dusk/3abf3c47e0bb872f8896a6c2ccb2e4940e016985/art/logo.svg" alt="Laravel Dusk Logo" style="height: 50px"/>

Laravel Dusk is a package that automates browser testing within Laravel.

## [LaRecipe](https://larecipe.binarytorch.com.my/)
<img src="https://larecipe.binarytorch.com.my/images/logo.svg" alt="LaRecipe Logo" style="height: 50px"/>

LaRecipe is the documentation package generating this documentation.

## [Clockwork](https://github.com/itsgoingd/clockwork)
<img src="https://raw.githubusercontent.com/itsgoingd/clockwork/master/.github/assets/title.png" alt="Clockwork Logo" style="height: 50px"/>

Clockwork is a useful tool for debugging.

## [Vue 3](https://v3.vuejs.org/)
<img src="https://vuejs.org/images/logo.png" alt="Vue Logo" style="height: 50px"/>

Vue 3 is the main frontend framework the project uses.

## [PrimeVue](https://www.primefaces.org/primevue/)
<img src="https://www.primefaces.org/primevue/resources/images/primevue-logo.svg" alt="PrimeVue Logo" style="height: 50px"/>

PrimeVue is the component library the admin side uses.

## [Bootstrap 5.1 & Bootstrap Icons](https://getbootstrap.com/docs/5.1/)
<img src="https://getbootstrap.com/docs/5.0/assets/brand/bootstrap-logo.svg" alt="Bootstrap Logo" style="height: 50px"/>

