# Deployment

## x8

The following must be writable by `x8wood`:
1. storage/framework
1. storage/clockwork
1. storage/logs/laravel.log

## Live

The directory `storage/clockwork` does not need to be writable