# Dusk

Browser Testing and Automation.

Running all dusk tests:

```bash
php artisan dusk
```

Creating a new Dusk Test:

```bash
php artisan dusk:make SampleTest
```

The test file will be in `tests/Browser/SampleTest.php`.

Creating a new Unit Test in a subdirectory:

```bash
php artisan dusk:make Folder/SampleTest
```

The test file will be in `tests/Browser/Folder/SampleTest.php`.