# Feature

Testing different parts of the system working together to serve a particular feature.

Running all feature tests:

```bash
php artisan test --testsuite=Feature
```

Creating a new Feature Test:

```bash
php artisan make:test SampleTest
```

The test file will be in `tests/Feature/SampleTest.php`.

Creating a new Unit Test in a subdirectory:

```bash
php artisan make:test Folder/SampleTest
```

The test file will be in `tests/Feature/Folder/SampleTest.php`.