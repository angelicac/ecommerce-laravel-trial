# Unit

Testing individual parts of the system.

Running all unit tests:

```bash
php artisan test --testsuite=Unit
```

Creating a new Unit Test:

```bash
php artisan make:test SampleTest --unit
```

The test file will be in `tests/Unit/SampleTest.php`.

Creating a new Unit Test in a subdirectory:

```bash
php artisan make:test Folder/SampleTest --unit
```

The test file will be in `tests/Unit/Folder/SampleTest.php`.