# Configuration

Required:

| #  | Variable              | Description                                                                                |
|:--:|-----------------------|--------------------------------------------------------------------------------------------|
| 1  | APP_NAME              | Name of the application                                                                    |
| 2  | APP_ENV               | The environment (local, testing, production)                                               |
| 3  | APP_KEY               | The secret key of the app. Generate with `key:generate`                                    |
| 4  | APP_DEBUG             | If debug mode is on or off (true, false)                                                   |
| 5  | APP_URL               | URL of the application.                                                                    |
| 6  | DB_CONNECTION         | What kind of database is used. Typically mysql                                             |
| 7  | DB_HOST               | Host of the database. Typically 127.0.0.1                                                  |
| 8  | DB_PORT               | Port of the database. Typically 3306.                                                      |
| 9  | DB_DATABASE           | Name of the database.                                                                      |
| 10 | DB_USERNAME           | Username of the user to connect to the db                                                  |
| 11 | DB_PASSWORD           | Password of the user to connect to the db                                                  |
| 12 | RECAPTCHA_ENABLED     | If recaptcha is enabled or not. Turning this off would be useful in testing. (true, false) |
| 13 | GRECAPTCHA_SITE_KEY   | The Google Recaptcha Site Key                                                              |
| 14 | GRECAPTCHA_SECRET_KEY | The Google Recaptcha Secret Key                                                            |
| 15 | ADDRESS_API           | Address API url. https://www.x8wood.com/kim.palao/techno-address-api/v1                    |


> {info} Documentation of the `.env` file: [Laravel: Configuration](https://laravel.com/docs/8.x/configuration)
