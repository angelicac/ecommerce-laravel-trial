# Item Page

## Blade Template

By default, generated list blade files will extend `resources/views/layouts/admin/item.blade.php`, which extends `resources/views/layouts/admin.blade.php`. You may create different blade files to extend from, or define the whole view in that file.

## Usage

The Item page is used to interact with the `store` and `update` endpoints of the `AdminApiController`. For example, consider the `Category` model:

```php
/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $name
 * ...
 */
class Category extends Model
{
  ...
}
```

It has the fields `id` and `name`. To add a new `Category`, you would need to send the following payload to `/api/admin/categories` (POST)

```json
{
  "name": "Brand Name"
}
```

The response will be something like:

```json
{
  "id": 1
}
```

To update a `Category`, you would need to send the following payload to `/api/admin/categories/1` (PUT)

```json
{
  "name": "New Brand Name"
}
```

### One to Many relations

To resolve one-to-many relations, the payload must include a `one2m` field. Take the `Item` model for example:

```php
/**
 * App\Models\Item
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $brand_id
 * @property int $stock
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ItemImage[] $images
 * @property-read int|null $images_count
 * ...
 */
class Item extends Model
{
    // ...

    public $one2m = [
        'images'
    ];

    public function images()
    {
        return $this->hasMany(ItemImage::class);
    }

    // ...
}
```

The model must also declare the field `$one2m` that lists the `hasMany` relations that can be automatically resolved.

The payload would then look like:

```json
{
  "name": "Item name",
  "description": "Item description",
  "one2m": {
    "images": {
      "data": [],
      "delete": [],
    }
  }
}
```

Each key of `one2m` is a relation name. The `data` must contain the fields to insert. For example, to insert images:

```json
{
  "name": "Item name",
  "description": "Item description",
  "one2m": {
    "images": {
      "data": [
        {
          "file_name": "file.png"
        },
        {
          "file_name": "file2.png"
        }
      ],
      "delete": [],
    }
  }
}
```

> {info} Note that this would not actually work with how the ItemsApiController is defined.

To remove images:

```json
{
  "name": "Item name",
  "description": "Item description",
  "one2m": {
    "images": {
      "data": [],
      "delete": [1, 2, 3],
    }
  }
}
```

This will delete the `images` with ids 1, 2, and 3 that are under that specific item.

### Many to Many relations

To resolve many-to-many relations, the payload must include a `one2m` field. Take the `CouponPromo` model for example:

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CouponPromo
 *
 * @property int $id
 * ...
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ItemVariant[] $items
 * @property-read int|null $items_count
 * ...
 */
class CouponPromo extends Model
{

    // ...

    public $m2m = [
        'items' => CouponPromoItem::class
    ];

    public function items()
    {
        return $this->belongsToMany(ItemVariant::class, 'coupon_promo_items', 'coupon_promo_id', 'sku', 'id', 'sku')->withPivot('price')->with('item');
    }

    // ...
}
```

The model must also declare the field `$m2m` that lists the `belongsToMany` relations to be automatically resolved. The key is the relation to resolve, and the value can either be `null` or a model class. This is to use the `$fillable` property of the class.

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CouponPromoItem
 *
 * @property int $id
 * @property string $sku
 * @property float $price
 * @property int $coupon_promo_id
 * ...
 */
class CouponPromoItem extends Model
{
    // ...

    public $fillable = ['sku', 'price'];

    // ...
}
```

The payload would then look like:

```json
{
  "name": "Sample Price Code",
  "m2m": {
    "items": [
      {
        "sku": "sku-1",
        "price": 10
      },
      {
        "sku": "sku-2",
        "price": 20
      }
    ]
  }
}
```

Any items not in the payload are removed.

## AdminItemMixin

Defined in `public/js/admin/admin-item-mixin.js`.

### data

#### `id`

#### `entity`

#### `controller`

#### `loading`

#### `submitting`

#### `display_saved`

#### `hide_saved_timeout_id`

#### `form_errors`

#### `one2m`

#### `m2m`

### computed

#### `entity_with_relations`

#### `entity_payload`

#### `item_url`

#### `store_url`

#### `update_url`

### methods

#### `get_data`

#### `after_get_data`

#### `after_update`

#### `after_submit`

#### `verify_entity`

#### `add_to_one2m`

#### `add_to_m2m`

#### `markForDeletion`

#### `isMarkedForDeletion`

#### `submit`

#### `update`

#### `create`
