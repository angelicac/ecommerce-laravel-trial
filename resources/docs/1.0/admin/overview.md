# Overview

Each Admin Controller has at least 4 files associated with it.  For example, take an admin controller named Categories and primarily uses the Category model, the following files would be needed:

1. app/Http/Controllers/Admin/CategoriesController.php
2. app/Http/Controllers/Api/Admin/CategoriesApiController.php
3. resources/views/admin/categories/list.blade.php
4. resources/views/admin/categories/item.blade.php

For easier setup, an artisan command for scaffolding is made available.

```bash
# Initialize an admin controller for Categories
php artisan make:admin-controller Categories
```

This command will create the four needed files.

> {info} In case the files are already present, they will not be overwritten.

## List Page

In `list.blade.php`, a Vue instance must be created with `Vue.createApp()` and assigned to a constant named `App`. It must register at least `AdminListMixin` as a mixin. Using the artisan command will fill the files with code that will help you get started.

Main page: [List](/{{route}}/{{version}}/admin/list)
## Item Page

Main page: [Item](/{{route}}/{{version}}/admin/item)