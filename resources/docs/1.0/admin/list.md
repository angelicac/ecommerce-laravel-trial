# List Page

The admin list page utilizes PrimeVue's [DataTable](https://www.primefaces.org/primevue/showcase/#/datatable)

## Blade Template

By default, generated list blade files will extend `resources/views/layouts/admin/list.blade.php`, which extends `resources/views/layouts/admin.blade.php`. You may create different blade files to extend from, or define the whole view in that file.

## AdminListMixin

Defined in `public/js/admin/admin-list-mixin.js`.

### data

#### `linear_filters`

This computed property converts the filters set by PrimeVue’s DataTable to an object with a depth of 1. This way, this property can be directly passed into axios.

#### `FILTER_DATE_RANGE`

A constant used by `linear_filters`

#### `FILTER_NUMBER_RANGE`

A constant used by linear_filters

#### `columns`

Determines what gets shown in the DataTable. For example, to show the object names:

```js
columns: [{
    label: 'Name',
    field: 'name',
    link: true,
}]
```

#### `entities`

The objects loaded from the backend that should be shown.

#### `loading`

Determines if the DataTable should show a loading state.

#### `filters`

DataTable filters

#### `global_search`

Used for the textbox search

#### `totalRecords`

The total number of records from the backend, after filtering and before pagination.

#### `search`

Search fields for the backend

##### `page`

The page of items to show

##### `rows`

The number of rows to retrieve

##### `sortField`

The field to sort by

##### `sortOrder`

The direction to sort by

### methods

#### `get_data`

