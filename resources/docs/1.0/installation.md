# Installation

---

- [First Section](#section-1)

<a name="section-1"></a>
## Setup

1. Clone from the Technomancer Repository (Updated as of 12th January 2022)

```bash
git clone ssh://firstname.lastname@x8t.x8ting.com/home/x8wood/public_html/ecommerce/.git
```

2. Copy the .env.example file for local and testing (details for configuration in [Configuration](/{{route}}/{{version}}/configuration))

```bash
cp .env.example .env
cp .env.example .env.testing
```

3. Install packages

```bash
composer install
```

4. Migrate

```bash
php artisan migrate
```