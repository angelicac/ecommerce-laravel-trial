@extends('layouts.app')

@section('styles')
<style>
  .product-card-link {
    text-decoration: none;
  }
</style>
@endsection

@section('content')
<div id="intro_gallery" class="carousel slide pointer-event" data-bs-ride="carousel" v-if="gallery.length">
  <!-- v-if="gallery[0]" means that the carousel will only load once the gallery is populated with at least one gallery image -->
  <div class="carousel-indicators">
    <div v-for="(item, i) in gallery">
      <!-- vue for loop to create slide buttons -->
      <button type="button" data-bs-target="#intro_gallery" :data-bs-slide-to="i"
        :class="{ 'active' : i == 0}"></button>
    </div>
  </div>
  <div class="carousel-inner">
    <div class="carousel-item" v-for="(item, i) in gallery" :class="{ 'active' : i == 0}">
      <!-- active class on the first slide (carousel won't work without one active slide) -->
      <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" aria-hidden="true"
        preserveAspectRatio="xMidYMid slice" focusable="false" :style="{ backgroundImage: gallery_image(item) }">

      </svg>

      <div class="container">
        <div class="carousel-caption text-start">
          <h1>@{{ item.headline }}</h1>
          <p>@{{ item.description }}</p>
          <p><a class="btn btn-lg btn-primary" href="#">Sign up today</a></p>
        </div>
      </div>
    </div>

  </div>

  <button class="carousel-control-prev" type="button" data-bs-target="#intro_gallery" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#intro_gallery" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>

<div class="container">
  <div class="row mb-4">
    <div class="col">
      <h1 class="display-1 text-center">Products</h1>
    </div>
  </div>
  <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">

    <div class="product-single col" v-for="(item, i) in items" :key="item.id">
      <a :href="`{{ url('') }}/shop/item/${item.id}`" class="product-card-link">
        <div class="card shadow-sm">
          <div class="product-image ratio ratio-1x1" :style="{backgroundImage: itemBackgroundImage(item)}">
          </div>
          <div class="card-body">
            <div
              v-if="item.final_price < item.price && item.start_date < item.date_today && item.date_today<item.end_date"
              class="type-lb">
              <p class="sale">Sale</p>
            </div>

            <h4 class="pb-2">
              @{{ item.name }}
            </h4>
            <div class="why-text">
              <div class="d-flex flex-column ">
                <div class="d-flex justify-content-between">
                  <p>
                    <span v-if="item.price_min < item.price_max">
                      <ruby>
                        @peso@{{item.price_min}}
                        <rt>starting from</rt>
                      </ruby>
                    </span>
                    <span v-else>
                      @peso@{{ item.price_min }}
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>

  </div>
</div>
@endsection

@section('scripts')
<script type="module" defer src="{{ asset('js/home/index.js') }}"></script>
@endsection