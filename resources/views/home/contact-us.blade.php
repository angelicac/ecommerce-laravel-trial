@extends('layouts.app')

@section('content')

<!-- Start All Title Box -->
<div class="all-title-box back-pst">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2>Contact Us</h2>
      </div>
    </div>
  </div>
</div>
<!-- End All Title Box -->
<!-- Start Contact Us  -->
<div class="contact-box-main">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-sm-12">
        <div class="contact-form-right">
          <h2>GET IN TOUCH</h2>
          <p>We'd love to hear from you! Leave a message below and we will get in touch with you shortly.</p>
          <form @@submit.prevent="submit" novalidate>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Your Name"
                    v-model="formData.name" required />
                  <x-forms.error :field="'name'"></x-forms.error>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="email" placeholder="Your Email" id="email" class="form-control" name="email"
                    v-model="formData.email" required />
                  <x-forms.error :field="'email'"></x-forms.error>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject"
                    v-model="formData.subject" required />
                  <x-forms.error :field="'subject'"></x-forms.error>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <textarea class="form-control" id="message" name="message" placeholder="Your Message" rows="4"
                    v-model="formData.message" required></textarea>
                  <x-forms.error :field="'message'"></x-forms.error>
                </div>
                <div class="submit-button text-center">
                  <button class="btn hvr-hover" type="submit" style="opacity: unset;">Send Message</button>
                  <h3 class="h3 text-center" :class="formFeedbackClasses">@{{ formResponse }}</h3>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="col-lg-4 col-sm-12">
        <div class="contact-info-left">
          <h2>CONTACT INFO</h2>
          <ul>
            <li>
              <a href="https://www.google.com/maps/place/Sct.+Limbaga+St,+Diliman,+Quezon+City,+Metro+Manila"
                target="_blank">
                <p><i class="fas fa-map-marker-alt"></i>59 Samonte Street, Holy Spirit, QC</p>
              </a>
            </li>
            <li>
              <p><i class="fas fa-phone-square"></i>Phone: <a href="tel:+63 2 8861 2688">+63 2 8861 2688</a> |
                <span><a href="tel:+63 2 8236 4854">+63 2 8236 4854</a></span>
              </p>
            </li>
            <li>
              <p><i class="fas fa-envelope"></i>Email: <a
                  href="mailto:inquiries@technomancer.biz">inquiries@technomancer.biz</a>
              </p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script defer src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"></script>
<script type="module" defer src="{{ asset('js/home/contact-us.js') }}"></script>
@endsection