<p>
  From: {{ $name }} <{{ $email }}> <br>
    Subject: {{ $subject }} <br>
</p>

@foreach ($paragraphs as $paragraph)
<p>
  {{ $paragraph }}
</p>
@endforeach