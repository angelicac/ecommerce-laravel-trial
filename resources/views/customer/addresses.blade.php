@extends('layouts.app')

@section('styles')
<link rel="stylesheet"
      href="https://unpkg.com/vue-next-select/dist/index.min.css">
@endsection

@section('content')
<div class="row g-0">
  @include('customer.templates.sidebar')
  <!-- Start Cart  -->
  <div class="col-lg-9 px-4 personal-info">
    <div class="title-left">
    </div>
    <div class="row">
      <div class="col">
        <h3>Addresses</h3>
      </div>
      <div class="col-auto">
        <button class="btn btn-primary"
                @@click="formModalVisible = true"><i class="bi bi-plus"></i> New
          Address</button>
      </div>
    </div>
    <div class="row g-0 address-row"
         v-for="address in addresses">
      <div class="col-sm-12 col-lg-12">
        <table>
          <tr>
            <td>Name</td>
            <td>@{{ address.name }}</td>
          </tr>
          <tr>
            <td>Street</td>
            <td>@{{ address.street }}</td>
          </tr>
          <tr>
            <td>Barangay</td>
            <td>@{{ address.barangay || address.barangay_id }}</td>
          </tr>
          <tr>
            <td>City</td>
            <td>@{{ address.city || address.city_id }}</td>
          </tr>
          <tr>
            <td>Province</td>
            <td>@{{ address.province || address.province_id }}</td>
          </tr>
          <tr>
            <td>Region</td>
            <td>@{{ address.region || address.region_id }}</td>
          </tr>
          <tr>
            <td>Zip Code</td>
            <td>@{{ address.zip_code }}</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>

<p-dialog header="Add Address"
          v-model:visible="formModalVisible"
          :style="{ width: '50vw' }"
          :modal="true">
  <form novalidate
        @submit.prevent="submit"
        id="new_address_form">
    <div class="row">
      <div class="col mb-3">
        <label for="name">Name *</label>
        <input type="text"
               class="form-control"
               id="name"
               name="name"
               required
               :readonly="submitting"
               v-model="formData.name"
               placeholder="Home / Work / etc." />
        <x-forms.error :field="'name'"></x-forms.error>

      </div>
    </div>

    <!-- TODO: Finish section -->
    <div class="row">
      <div class="col mb-3">
        <label for="name">Region *</label>
        <p-dropdown class="w-100"
                    v-model="formData.region_id"
                    :options="regions"
                    option-label="name"
                    option-value="id"
                    :filter="true"
                    placeholder="Select a Region" />
      </div>
    </div>

    <div class="row">
      <div class="col mb-3">
        <label for="name">Province *</label>
        <p-dropdown class="w-100"
                    v-model="formData.province_id"
                    :options="provinces"
                    option-label="name"
                    option-value="id"
                    :filter="true"
                    placeholder="Select a Province"
                    :disabled="!formData.region_id" />
      </div>
    </div>

    <div class="row">
      <div class="col mb-3">
        <label for="name">City *</label>
        <p-dropdown class="w-100"
                    v-model="formData.city_id"
                    :options="cities"
                    option-label="name"
                    option-value="id"
                    :filter="true"
                    @@filter="getCities"
                    placeholder="Select a City"
                    :disabled="!formData.province_id" />
      </div>
    </div>

    <div class="row">
      <div class="col mb-3">
        <label for="name">Barangay *</label>
        <p-dropdown class="w-100"
                    v-model="formData.barangay_id"
                    :options="barangay"
                    option-label="name"
                    option-value="id"
                    :filter="true"
                    @@filter="getBarangay"
                    placeholder="Select a Barangay"
                    :disabled="!formData.city_id" />
      </div>
    </div>

    <div class="col mb-3">
      <label for="street">Street & House Number</label>
      <input type="text"
             class="form-control"
             id="street"
             name="street"
             required
             v-model="formData.street" />
    </div>

    <div class="col mb-3">
      <label for="zip_code">Zip *</label>
      <input type="text"
             class="form-control"
             id="zip_code"
             name="zip_code"
             required
             v-model="formData.zip_code" />
    </div>

    <h3 class="h3 text-center"
        :class="formFeedbackClasses">
      @{{ formResponse }}
    </h3>
  </form>
  <template #footer>

    <button type="button"
            class="btn btn-secondary"
            @@click="formModalVisible = false"
            data-bs-dismiss="modal">Close</button>
    <button type="submit"
            class="btn btn-primary"
            form="new_address_form">Save changes</button>

  </template>
</p-dialog>

@endsection

@section('scripts')
<script defer
        src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"></script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/dialog/dialog.min.js">
</script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/dropdown/dropdown.min.js">
</script>
<script type="module"
        src="{{ asset('js/customer/addresses.js') }}"></script>
@endsection