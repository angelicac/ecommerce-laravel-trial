@extends('layouts.app')

@section('styles')
<link rel="stylesheet"
      href="https://unpkg.com/vue-next-select/dist/index.min.css">
@endsection

@section('content')
<div class="row g-0">
  @include('customer.templates.sidebar')
  <!-- Start Cart  -->
  <div class="col-lg-9 px-4 personal-info">
    <div class="title-left">
      <h3><i class="fas fa-user-edit"></i> Change Password</h3>
    </div>
    <div class="row g-0">
      <div class="col-sm-12 col-lg-12">
        <form class="needs-validation"
              novalidate
              id="account_form"
              @@submit.prevent="submit">
          <div class="checkout-address">
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="password">Password *</label>
                <input :type="showPassword ? 'text' : 'password'"
                       class="form-control"
                       id="password"
                       name="password"
                       required
                       :readonly="submitting"
                       v-model="formData.password"
                       autocomplete="new-password">
                <x-forms.error :field="'password'"></x-forms.error>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="password_conf">Password Confirmation *</label>
                <input :type="showPassword ? 'text' : 'password'"
                       class="form-control"
                       id="password_conf"
                       name="password_conf"
                       required
                       :readonly="submitting"
                       v-model="formData.password_conf"
                       autocomplete="new-password">
                <x-forms.error :field="'password_conf'"></x-forms.error>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 mb-3">
                <div class="custom-control spantxt custom-checkbox">
                  <input type="checkbox"
                         class="custom-control-input"
                         id="show_password"
                         required
                         name="show_password"
                         v-model="showPassword">
                  <label class="custom-control-label"
                         for="show_password">Show Password</label>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="current_password">Enter your current password to confirm</label>
                <input type="password"
                       class="form-control"
                       id="current_password"
                       name="current_password"
                       required
                       :readonly="submitting"
                       v-model="formData.current_password"
                       autocomplete="current-password">
                <x-forms.error :field="'current_password'"></x-forms.error>
              </div>
            </div>


            <hr class="mb-4">

            <div class="row g-0 justify-content-center">
              <div class="col-auto mb-4">
                <div class="shopping-box"
                     style="text-align: center;">
                  <button type="submit"
                          href="#"
                          class="ml-auto btn btn-primary hvr-hover">Edit Account</button>
                </div>
              </div>
            </div>

            <div class="col-md-12 col-lg-12">
              <div class="h3 text-center"
                   :class="formFeedbackClasses">@{{ formResponse }}</div>
            </div>

          </div>
        </form>
      </div>
    </div>
  </div>
</div>


@endsection

@section('scripts')
<script defer
        src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"></script>
<script src="https://unpkg.com/vue-next-select/dist/vue-next-select.iife.prod.js"></script>
<script type="module">
  import cartStore from "/js/store/cartStore.js";
  import { formVerify, formValid, genders, addError } from "/js/modules/useForm.js";
  const { createApp, ref, onMounted, computed } = Vue;
  window.app = createApp({
    setup() {

      const exports = {
        cartState: cartStore
      };

      const formData = ref({
        password: '',
        password_conf: '',
        current_password: '',
      });
      const formErrors = ref({});
      const formRules = ref({
        password: [{
          type: 'required'
        }],
        password_conf: [{
          type: 'required'
        }],
        current_password: [{
          type: 'required'
        }],
      });

      const showPassword = ref(false);
      const formResponse = ref('');
      const formSuccess = ref(false);
      const submitting = ref(false);

      const formFeedbackClasses = computed(() => {
        formSuccess.value ? 'text-success' : 'text-danger';
      });

      const submit = async event => {
        formVerify(formData, formRules, formErrors);
        if (formData.value.password !== formData.value.password_conf)
          addError(formErrors, 'password_conf', 'Passwords do not match');
        if (!formValid(formErrors)) return;
        try {
          const response = await axios.post('/customer/change-password', formData.value);
          formResponse.value = response.data.data;
          formSuccess.value = true;
          for (let key in formData.value) {
            formData.value[key] = '';
          }
        } catch (e) {
          console.log(e);
          if (e.response) {
            formErrors.value = e.response.data.errors;
            formSuccess.value = false;
          }
        }
      }

      Object.assign(exports, {
        submit,
        showPassword,
        submitting,
        formData,
        formErrors,
        formResponse,
        formFeedbackClasses,
        submit,
      });

      return exports;

    },
  });
</script>
@endsection