<div class="col-lg-3 px-4 mb-md-4">
  <h2>My Account</h2>
  <div class="filter-sidebar-left">
    <div class="list-group list-group-collapse list-group-sm list-group-tree" id="list-group-men"
      data-children=".sub-men">
      <a href="{{ route('customer.index') }}" class="list-group-item list-group-item-action">
        Personal Information
      </a>
      <a href="{{ url('customer/change-password') }}" class="list-group-item list-group-item-action">
        Change Password
      </a>
      <a href="{{ url('customer/addresses') }}" class="list-group-item list-group-item-action">
        Saved Addresses
      </a>
      <a href="{{ url('customer/orders') }}" class="list-group-item list-group-item-action">
        Past Orders
      </a>
      @if (Auth::user()->price_code_id)
      <a href="{{ url('customer/price-code') }}" class="list-group-item list-group-item-action">
        Price Code
      </a>
      @endif
    </div>
  </div>
</div>