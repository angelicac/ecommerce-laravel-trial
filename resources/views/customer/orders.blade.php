@extends('layouts.app')

@section('content')
<div class="row g-0">
  @include('customer.templates.sidebar')
  <div class="col-lg-9 px-4 cart-box-main personal-info">
    <div class="title-left">
      <h3>Past Orders</h3>
    </div>
    <div class="row" v-for="order in orders" :key="order.id">
      <p>
        <a :href="`{{ url('') }}/order/track/${order.number}`">
          Order #@{{order.number}}
        </a>
      </p>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  const app = Vue.createApp({
  data() {
    return {
      orders: [],
    };
  },
  methods: {
    async get() {
      try {
        const response = await axios.get('/orders');
        this.orders = response.data.data;
      } catch (e) {
        console.log(e);
      }
    }
  },
  mounted() {
    this.get();
  }
});
</script>
@endsection