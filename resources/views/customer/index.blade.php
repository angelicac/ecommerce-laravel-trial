@extends('layouts.app')

@section('styles')
<link rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css">
@endsection

@section('content')

<div class="row g-0">
  @include('customer.templates.sidebar')
  <!-- Start Cart  -->
  <div class="col-lg-9 px-4 cart-box-main personal-info">
    <div class="title-left">
      <h3><i class="fas fa-user-edit"></i> Personal Information</h3>
    </div>
    <div class="row g-0">
      <div class="col-sm-12 col-lg-12">
        <form class="needs-validation"
              novalidate
              id="account_form"
              @submit.prevent="submit">
          <div class="checkout-address">
            <div class="row g-0">
              <div class="col-md-6 mb-3">
                <label for="firstName">First name *</label>
                <input type="text"
                       class="form-control"
                       id="first_name"
                       name="first_name"
                       required
                       :readonly="!editing"
                       v-model="formData.first_name">
                <x-forms.error :field="'first_name'"></x-forms.error>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Last name *</label>
                <input type="text"
                       class="form-control"
                       id="last_name"
                       name="last_name"
                       required
                       :readonly="!editing"
                       v-model="formData.last_name">
                <x-forms.error :field="'last_name'"></x-forms.error>
              </div>
            </div>
            <div class="row g-0">
              <div class="col-md-6 mb-3">
                <label for="email">Email Address *</label>
                <input type="email"
                       class="form-control"
                       id="email"
                       name="email"
                       required
                       :readonly="!editing"
                       v-model="formData.email">
                <x-forms.error :field="'email'"></x-forms.error>
              </div>
              <div class="col-md-6 mb-3">
                <label for="address2">Contact No. *</label>
                <input type="text"
                       class="form-control"
                       id="contact_number"
                       name="contact_number"
                       placeholder=""
                       required
                       :readonly="!editing"
                       v-model="formData.contact_number">
                <x-forms.error :field="'contact_number'"></x-forms.error>
              </div>
            </div>
            <div class="row g-0">
              <div class="col-md-6 mb-3">
                <label for="birth_date">Birthdate</label>
                <input type="date"
                       class="form-control"
                       id="birth_date"
                       name="birth_date"
                       required
                       :readonly="!editing"
                       v-model="formData.birth_date">
                <x-forms.error :field="'birth_date'"></x-forms.error>
              </div>
              <div class="col-md-6 mb-3">
                <label for="state">Gender</label>
                <select class="wide w-100 form-control"
                        id="gender"
                        name="gender"
                        :disabled="!editing"
                        v-model="formData.gender">
                  <option disabled
                          value="">Please select one</option>
                  <option v-for="gender in genders"
                          :value="gender"
                          :selected="gender === formData.gender">@{{ gender
                    }}
                  </option>
                </select>
                <x-forms.error :field="'gender'"></x-forms.error>
              </div>
            </div>
            <hr class="mb-4">
            <div class="row g-0"
                 v-if="!editing">
              <div class="mb-4">
                <div class="shopping-box"
                     style="text-align: center;">
                  <button type="button"
                          @@click="editing = true"
                          class="ml-auto btn btn-primary hvr-hover">Edit
                    Account</button>
                </div>
              </div>
            </div>
            <div class="row g-0 justify-content-center"
                 v-else>
              <div class="col-auto mb-4">
                <div class="shopping-box"
                     style="text-align: center;">
                  <button type="button"
                          @@click="cancel"
                          class="ml-auto btn btn-primary hvr-hover">Cancel</button>
                </div>
              </div>
              <div class="col-auto mb-4">
                <div class="shopping-box"
                     style="text-align: center;">
                  <button type="submit"
                          href="#"
                          class="ml-auto btn btn-primary hvr-hover">Edit Account</button>
                </div>
              </div>
            </div>

            <div class="col-md-12 col-lg-12">
              <div class="alert alert-danger alert-dismissible fade show d-none"
                   role="alert"
                   id="error_alert">
                <span class="alert-message"></span>
                <button type="button"
                        class="close"
                        data-closes="alert_error"
                        aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            </div>

          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script defer
        src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"></script>
<script type="module">
  import cartStore from "/js/store/cartStore.js";
  import { formVerify, formValid, genders } from "/js/modules/useForm.js";
  const { createApp, ref, onMounted } = Vue;
  window.app = createApp({
    setup() {
      const exports = {
        cartState: cartStore
      }
      const editing = ref(false);
      const originalData = ref({});
      const formData = ref({});
      const formErrors = ref({});
      const formRules = ref({
        'first_name': [{ type: 'required' }],
        'last_name': [{ type: 'required' }],
        'email': [{ type: 'required' }],
        'contact_number': [{ type: 'required' }],
        'birth_date': [{ type: 'required' }],
        'gender': [{ type: 'required' }],
      });

      const get = async () => {
        try {
          const response = await axios.get('user');
          originalData.value = response.data;
          const fields = ['first_name', 'last_name', 'email', 'contact_number', 'gender'];
          for (let field of fields)
            formData.value[field] = originalData.value[field];
          formData.value.birth_date = originalData.value.birth_date;
        } catch (e) {
          console.log(e);
        }
      };

      const submit = async() => {
        formVerify(formData, formRules, formErrors);
        if (!formValid(formErrors)) return;
        try {
          const response = await axios.post('customer/update', formData.value);
          get();
          editing.value = false;
        } catch (e) {
          console.log(e);
        }
      };

      const cancel = () => {
        editing.value = false;
        const fields = ['first_name', 'last_name', 'email', 'contact', 'birth_date', 'gender'];
        for (let field of fields)
          formData.value[field] = originalData.value[field];
      };

      onMounted(get);

      Object.assign(exports, {
        submit,
        cancel,
        editing,
        originalData,
        formData,
        formErrors,
        genders
      });

      return exports;
    },
});
</script>
<script defer
        src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"></script>
@endsection