@extends('layouts.app')


@section('styles')
<link rel="stylesheet"
      href="https://unpkg.com/vue-next-select/dist/index.min.css">
<style>
  .address-box {
    font-size: 10px;
    border: 1px solid black;
    border-radius: 5px;
    padding: 5px;
    background: #fff;
    cursor: pointer;
  }

  .address-box.disabled {
    cursor: initial;
  }

  .address-name {
    font-weight: bold;
  }

  .vue-select {
    width: 100%;
  }

  .form-errors {
    font-size: 12px;
  }
</style>
@endsection

@section('content')

<!-- Start All Title Box -->
<div class="all-title-box back-pst"
     style=" background-image: url(https://www.x8wood.com/quantagoods.com/assets/images/shop.jpg?v=2);">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2>Checkout</h2>
      </div>
    </div>
  </div>
</div>
<!-- End All Title Box -->
<!-- Start Cart  -->
<div class="cart-box-main">
  <div class="container">
    <form class="needs-validation"
          novalidate
          id="checkup_form"
          @@submit.prevent="checkout">
      <div class="row">
        <div class="col-sm-8 col-lg-8 col-md-12 mb-3">
          <div class="checkout-address">
            <div class="title-left">
              <h4>Personal Information</h4>
            </div>
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="first_name">First name *</label>
                <input type="text"
                       class="form-control"
                       id="first_name"
                       name="first_name"
                       required
                       v-model="formData.first_name">
                <x-forms.error :field="'first_name'"></x-forms.error>
              </div>
              <div class="col-md-6 mb-3">
                <label for="last_name">Last name *</label>
                <input type="text"
                       class="form-control"
                       id="last_name"
                       name="last_name"
                       required
                       v-model="formData.last_name">
                <x-forms.error :field="'last_name'"></x-forms.error>
              </div>
            </div>
            <div class="mb-3">
              <label for="email">Email Address *</label>
              <input type="email"
                     class="form-control"
                     id="email"
                     name="email"
                     required
                     v-model="formData.email">
              <x-forms.error :field="'email'"></x-forms.error>
            </div>
            <div class="mb-3">
              <label for="address2">Contact No. *</label>
              <input type="text"
                     class="form-control"
                     id="contact_number"
                     name="contact_number"
                     placeholder="09xxxxxxxxx"
                     v-model="formData.contact_number"
                     required>
              <x-forms.error :field="'contact_number'"></x-forms.error>
            </div>

            <template v-if="addresses.length">
              <div class="row">
                <label for="">
                  Saved Addresses:
                </label>
              </div>
              <div class="row g-0 mb-3">
                <div class="col-md-2 me-4 address-box"
                     :class="{disabled: !addressesResolved}"
                     v-for="(address, index) in addresses"
                     @@click="setAddress(index)"
                     :id="`address-button-${index}`">
                  <span class="address-name"> @{{ address.name }} </span><br />
                  @{{ address.street }} @{{ address.barangay || address.barangay_id }} @{{ address.city ||
                  address.city_id
                  }} @{{ address.province || address.province_id }} @{{ address.region || address.region_id }}
                </div>
              </div>
            </template>

            <div class="row">
              <div class="col mb-3">
                <label for="address">House / Unit Number and Street *</label>
                <input type="text"
                       class="form-control"
                       id="street"
                       name="street"
                       placeholder=""
                       required
                       v-model="formData.street">
                <x-forms.error :field="'street'"></x-forms.error>
              </div>

            </div>

            {{-- Address --}}

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="region">Region *</label>
                <p-dropdown class="w-100"
                            v-model="formData.region_id"
                            :options="regions"
                            option-label="name"
                            option-value="id"
                            :filter="true"
                            placeholder="Select a Region" />
                <x-forms.error :field="'region_id'"></x-forms.error>
              </div>
              <div class="col-md-6 mb-3">
                <label for="province">Province *</label>
                <p-dropdown v-model="formData.province_id"
                            class="w-100"
                            :options="provinces"
                            option-label="name"
                            option-value="id"
                            :filter="true"
                            :disabled="!formData.region_id"
                            placeholder="Select a Province" />
                <x-forms.error :field="'province_id'"></x-forms.error>
              </div>
            </div>

            <div class="row">
              <div class="col-md-5 mb-3">
                <label for="city">City *</label>
                <p-dropdown class="w-100"
                            v-model="formData.city_id"
                            :options="cities"
                            option-label="name"
                            option-value="id"
                            :filter="true"
                            @filter="getCities"
                            :loading="loading"
                            :disabled="!formData.province_id"
                            placeholder="Select a City" />
                <x-forms.error :field="'city_id'"></x-forms.error>
              </div>
              <div class="col-md-4 mb-3">
                <label for="state">Barangay *</label>
                <p-dropdown class="w-100"
                            v-model="formData.barangay_id"
                            :options="barangay"
                            option-label="name"
                            option-value="id"
                            :filter="true"
                            @filter="getBarangay"
                            :disabled="!formData.city_id"
                            placeholder="Select a Barangay" />
                <x-forms.error :field="'barangay_id'"></x-forms.error>
              </div>
              <div class="col-md-3 mb-3">
                <label for="zip_code">Zip *</label>
                <input type="text"
                       class="form-control"
                       id="zip_code"
                       name="zip_code"
                       required
                       v-model="formData.zip_code">
                <x-forms.error :field="'zip_code'"></x-forms.error>
              </div>
            </div>


            <hr class="mb-4" />
            <h4>Shipping Method</h4>
            <h5 v-if="!addressFilled"
                class="text-center">
              Please fill out your address
            </h5>
            <template v-else>
              <div class="form-check shipping-method-radio my-2"
                   v-for="(shippingMethod, index) in shippingMethods"
                   :key="`shipping-method-${index}`">
                <input class="form-check-input"
                       type="radio"
                       name="shipping_method"
                       v-model="formData.shipping_fee"
                       :value="shippingMethod.id"
                       :id="`shipping-method-${index}`"
                       :disabled="isShippingMethodDisabled(shippingMethod)" />
                <label class="form-check-label"
                       :for="`shipping-method-${index}`">
                  @{{ shippingMethod.method.name }} <br />
                  @{{ shippingMethodText(shippingMethod) }}
                </label>
              </div>
            </template>
          </div>
        </div>
        <div class="col-sm-4 col-lg-4 col-md-12 mb-3">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="shipping-method-box">
                <div class="title-left">
                  <h4>Delivery Instructions (if any):</h4>
                </div>
                <div class="mb-4">
                  <textarea name="delivery_instructions"
                            id="delivery_instructions"
                            name="delivery_instructions"
                            cols="30"
                            rows="2"
                            class="form-control"
                            placeholder="Message"
                            spellcheck="false"
                            v-model="formData.delivery_instructions">
                  </textarea>
                  <x-forms.error :field="'delivery_instructions'"></x-forms.error>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="mt-2">
                <table class="table price-table">
                  <tbody>
                    <tr>
                      <td>Subtotal</td>
                      <td> @{{ formatPrice(cartState.originalTotal) }} </td>
                    </tr>
                    <tr>
                      <td>Discount</td>
                      <td>
                        @{{ formatPrice(cartState.discount) }}
                      </td>
                    </tr>
                    <tr>
                      <td>Coupon Discount</td>
                      <td>
                        @{{ formatPrice(cartState.discountTotal) }}
                      </td>
                    </tr>
                    <tr>
                      <td>Shipping Fee</td>
                      <td>
                        @{{ formatPrice(shippingFee) }}
                      </td>
                    </tr>
                    <tr>
                      <td>Grand Total</td>
                      <td>
                        @{{ formatPrice(totalWithShipping) }}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-md-12 col-lg-12">
              <div class="shipping-method-box">
                <div class="mb-4">
                  <div class="d-block my-3">
                    @if (config('app.recaptcha.enabled'))
                    <div class="g-recaptcha d-flex justify-content-center"
                         id="recaptcha">

                    </div>
                    <em style="color:red; display: none;"
                        id="recaptcha_message"
                        class="form-error">Please finish the
                      captcha
                      first</em>
                    @endif

                    <div style="padding-top: 5%;">
                      <div v-if="cartState.cart.length"
                           class="shopping-box d-flex flex-column">
                        <button type="submit"
                                class="mx-auto btn hvr-hover btn-lg">Checkout</button>
                      </div>
                      <div v-else
                           class="shopping-box d-flex flex-column">
                        <button type="button"
                                class="mx-auto btn hvr-hover btn-lg"
                                style="pointer-events: none"
                                disabled>Checkout</button>
                        <div class="text-danger pt-2 mx-auto"> You have no items in your cart. </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- here -->

            <div class="col-md-12 col-lg-12">
              <div class="alert alert-danger alert-dismissible fade show d-none"
                   role="alert">
                <span class="alert-message">
                  @{{ alert_message }}
                </span>
                <button type="button"
                        class="close"
                        data-closes="alert_error"
                        aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

@endsection

@section('scripts')
<script defer
        src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"></script>
@include('templates.recaptcha')
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/dropdown/dropdown.min.js">
</script>
<script>
  const shippingMethodsMap = {{ 
    Js::from($shippingMethods)
   }};
  const SITE_KEY = '{{ config('app.recaptcha.site_key') }}';
</script>
<script type="module"
        src="{{ asset('js/shop/checkout.js') }}">
</script>
@endsection