@extends('layouts.app')

@section('styles')

<style>
  .grid-item {
    display: grid;
    width: 100px;
    grid-template-columns: 100%;
  }

  .grid-item p {
    white-space: normal;
    font-size: 10px;
    text-align: center;
  }

  .price-table {
    table-layout: fixed
  }

  .price-table td:last-child {
    text-align: right;
  }
</style>
@endsection

@section('content')

<!-- Start All Title Box -->
<div class="all-title-box back-pst">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2>My Cart</h2>
      </div>
    </div>
  </div>
</div>
<!-- End All Title Box -->
<!-- Start Cart  -->
<div class="cart-box-main">
  <div class="container" v-if="cartState.cart.length">
    <div class="row">
      <div class="col-md-8">
        <div class="row">
          <div class="col-lg-12">
            <div class="table-main table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Product</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="(item, i) in cartState.cart" :key="item.sku">
                    <td class="thumbnail-img">
                      <a :href="`{{ url('shop/item/${item.item_id}') }}`">
                        <div class="product-image ratio ratio-1x1" :class="{'opacity-50': item.stock === 0}"
                          :style="{backgroundImage: itemBackgroundImageApi(item.item_id)}">
                        </div>
                      </a>
                    </td>
                    <td class="name-pr">
                      <div :class="{'opacity-50': item.stock === 0}">
                        <p>
                          <span class="font-weight-bold">@{{ item.item.name }} (@{{ item.sku }})</span><br>
                          <small v-if="item.meta.length">(@{{variantSummary(item)}})</small>
                        </p>
                        <p>Price:
                          <span v-if="item.final_price < item.price">
                            <del>
                              @{{ formatPrice(item.price) }}
                            </del>
                            @{{ formatPrice(item.final_price) }}
                          </span>
                          <span v-else>
                            @{{ formatPrice(item.final_price) }}
                          </span>
                        </p>
                      </div>
                      <div v-if="item.stock">
                        <div class="quantity-box">
                          <button class="btn btn-light btn-sm sub-quantity-button" @@click="subtract(item, 1)"
                            :disabled="updating">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                              class="bi bi-dash" viewBox="0 0 16 16">
                              <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z" />
                            </svg>
                          </button>
                          <input type="number" size="4" :value="item.pivot.quantity" min="0" :max="item.balance"
                            step="1" class="c-input-text qty text" onkeydown="return false;" :disabled="updating">
                          <button class="btn btn-light btn-sm add-quantity-button" @@click="add(item, 1)"
                            :disabled="updating">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                              class="bi bi-plus" viewBox="0 0 16 16">
                              <path
                                d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                            </svg>
                          </button>
                        </div>
                        <p>
                          Total
                          <span class="text-success mx-2 font-weight-bold">
                            @{{ formatPrice(item.pivot.quantity * item.final_price) }}
                          </span>
                        </p>
                      </div>
                      <div v-else>
                        <p class="text-secondary">Out of stock</p>
                      </div>
                      <div class="alert alert-dismissible fade show" role="alert" v-if="item.pivot.notes">
                        @{{ item.pivot.notes }}
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"
                          @@click="dismiss_notes(item.pivot.sku)"></button>
                      </div>
                      <button type="button" class="btn btn-sm btn-danger hvr-hover my-2"
                        @@click.prevent="remove(item)">Remove <i class="fas fa-times"></i></button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div>
          <form class="coupon-box" @@submit.prevent="applyCoupon">
            <div class="input-group input-group-sm">
              <input class="form-control disable-txt" placeholder="Enter your coupon code" aria-label="Coupon code"
                type="text" name="coupon_code" id="coupon_code" v-model="couponCode">
              <div class="input-group-append">
                <button class="btn btn-theme disable-txt" id="apply_coupon_button" type="submit">Apply Coupon</button>
              </div>
            </div>
          </form>
          <div class="p-2">
            <em v-if="showCouponMessage && couponApplied" class="text-success">Coupon Applied!</em>
            <em v-if="showCouponMessage && !couponApplied" class="text-danger">Invalid Coupon</em>
          </div>
          <p v-if="cartState.coupon && cartState.coupon.message" v-html="cartState.coupon.message">
          </p>
        </div>

        <div class="mt-2">
          <table class="table price-table">
            <tbody>
              <tr>
                <td>Subtotal</td>
                <td> @{{ formatPrice(cartState.originalTotal) }} </td>
              </tr>
              <tr>
                <td>Discount</td>
                <td>
                  @{{ formatPrice(cartState.discount) }}
                </td>
              </tr>
              <tr>
                <td>Coupon Discount</td>
                <td>
                  @{{ formatPrice(cartState.discountTotal) }}
                </td>
              </tr>
              <tr>
                <td>Grand Total</td>
                <td>
                  @{{ formatPrice(cartState.total) }}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="d-flex shopping-box">
          <a href="{{ route('cart.checkout') }}" class="ml-auto btn hvr-hover">
            Checkout
          </a>
        </div>
      </div>
    </div>
  </div>
  <div v-else class="container my-5">
    <div class="row my-5">
      <div class="col text-center">
        <h1>You have no items in your cart right now</h1>
        <a href="{{ route('shop.index') }}" class="btn hvr-hover">Shop Now!</a>

      </div>
    </div>
    <div class="row my-5"></div>
  </div>

</div>
@endsection

@section('scripts')
<script type="module" defer src="{{ asset('js/shop/cart.js') }}"></script>
@endsection