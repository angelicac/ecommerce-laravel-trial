@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="https://unpkg.com/vueperslides/dist/vueperslides.css">
<style>
  .preview-image {
    background-size: cover;
  }

  figure img {
    width: 100%;
  }

  figure div {
    background-position: center center;
    background-size: cover;
    -webkit-transition: background-image 0.2s ease-in-out;
    transition: background-image 0.2s ease-in-out;
  }

  .variant-option {
    background: white;
    border: 2px solid #177bbe;
    color: #177bbe;
    padding: 0px 5px;
    margin-right: 5px;
    cursor: pointer;
  }

  .variant-option.active {
    background: #177bbe;
    color: white;
  }

  rt.original-price {
    text-decoration: line-through;
  }
</style>
@endsection

@section('content')
<div class="container">
  <div class="row my-4">
    <div class="col">
      <a href="{{ route('shop.index') }}">Shop</a> &gt; <a href="{{ route('shop.item', ['id' => $item_id]) }}">@{{
        entity.name }}</a>
    </div>
  </div>
  <div class="row">
    <!-- Product Images -->
    <div class="col-lg-6 col-md-12">
      <div class="row">
        <figure>
          <div class="ratio ratio-1x1 preview-image" :style="{backgroundImage: `url(${currentImage.file_name})`}">
          </div>

        </figure>
      </div>
      <div class="row">
        <vueper-slides class="no-shadow" :visible-slides="3" :gap="3" :slide-ratio="1 / 4" :dragging-distance="200"
          :arrows="false" :breakpoints="{ 800: { visibleSlides: 2, slideMultiple: 2 } }" ref="slides">
          <vueper-slide v-for="(image, i) in entity.images" :key="i" :image="image.file_name"
            @@click="focus_image(i)" />
        </vueper-slides>
      </div>
    </div>
    <!-- Price & Description -->
    <div class="col-lg-6 col-md-12">
      <h3>@{{ entity.name }}</h3>
      <h5 v-if="selectedVariant || entity.price_min === entity.price_max">
        <span v-if="selectedVariant && selectedVariant.price < selectedVariant.original_price">
          <ruby>
            @peso@{{ selectedSku ? selectedVariant.price : entity.price_min }}
            <rt class="original-price">
              @peso@{{ selectedVariant.original_price }}
            </rt>
          </ruby>

        </span>
        <span v-else>
          @peso@{{ selectedSku ? selectedVariant.price : entity.price_min }}
        </span>
      </h5>
      <h5 v-else>
        @peso@{{ entity.price_min }} -
        @peso@{{ entity.price_max }}
      </h5>
      <p>
        @{{entity.description}}
      </p>
      <template v-if="entity.variants && entity.variants.length > 1">
        <small>Select</small>
        <div v-for="(options, field) in variantOptions" class="variant-selection mb-3">
          <h6>@{{ field }}</h6>
          <button class="btn variant-option" :class="{active: selection[field] === option}" v-for="option in options"
            :disabled="!canExist(field, option)" @@click="setSelection(field, option)">@{{ option }}</button>
        </div>
      </template>

      <p>Stock: @{{ selectedVariant ? selectedVariant.remaining_stock : '' }}</p>

      <form class="quantity-box" @@submit.prevent="addToCart">
        <button class="btn btn-light btn-sm" @@click="increment(-1)" type="button">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash"
            viewBox="0 0 16 16">
            <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z" />
          </svg>
        </button>
        <input type="number" size="4" min="0" step="1" onkeydown="return false;" @@keyup.up="increment(1)"
          @@keyup.down="increment(-1)" v-model="quantity">
        <button class="btn btn-light btn-sm" @@click="increment(1)" type="button">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus"
            viewBox="0 0 16 16">
            <path
              d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
          </svg>
        </button>
        <div class="row">
          <div class="col">
            <button class="btn" type="submit" :disabled="!canAddToCart || addingToCart" id="addToCart_button">
              <span v-if="selectedVariant && selectedVariant.stock === 0">
                Out of Stock
              </span>
              <span v-else-if="selectedVariant && selectedVariant.remaining_stock === 0">
                Max quantity reached
              </span>
              <span v-else>
                Add To Cart
              </span>
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<p-dialog header="Cart" v-model:visible="showAddToCartModal" :style="{ width: '50vw' }" modal>
  <h3 class="text-success">Added to Cart!</h3>
</p-dialog>

@endsection

@section('scripts')
<script>
  const ID = {{ $item_id }};
</script>
<script defer src="https://unpkg.com/vueperslides@next"></script>
<script defer src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"></script>
<script defer src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/dialog/dialog.min.js"></script>
<script type="module" defer src="{{ asset('js/shop/item.js') }}"></script>
@endsection