@extends('layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/shop.css') }}">
<link href="https://unpkg.com/primevue@^3/resources/themes/saga-blue/theme.css" rel="stylesheet" />
<link href="https://unpkg.com/primevue@^3/resources/primevue.min.css" rel="stylesheet" />
<link href="https://unpkg.com/primeflex@^3/primeflex.min.css" rel="stylesheet" />
<link href="https://unpkg.com/primeicons/primeicons.css" rel="stylesheet" />
@endsection


@section('content')
<div class="all-title-box back-pst">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2>Shop</h2>
      </div>
    </div>
  </div>
</div>

<div class="shop-box-inner">
  <div class="container">
    <div class="row">
      <div class="col-xl-3 col-lg-3 col-sm-12 col-xs-12 sidebar-shop-left mb-4">

        <div class="filter-sidebar-left">
          <div class="title-left d-flex justify-content-between pb-2">
            <h3 class="my-auto pb-0">Categories</h3>
            <i class="bi bi-x category-clear-button" @@click.prevent="selectedCategory = 0" v-if="selectedCategory"></i>
          </div>

          <p-tree :value="categories" :expanded-keys="expandedCategories" selection-mode="single"
            v-model:selection-keys="selectedKeys" @@node-select="selectCategory" @@node-expand="expandCategory"
            :loading="categoriesLoading">
          </p-tree>

        </div>
        <div class="filter-price-left" style="display: none;">
          <div class="title-left">
            <h3>Price</h3>
          </div>
          <div class="price-box-slider">
            <div id="slider-range"></div>
            <p>
              <input type="text" id="amount" readonly style="border:0; color:#fbb714; font-weight:bold;">
              <button class="btn hvr-hover" type="submit">Filter</button>
            </p>
          </div>
        </div>
      </div>
      <div class="col-xl-9 col-lg-9 col-sm-12 col-xs-12 p-4 shop-content-right">
        <transition name="fade">
          <div class="shop-loader" v-if="loading">
            <div class="spinner-border text-light" role="status">
              <span class="visually-hidden">Loading...</span>
            </div>
          </div>
        </transition>

        <div class="search-product row g-0" id="search-product">
          <div class="col">
            <input class="form-control col" placeholder="Search here..." type="text" v-model="search">
          </div>
          <div class="col-auto">
            <button type="submit" class="btn btn-primary">
              <i class="bi bi-search"></i>
            </button>
          </div>
        </div>

        <hr>

        <div class="product-item-filter row">
          <div class="col-sm-6 text-center text-sm-left">
            <div class="row">
              <div class="col-auto d-flex flex-column justify-content-center">
                <span>Sort by</span>
              </div>
              <div class="col">
                <select id="basic" class="selectpicker show-tick form-control" v-model="sort">
                  <option value="" data-display="Select">Unsorted</option>
                  <option value="-price">High Price → Low Price</option>
                  <option value="price">Low Price → High Price</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-sm-6 text-center text-sm-left">
            <div class="row">
              <div class="col-auto d-flex flex-column justify-content-center"><span>Price</span></div>
              <div class="col-auto">
                <input type="number" class="form-control price-range" v-model="priceRange[0]">
              </div>
              <div class="col d-flex flex-column justify-content-center">
                <p-slider v-model="priceRange" range :max="100000" />
              </div>
              <div class="col-auto">
                <input type="number" class="form-control price-range" v-model="priceRange[1]">
              </div>
            </div>
          </div>
        </div>

        <hr>

        @include('templates.pagination')

        <hr>

        <div id="products">

          <transition-group name="list-group" tag="div" role="tabpanel" class="tab-pane fade show active row"
            v-if="items.length">
            <div class="product-single fix col-6 col-md-4 col-lg-3 mb-4" v-for="(item, i) in items" :key="item.id">
              <a :href="`{{ url('shop/item/${item.id}') }}`">
                <div class="card shadow-sm">
                  <div class="product-image ratio ratio-1x1" :style="{backgroundImage: itemBackgroundImage(item)}">
                  </div>
                  <div class="card-body">
                    <div
                      v-if="item.final_price < item.price && item.start_date < item.date_today && item.date_today<item.end_date"
                      class="type-lb">
                      <p class="sale">Sale</p>
                    </div>

                    <h4 class="pb-2">
                      @{{ item.name }}
                    </h4>
                    <div class="why-text">
                      <div class="d-flex flex-column ">
                        <div class="d-flex justify-content-between">
                          <p>
                            <span v-if="item.price_min < item.price_max">
                              <ruby>
                                @peso@{{item.price_min}}
                                <rt>starting from</rt>
                              </ruby>
                            </span>
                            <span v-else>
                              @peso@{{ item.price_min }}
                            </span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </transition-group>
          <h3 v-else class="text-center">
            No products found
          </h3>
        </div>

        <hr>
        @include('templates.pagination')
      </div>
    </div>
  </div>
</div>
@include('templates.added-to-card-modal')
<!-- End Shop Page -->
@endsection

@section('scripts')
<script defer src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"></script>
<script defer src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/tree/tree.min.js"></script>
<script defer src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/slider/slider.min.js"></script>
<script type="module" defer src="{{ asset('js/shop/shop.js') }}"></script>
@endsection