<div class="modal" id="added_to_cart_modal" tabindex="-1" aria-hidden="true" ref="added_to_cart_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">&nbsp;</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <h3 class="text-success">
          Added to Cart!
        </h3>
      </div>
    </div>
  </div>
</div>