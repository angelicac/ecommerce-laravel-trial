<div class="pagination row">
  <div class="ml-auto text-center text-sm-right store-pages-container">
    <ul class="store-pages">
      <li :class="{invisible: currentPage <= 0}">
        <a @@click.prevent="turnPage(-1)" href="#search-product">
          <i class="bi bi-caret-left-fill"></i>
        </a>
      </li>
      <li v-for="page in pages" :key="page" :class="{active: currentPage == page - 1}">
        <a href="#search-product" @@click="currentPage = page - 1">@{{page}}</a>
      </li>
      <li :class="{invisible: currentPage >= pages - 1}">
        <a @@click.prevent="turnPage(1)" href="#search-product">
          <i class="bi bi-caret-right-fill"></i>
        </a>
      </li>
    </ul>
  </div>
</div>