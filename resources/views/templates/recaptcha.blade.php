<script id="sitekey">
  {!! json_encode(config('app.recaptcha.site_key')) !!}
</script>

<script>
  function onloadCallback() {
    recaptcha = grecaptcha.render('recaptcha', {
      sitekey: JSON.parse(document.querySelector('#sitekey').textContent),
      theme: 'light',
      callback: onResponse
    });
  }

@if (config('app.recaptcha.enabled'))
  function validateCaptcha() {
    recaptcha_message.classList.add('d-none');
    form_button.setAttribute('disabled', 'disabled');
    if (!grecaptcha.getResponse(recaptcha)) {
      recaptcha_message.classList.remove('d-none');
      return false;
    } else {
      form_button.removeAttribute('disabled');
      return true;
    }
  }

  function getCaptchaResponse() {
    return grecaptcha.getResponse();
  }
@else
  function validateCaptcha() {
    return true;
  }

  function getCaptchaResponse() {
    return '';
  }
@endif
  function onResponse(event) {
    validateCaptcha();
  }
</script>
@if (config('app.recaptcha.enabled'))
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"></script>
@endif