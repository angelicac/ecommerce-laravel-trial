@extends('layouts.admin.index')

@section('app')
<script type="module">
  import useAdminIndex, { components } from "/js/modules/useAdminIndex.js";
  const { createApp, ref } = Vue;

  window.app = createApp({
      setup() {
          const controller = ref('overall-promotions');
          const columns = ref([{
              label: 'Name',
              link: true,
              formatter(data, row) {
                return row.item.item.name;
              }
            }, {
              label: 'Original Price',
              formatter(data, row) {
                return Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP'}).format(row.item.price);
              }
            }, {
              label: 'Sale Price',
              formatter(data, row) {
                return Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP'}).format(row.price);
              }
            }
          ]);
          const exports = useAdminIndex({controller, columns});
          return exports;
      },
      components
  })
</script>
@endsection