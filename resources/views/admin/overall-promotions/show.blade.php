@extends('layouts.admin.show')

@section('form')

<div class="row">
  <div class="col-6">
    <p-dropdown v-model="entity.sku"
                data-key="sku"
                :options="options"
                :option-label="(option) => `${option.name} - ${option.sku}`"
                option-value="sku"
                placeholder="Select an item"
                :filter="true"
                @@filter="search"
                :filter-fields="['name', 'sku']"
                :loading="loading"
                name="sku"
                id="sku"
                filter-placeholder="Find Item"
                @@change="setPrice($event, i)"
                class="w-100">
  </div>
  <div class="col-3">
    <p-inputnumber v-model.number="originalPrice"
                   mode="currency"
                   currency="PHP"
                   locale="en-US"
                   disabled />
  </div>
  <div class="col-3">
    <p-inputnumber v-model.number="entity.price"
                   mode="currency"
                   currency="PHP"
                   locale="en-US"
                   id="price"
                   name="price" />
  </div>
</div>
<div class="row">
  <div class="col">
    <label for="start_date">Start Date</label>
    <input type="date"
           class="form-control"
           id="start_date"
           name="start_date"
           v-model="entity.start_date">
    <x-forms.error :field="'start_date'"></x-forms.error>
  </div>
  <div class="col">
    <label for="end_date">End Date</label>
    <input type="date"
           class="form-control"
           id="end_date"
           name="end_date"
           v-model="entity.end_date">
    <x-forms.error :field="'end_date'"></x-forms.error>
  </div>
</div>
@endsection

@section('app')
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/dropdown/dropdown.min.js">
</script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/inputnumber/inputnumber.min.js">
</script>
<script type="module">
  import useAdminShow, { components } from "/js/modules/useAdminShow.js";
  const { createApp, ref } = Vue;

  window.app = createApp({
    setup() {
      const id = ref(ID);
      const controller = ref('overall-promotions');
      const title = ref('Overall Promotion');
      const loading = ref(false);

      const options = ref([]);

      const search = _.debounce(async (event) => {
        loading.value = true;
        try {
          const response = await axios.get("/admin/item-variants", {
            params: {
              name__like: event.value,
            },
          });
          options.value = response.data.data;
        } catch (e) {
          console.log(e);
        } finally {
          loading.value = false;
        }
      }, 300);

      const originalPrice = ref(0);

      const setPrice = (entity, event, index) => {
        for (let item of options.value) {
          if (item.sku === event.value) {
            originalPrice.value = item.price;
            entity.price = item.price;
          }
        }
      };
      const afterGetData = (responseData) => {
        if (!responseData.data.item) {
          return;
        }
        const item = responseData.data.item;
        item.name = item.item.name;
        options.value = [item];
        originalPrice.value = item.price;
      };

      const exports = useAdminShow({
        id,
        controller,
        title,
        afterGetData,
      });
      Object.assign(exports, {
        setPrice,
        originalPrice,
        search,
        options
      });
      return exports;
    },
    components: {
      ...components,
      'p-dropdown': primevue.dropdown,
      'p-inputnumber': primevue.inputnumber,
    }
  });

</script>
@endsection