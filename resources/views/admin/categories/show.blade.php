@extends('layouts.admin.show')

@section('form')
<div class="row">
  <div class="col">
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text"
             class="form-control"
             id="name"
             name="name"
             v-model="entity.name"
             required>
      <x-forms.error :field="'name'"></x-forms.error>
    </div>
  </div>
  <div class="col">
    <div class="form-group">
      <label for="parent">Parent Category</label><br>
      <div>
        <p-dropdown v-model="entity.parent_id"
                    option-value="id"
                    data-key="parent_id"
                    option-label="name"
                    :options="categories"
                    placeholder="Select Parent"
                    :selection-limit="1"
                    input-id="parent"
                    :loading="loading"
                    id="parent_id"
                    name="parent_id"
                    class="w-100"
                    filter
                    @@filter="(event) => search(event, i)"
                    show-clear />
      </div>
      <x-forms.error :field="'parent_id'"></x-forms.error>
    </div>

  </div>
</div>
@endsection

@section('app')
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/dropdown/dropdown.min.js">
</script>
<script type="module">
  import useAdminShow, { components } from "/js/modules/useAdminShow.js";
  const { createApp, ref, onMounted } = Vue;
  window.app = createApp({
    setup() {
      const id = ref(ID);
      const controller = ref('categories');
      const title = ref('Category');

      const categories = ref([]);

      const afterGetData = (responseData) => {
        if (responseData.data.parent_category) {
          categories.value.push(responseData.data.parent_category);
        }
      };

      
      const exports = useAdminShow({
        id,
        controller,
        title,
        afterGetData,
      });

      const search = _.debounce(async function (event, index) {
        exports.loading.value = true;
        try {
          const response = await axios.get('/admin/categories', {
            params: {
              name__like: event.value,
              id__ne: id.value
            }
          });
          categories.value = response.data.data;
        } catch (e) {
          console.log(e);
        } finally {
          exports.loading.value = false;
        }
      }, 300);
      
      onMounted(() => search({}));

      Object.assign(exports, { categories });

      return exports;
    },
    components: {
      ...components,
      'p-dropdown': primevue.dropdown
    }
});
</script>
@endsection