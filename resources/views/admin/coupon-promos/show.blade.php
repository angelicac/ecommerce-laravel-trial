@extends('layouts.admin.show')

@section('form')

<div class="row">
  <div class="col">
    <div class="form-group">
      <label for="code">Code</label>
      <input type="text"
             class="form-control"
             id="code"
             name="code"
             v-model="entity.code">
      <x-forms.error :field="'name'"></x-forms.error>
    </div>
  </div>
  <div class="col">
    <div class="form-group">
      <label for="type">Type</label>
      <select class="wide w-100 form-control"
              id="type"
              name="type"
              v-model="entity.type">
        <option value=""
                data-display="Select"
                disabled>Choose...</option>
        <option value="1">Flat</option>
        <option value="2">Minimum Spend</option>
      </select>
    </div>

  </div>
</div>

<div class="row">
  <div class="col">
    <div class="form-group">
      <label for="start_date">Start Date</label>
      <input type="date"
             class="form-control"
             id="start_date"
             name="start_date"
             v-model="entity.start_date"
             required>
      <x-forms.error :field="'start_date'"></x-forms.error>
    </div>
  </div>
  <div class="col">
    <div class="form-group">
      <label for="end_date">End Date</label>
      <input type="date"
             class="form-control"
             id="end_date"
             name="end_date"
             v-model="entity.end_date"
             :min="entity.start_date"
             required>
      <x-forms.error :field="'end_date'"></x-forms.error>
    </div>
  </div>
</div>

<template v-if="entity.type == 1">
  <div class="row">
    <h2>Items</h2>
  </div>
  <div class="row">
    <table class="table">
      <thead>
        <tr>
          <th></th>
          <th>SKU</th>
          <th>Original Price</th>
          <th>Price</th>
          <th>
            <button class="btn btn-sm btn-primary"
                    type="button"
                    :disabled="submitting"
                    @@click.prevent="add_to_m2m('items')"><i class="bi bi-plus"></i></button>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="(item, i) in m2m.items"
            :key="i">
          <td>
            {{-- <button class=" btn btn-danger"
                    @@click="delete_one2m('coupon_promo_items', i)"
                    type="button">
              <svg xmlns=" http://www.w3.org/2000/svg"
                   width="16"
                   height="16"
                   fill="currentColor"
                   class="bi bi-x"
                   viewBox="0 0 16 16">
                <path
                      d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
              </svg>
            </button> --}}
          </td>
          <td>
            <p-dropdown v-model="m2m.items[i].sku"
                        data-key="sku"
                        :options="options[i] ?? []"
                        :option-label="(option) => `${option.name} - ${option.sku}`"
                        option-value="sku"
                        placeholder="Select an item"
                        :filter="true"
                        @@filter="(event) => search(event, i)"
                        :filter-fields="['name', 'sku']"
                        :loading="searchLoading[i] ?? false"
                        :show-clear="true"
                        filter-placeholder="Find Item"
                        @@change="setPrice($event, i)">
          </td>
          <td>
            <p-inputnumber v-model="m2m.items[i].original_price"
                           mode="currency"
                           currency="PHP"
                           locale="en-US"
                           disabled />
          </td>
          <td>
            <p-inputnumber v-model="m2m.items[i].price"
                           mode="currency"
                           currency="PHP"
                           locale="en-US" />
          </td>

          <td></td>
        </tr>
      </tbody>
    </table>
  </div>

</template>

<div class=" row"
     v-if="entity.type == 2">
  <div class="col">
    <div class="form-group">
      <label for="rate_threshold">Apply discount on minimum spend of:</label>
      <input type="number"
             class="form-control"
             id="rate_threshold"
             name="rate_threshold"
             v-model="entity.rate_threshold">
    </div>
  </div>
  <div class="col">
    <div class="form-group">
      <label for="rate_reduction">Reduce price by:</label>
      <input type="number"
             class="form-control"
             id="rate_reduction"
             name="rate_reduction"
             v-model="entity.rate_reduction">
    </div>
  </div>
</div>


@endsection

@section('app')
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/dropdown/dropdown.min.js">
</script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/inputnumber/inputnumber.min.js">
</script>

<script type="module">
  import useAdminShow, { components } from "/js/modules/useAdminShow.js";
  const { createApp, ref, onMounted } = Vue;

  window.app = createApp({
    setup() {
      const id = ref(ID);
      const controller = ref('coupon-promos');
      const title = ref('Coupon Promo');

      const options = ref([]);
      const entities = ref([]);
      const searchItem = ref('');
      const searchLoading = ref({});

      const m2m = ref({items: []});

      const search = _.debounce(async function (event, index) {
        searchLoading.value[index] = true;
        try {
          const response = await axios.get('/admin/item-variants', {
            params: {
              name__like: event.value
            }
          });
          options.value[index] = response.data.data;
        } catch (e) {
          console.log(e);
        } finally {
          searchLoading.value[index] = false;
        }
      }, 300);

      const afterGetData = (responseData) => {
        const items = responseData.data.items;
        m2m.value.items = [];
        if (items) for (let i = 0; i < items.length; i++) {
          let item = items[i];
          item.name = item.item.name;
          item.original_price = item.price;
          item.price = item.pivot.price;
          options.value[i] = [item];
          m2m.value.items.push(item);
        }
      };

      const setPrice = (event, index) => {
        for (let item of options.value[index]) {
          if (item.sku == event.value) {
            m2m.value.items[index].original_price = item.price;
            m2m.value.items[index].price = item.price;
          }
        }
      };
      onMounted(() => search({}));

      const exports = useAdminShow({
        id,
        controller,
        title,
        afterGetData,
        m2m
      });

      Object.assign(exports, {
        searchLoading,
        options
      });

      return exports;
    },
    components: {
      ...components,
      'p-dropdown': primevue.dropdown,
      'p-inputnumber': primevue.inputnumber
    }
  });
</script>
@endsection