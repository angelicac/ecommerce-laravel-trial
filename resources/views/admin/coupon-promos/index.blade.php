@extends('layouts.admin.index')

@section('app')
<script type="module">
    import useAdminIndex, { components } from "/js/modules/useAdminIndex.js";
    const { createApp, ref } = Vue;

    window.app = createApp({
        setup() {
            const controller = ref('coupon-promos');
            const columns = ref([{
                label: 'Code',
                field: 'code',
                link: true,
            }]);
            const exports = useAdminIndex({controller, columns});
            return exports;
        },
        components
    })
</script>
@endsection