@extends('layouts.admin.index')

@section('app')
<script src="https://cdn.jsdelivr.net/npm/luxon@2.3.0/build/global/luxon.min.js"
        integrity="sha256-eWGhq5jRZ5QSi+PQuJt0eg4+hGCTwtsR9Qo+mC6Lyco="
        crossorigin="anonymous"
        defer></script>
<script type="module">
  const DateTime = luxon.DateTime;
  import useAdminIndex, { components } from "/js/modules/useAdminIndex.js";
  const { createApp, ref } = Vue;

  window.app = createApp({
      setup() {
          const controller = ref('orders');
          const columns = ref([
              {
                label: 'Number',
                field: 'number',
                link: true,
              },
              {
                label: 'Placed',
                field: 'created_at',
                formatter(data, row) {
                  return DateTime.fromISO(data).toLocaleString(DateTime.DATETIME_MED);
                }
              },
              {
                label: 'Total',
                field: 'total',
                formatter(data, row) {
                  return Intl.NumberFormat('en-PH', { style: 'currency', currency: 'PHP'}).format(data);
                }
              }
          ]);
          const exports = useAdminIndex({controller, columns});
          return exports;
      },
      components
  })
</script>
@endsection