@extends('layouts.admin.show')

@section('content')

<h1>
  <span v-if="entity.id">
    Editing Order @{{entity.number}}
  </span>
</h1>
<div class="container">
  <div class="row">

    <div class="col">
      <dl>
        <dt>Number</dt>
        <dd>@{{ entity.number }}</dd>
        <dt>Total</dt>
        <dd>@{{ entity.total }}</dd>
        <dt>Date & Time</dt>
        <dd>@{{ entity.datetime }}</dd>
        <dt>First Name</dt>
        <dd>@{{ entity.first_name }}</dd>
        <dt>Last Name</dt>
        <dd>@{{ entity.last_name }}</dd>
        <dt>Email</dt>
        <dd>@{{ entity.email }}</dd>
        <dt>Contact Number</dt>
        <dd>@{{ entity.contact_number }}</dd>
        <dt>Address</dt>
        <dd>@{{entity.address || entity.street}}</dd>
        <dt>Region</dt>
        <dd>@{{entity.region || entity.region_id}}</dd>
        <dt>Province</dt>
        <dd>@{{entity.province || entity.province_id}}</dd>
        <dt>City</dt>
        <dd>@{{entity.city || entity.city_id}}</dd>
        <dt>Barangay</dt>
        <dd>@{{entity.barangay || entity.barangay_id}}</dd>
        <dt>Zip Code</dt>
        <dd>@{{entity.zip_code}}</dd>
        <dt>Delivery Instructions</dt>
        <dd>@{{entity.delivery_instructions}}</dd>
      </dl>

    </div>
    <div class="col">
      <form action=""
            @submit.prevent="submit"
            method="POST">
        <div class="row">
          <h3>Status</h3>
        </div>
        <div class="row">
          <div class="col">
            <select name="order_status"
                    id="order_status"
                    class="form-control"
                    v-model="entity.order_status">
              <option v-for="(option, i) in orderStatusOptions"
                      :value="option.id">@{{ option.name }}</option>
            </select>
            <input type="hidden"
                   name="id"
                   v-model="entity.id">
          </div>
          <div class="col-auto">
            <button type="submit"
                    class="btn btn-primary"
                    :disabled="submitting">Update</button>
          </div>
        </div>
        <div class="row"
             v-if="entity.order_status == 'DELIVERED'">
          <div class="col">
            <label for="date_delivered">Date delivered: </label>
            <input type="date"
                   class="form-control"
                   id="date_delivered"
                   name="date_delivered"
                   v-model="entity.date_delivered">
          </div>
        </div>
        <div class="alert alert-success alert-dismissible fade show"
             role="alert"
             v-if="displaySaved">
          <i class="fa fa-check"></i> <em>Saved!</em>
          <button type="button"
                  class="btn-close"
                  @@click="displaySaved = false"
                  aria-label="Close"></button>
        </div>
      </form>
      <table class="table">
        <thead>
          <tr>
            <th>Products</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(item, i) in entity.items"
              :key="i">
            <td v-if="item.pivot.quantity > 0"
                class="name-pr"> <a :href="`{{ url('admin/items') }}/${item.item.id}`">@{{
                item.item.name }} (@{{ item.sku }})</a>
              <p>Price ₱@{{ item.pivot.price }}</p>
              <p>Quantity: @{{ item.pivot.quantity }}</p>
              <p>Total ₱@{{ item.pivot.quantity * item.pivot.price }}</p>
            </td>
          </tr>
        </tbody>
      </table>
      <table class="table">
        <tbody>
          <tr>
            <td>
              Total
            </td>
            <td>
              @{{ formatPrice(entity.original_total) }}
            </td>
          </tr>
          <tr>
            <td>Discount</td>
            <td>
              @{{ formatPrice(discount || 0) }}
            </td>
          </tr>
          <tr>
            <td>Coupon Discount</td>
            <td>
              @{{ formatPrice(entity.coupon_discount_total || 0) }}
            </td>
          </tr>
          <tr>
            <td>Shipping</td>
            <td>
              @{{ formatPrice(entity.shipping_fee) }}
            </td>
          </tr>
          <tr>
            <td>
              <strong>
                Grand Total
              </strong>
            </td>
            <td>
              @{{ formatPrice(entity.total) }}
            </td>
          </tr>
        </tbody>
      </table>
      <div class="d-grid gap-2">
        <a :href="`{{ url('') }}/admin/orders/invoice/${entity.id}`"
           class="btn btn-secondary btn-block"
           id="invoice_download"
           :download="`invoice-${entity.number}.pdf`">Download
          Invoice</a>
      </div>

    </div>
  </div>
</div>

@endsection

@section('app')
<script type="module">
  import useAdminShow, { components } from "/js/modules/useAdminShow.js";
  import { formatPrice } from "/js/modules/useHelpers.js";
  import { resolveAddress } from "/js/modules/useAddress.js";
  const { createApp, ref, computed, watch, onMounted } = Vue;

  window.app = createApp({
    setup() {
      const id = ref(ID);
      const controller = ref('orders');
      const title = ref('Order');
      const entity = ref({});

      const exports = useAdminShow({
        id,
        controller,
        title,
        entity
      });

      const orderStatus = computed(() => {
        entity.value.order_status
      });
      const discount = computed(() => {
        return (entity.value.original_total || 0)
          - (entity.value.total || 0)
          - (entity.value.coupon_discount_total || 0)
          + (entity.value.shipping_fee || 0);
      });

      const afterGetData = async() => {
        const address = await resolveAddress({
          region_id: entity.value.region_id,
          province_id: entity.value.province_id,
          city_id: entity.value.city_id,
          barangay_id: entity.value.barangay_id,
        });
        entity.value.region = address.region.name;
        entity.value.province = address.province.name;
        entity.value.city = address.city.name;
        entity.value.barangay = address.barangay.name;
      };

      const orderStatusOptions = ref([]);
      const getOptions = async() => {
        try {
          const response = await axios.get('/admin/order-status');
          orderStatusOptions.value = response.data.data;
        } catch (e) {
          console.log(e);
        }
      };

      onMounted(getOptions);

      Object.assign(exports, {
        orderStatusOptions,
        formatPrice,
        discount,
      })

      return exports;
    }
  });
</script>
@endsection