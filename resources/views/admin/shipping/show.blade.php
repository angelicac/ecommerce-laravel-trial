@extends('layouts.admin.show')

@section('styles')
<style>
  .default-shipping-fee-input {
    font-size: 40px;
    font-weight: 500;
  }

  .peso-input-display {
    border-right: 0;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }

  .default-shipping-fee-input[type="number"] {
    border-left: 0;
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    padding-left: 0;
  }

  #add-fee-card {
    cursor: pointer;
    background: #dfdfdf;
    transition: background 0.1s;
  }

  #add-fee-card p {
    transition: transform 0.2s;
  }

  #add-fee-card:hover {
    background: #eee;
  }

  #add-fee-card :hover p {
    transform: translateY(-5px);
  }




  .to-delete {
    background: #e3342f;

    label {
      color: gray;
    }

    .p-inputwrapper-filled,
    .p-inputwrapper-focus,
    .p-filled {
      &+label {
        color: white;
      }
    }
  }
</style>
@endsection

@section('form')
<div class="row mb-2">
  <div class="col">
    <div class="form-group">
      <label for="name">Name *</label>
      <input type="text"
             class="form-control"
             id="name"
             name="name"
             v-model="entity.name"
             required />
      <x-forms.error :field="'name'"></x-forms.error>
    </div>
  </div>
</div>
<div class="row mb-2">
  <div class="col">
    <label for="name">Region *</label>
    <p-dropdown class="w-100"
                v-model="entity.region_id"
                :options="regions"
                option-label="name"
                option-value="id"
                :filter="true"
                placeholder="Select a Region"
                @change="setRegionId" />
  </div>
  <div class="col">
    <label for="name">Province</label>
    <p-dropdown class="w-100"
                v-model="entity.province_id"
                :options="provinces"
                option-label="name"
                option-value="id"
                :filter="true"
                placeholder="Select a Province"
                :disabled="!entity.region_id"
                show-clear
                @change="setProvinceId" />
  </div>
  <div class="col">
    <label for="name">City</label>
    <p-dropdown class="w-100"
                v-model="entity.city_id"
                :options="cities"
                option-label="name"
                option-value="id"
                :filter="true"
                @filter="getCities"
                placeholder="Select a City"
                :disabled="!entity.province_id"
                show-clear
                :loading="citiesLoading" />
  </div>
</div>

<h2 class="my-4">Fees</h2>
<!-- Shipping Fees -->
<template v-if="one2m.fees">
  <p-card v-for="(fee, index) in one2m.fees.data"
          :key="`fee-${index}`"
          class="my-2"
          :class="{
      'to-delete': isMarkedForDeletion(fee.id, 'fees'),
    }">
    <template #header>
      <div class="row">
        <div class="col-auto ms-auto">
          <!-- Existing variants -->
          <button v-if="
              fee.shipping_location_id &&
              !isMarkedForDeletion(fee.id, 'fees')
            "
                  class="btn btn-danger"
                  type="button"
                  @click="markForDeletion(fee.id, 'fees')"
                  :id="`delete-fee-${index}`">
            <i class="bi bi-trash text-white"></i>
          </button>
          <button v-else-if="
              fee.shipping_location_id &&
              isMarkedForDeletion(fee.id, 'fees')
            "
                  class="btn btn-light"
                  type="button"
                  @click="markForDeletion(fee.id, 'fees')">
            <i class="bi bi-arrow-counterclockwise"></i>
          </button>

          <!-- New, uncommitted variants -->
          <button v-else
                  class="btn btn-danger"
                  type="button"
                  @click="one2m.fees.data.splice(index, 1)">
            <i class="bi bi-trash text-white"></i>
          </button>
        </div>
      </div>
    </template>
    <template #content>
      <div class="row">
        <div class="col-2">
          <span>Type:</span>
        </div>
        <div class="col-4">
          <p-dropdown :options="shippingMethodsList"
                      v-model="one2m.fees.data[index].shipping_method_id"
                      option-label="name"
                      option-value="id"
                      placeholder="Select a Shipping Method" />
        </div>

        <div class="col-2">
          <span v-if="
              one2m.fees.data[index].shipping_method_id ===
              shippingMethods.FREE_SHIPPING
            ">Minimum Price:</span>
          <span v-else> Rate: </span>
        </div>

        <div class="col-4">
          <p-inputnumber v-if="
              one2m.fees.data[index].shipping_method_id ===
              shippingMethods.FLAT_RATE
            "
                         v-model="one2m.fees.data[index].fee"
                         mode="currency"
                         locale="en-US"
                         currency="PHP"></p-inputnumber>
          <p-inputnumber v-else-if="
              one2m.fees.data[index].shipping_method_id ===
              shippingMethods.FREE_SHIPPING
            "
                         v-model="one2m.fees.data[index].minimum_price"
                         mode="currency"
                         locale="en-US"
                         currency="PHP"></p-inputnumber>
          <p-inputtext v-if="
              one2m.fees.data[index].shipping_method_id ===
              shippingMethods.CALCULATED
            "
                       v-model="one2m.fees.data[index].fee"></p-inputtext>
        </div>
      </div>
    </template>
  </p-card>
</template>
<p-card class="my-2"
        id="add-fee-card"
        @click="addNewFee">
  <template #content>
    <p class="text-center">Add Fee +</p>
  </template>
</p-card>
<p-confirmdialog></p-confirmdialog>

@endsection

@section('app')
<script>
  const SHIPPING_METHODS = {{ Js::from($shipping_methods) }}
</script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/dropdown/dropdown.min.js">
</script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/inputnumber/inputnumber.min.js">
</script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/inputtext/inputtext.min.js">
</script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/confirmdialog/confirmdialog.min.js">
</script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/card/card.min.js">
</script>
<script type="module">
  import useAdminShow, { components } from "/js/modules/useAdminShow.js";
  const { createApp, ref, computed, watch, onMounted } = Vue;

  window.app = createApp({
    setup() {

      const exports = {};

      const id = ref(ID);
      const controller = ref('shipping');
      const title = ref('Shipping');  

      const shippingMethodsList = computed(() => {
        const list = [];
        for (let method in SHIPPING_METHODS) {
          list.push({
            name: method.replace("_", " "),
            id: SHIPPING_METHODS[method],
          });
        }
        return list;
      });
  
      Object.assign(exports, {
        shippingMethods: SHIPPING_METHODS,
        shippingMethodsList,
      });
  
      // Form
  
      const one2m = ref({});
  
      Object.assign(exports, { one2m });
  
      // Regions
  
      const regions = ref([]);
      const selectedRegion = ref(null);
      const regionShippingFee = ref(0);
  
      const regionId = ref(0);
      const getRegions = async () => {
        const response = await axios.get(
          `${ADDRESS_API}/regions`
        );
        regions.value = response.data.data.entities;
      };
      const setRegionId = (event) => {
        regionId.value = event.value;
      };
  
      Object.assign(exports, {
        regions,
        selectedRegion,
        regionShippingFee,
        setRegionId,
      });
  
      // Provinces
  
      const provinces = ref([]);
      const selectedProvince = ref(null);
      const provinceShippingFee = ref(0);
  
      const provinceId = ref(0);
      const getProvinces = async () => {
        let url = `${ADDRESS_API}/provinces`;
        if (regionId.value) url += `/${regionId.value}`;
        const response = await axios.get(url);
        provinces.value = response.data.data.entities;
      };
      const setProvinceId = (event) => {
        provinceId.value = event.value;
      };
  
      watch(regionId, getProvinces);
  
      Object.assign(exports, {
        provinces,
        selectedProvince,
        provinceShippingFee,
        getProvinces,
        setProvinceId,
      });
  
      // Cities
  
      const cities = ref([]);
      const citiesLoading = ref(false);
      const selectedCity = ref(null);
      const cityShippingFee = ref(0);
  
      const getCities = _.debounce(async (event) => {
        citiesLoading.value = true;
        const data = {
          params: null,
        };
        if (event && event.value) {
          data.params = { q: event.value };
        }
        let url = `${ADDRESS_API}/cities`;
        if (provinceId.value) url += `/${provinceId.value}`;
        const response = await axios.get(url, data);
        cities.value = response.data.data.entities;
        citiesLoading.value = false;
      }, 300);
      watch(provinceId, getCities);
  
      const citiesLabel = (city) => {
        return `${city.name} (${city.data.province})`;
      };
  
      Object.assign(exports, {
        cities,
        selectedCity,
        cityShippingFee,
        citiesLoading,
        getCities,
        citiesLabel,
      });
  
      // Fees
  
      const setFees = (responseData) => {
        one2m.value.fees.data = [];
        if (!responseData.data) return;
        const fees = responseData.data.fees;
        for (let fee of fees) {
          delete fee.method;
        }
        one2m.value.fees.data = fees;
        regionId.value = responseData.data.region_id;
        provinceId.value = responseData.data.province_id;
      };
  
      const addNewFee = () => {
        one2m.value.fees.data.push({
          shipping_method_id: null,
          fee: "",
        });
      };
  
      Object.assign(exports, { setFees, addNewFee });
  
      onMounted(() => {
        getRegions();
  
        one2m.value.fees = {
          data: [{ shipping_method_id: null, fee: null }],
          delete: [],
        };
      });
 
      Object.assign(exports, useAdminShow({
        id,
        title,
        controller,
        one2m,
        afterGetData: setFees
      }));

      return exports;
    },
    components: {
      ...components,
      'p-dropdown': primevue.dropdown,
      'p-inputnumber': primevue.inputnumber,
      'p-inputtext': primevue.inputtext,
      'p-card': primevue.card,
    }
  });

</script>
@endsection