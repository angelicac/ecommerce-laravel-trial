@extends('layouts.admin.index')

@section('app')
<script type="module">
    import useAdminIndex, { components } from "/js/modules/useAdminIndex.js";
    const { createApp, ref } = Vue;

    window.app = createApp({
        setup() {
            const controller = ref('items');
            const columns = ref([{
                label: 'Name',
                field: 'name',
                link: true,
            }]);
            const exports = useAdminIndex({controller, columns});
            return exports;
        },
        components
    })
</script>
@endsection