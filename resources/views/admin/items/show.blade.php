@extends('layouts.admin.show')

@section('styles')
@parent

<style>
  .sku-input,
  .sku-label {
    flex-basis: 100%;
  }
</style>
<link href="https://unpkg.com/primeflex@^3/primeflex.min.css"
      rel="stylesheet" />

@endsection

@section('form')
<div class="modal image-preview-modal"
     tabindex="-1"
     ref="imageModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <svg data-bs-dismiss="modal"
             xmlns="http://www.w3.org/2000/svg"
             width="24"
             height="24"
             fill="currentColor"
             class="bi bi-x-circle close-button"
             viewBox="0 0 16 16">
          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
          <path
                d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
        </svg>
        <img :src="imageFocused"
             alt="">
      </div>
    </div>
  </div>
</div>
<div class="grid grid-even-two">
  <div>
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text"
             class="form-control"
             id="name"
             name="name"
             v-model="entity.name">
      <x-forms.error :field="'name'"></x-forms.error>
    </div>
    <div class="form-group">
      <label for="brand">Brand</label><br>
      <p-dropdown v-model="entity.brand_id"
                  option-value="id"
                  data-key="brand_id"
                  option-label="name"
                  :options="brands"
                  placeholder="Select Brand"
                  :selection-limit="1"
                  input-id="brand"
                  :loading="loading"
                  id="brand_id"
                  name="brand_id" />
      <x-forms.error :field="'brand_id'"></x-forms.error>
    </div>
  </div>

  <div>
    <div class="form-group">
    </div>
    <div class="form-group">
      <label for="categories">Categories</label><br>
      <p-multiselect v-model="m2m.categories"
                     :option-value="option => ({category_id : option.id})"
                     data-key="category_id"
                     option-label="name"
                     :options="options"
                     placeholder="Select Categories"
                     :loading="loading"
                     display="chip"
                     multiple />
    </div>
  </div>

</div>

<div class="form-group">
  <label for="description">Description</label>
  <textarea name="description"
            class="form-control"
            id="description"
            cols="30"
            rows="10"
            v-model="entity.description"></textarea>
  <x-forms.error :field="'description'"></x-forms.error>
</div>

<div class="my-4">
  <div class="row justify-content-between">
    <div class="col-auto">
      <h3>Images</h3>
    </div>
    <div class="col-auto">
      <button type="button"
              class="btn btn-light caret-button"
              @@click="imagesOpen = !imagesOpen"
              :class="{'upside-down': imagesOpen}">
        <i class="bi bi-caret-down-fill"></i>
      </button>
    </div>
  </div>
  <div class="animate__wrapper">
    <transition name="custom-classes-transition"
                enter-active-class="animate__animated animate__slideInDown animate__faster"
                leave-active-class="animate__animated animate__slideOutUp animate__faster">
      <div v-show="imagesOpen">
        <p>
          *<em>Images must not be larger than 2MB</em><br />
          *<em>You can only upload a maximum of 10MB of images</em>
        </p>
        <span>Total Size: @{{imageSizesTotal/1000000}}MB/10MB</span>
        <hr class="my-3">
        <input class="form-control"
               type="file"
               multiple
               ref="imageInput"
               @@change="updateImages">
        <div class="multi-image-preview mt-2">
          <div v-for="(image, index) in entity.images">
            <div class="image-preview"
                 :style="{backgroundImage: `url(${image.file_name})`}"
                 @@click.self="viewImage(image.file_name)">
              <svg @@click="markForDeletion(image.id, 'images')"
                   xmlns="http://www.w3.org/2000/svg"
                   width="24"
                   height="24"
                   fill="currentColor"
                   class="bi bi-x-circle close-button"
                   viewBox="0 0 16 16"
                   :class="{'to-delete': isMarkedForDeletion(image.id, 'images')}">
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                <path
                      d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
              </svg>
              <div class="overlay"
                   :class="{'to-delete': isMarkedForDeletion(image.id, 'images')}">
              </div>
            </div>
          </div>
          <div v-for="(image, index) in images">
            <div class="image-preview"
                 :style="{backgroundImage: `url(${generateImage(image)})`}"
                 @@click.self="viewImage(`${generateImage(image)}`)">
              <svg @@click="removeImage(index)"
                   xmlns="http://www.w3.org/2000/svg"
                   width="24"
                   height="24"
                   fill="currentColor"
                   class="bi bi-x-circle close-button"
                   viewBox="0 0 16 16"
                   :class="{'to-delete': isMarkedForDeletion(image.id, 'images')}">
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                <path
                      d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
              </svg>
            </div>
          </div>
        </div>
      </div>
    </transition>
  </div>
</div>

<div class="my-4">
  <div class="row justify-content-between">
    <div class="col-auto">
      <h3>Variants</h3>
    </div>
    <div class="col-auto">
      <button type="button"
              class="btn btn-light caret-button"
              @@click="variantsOpen = !variantsOpen"
              :class="{'upside-down': variantsOpen}">
        <i class="bi bi-caret-down-fill"></i>
      </button>
    </div>
  </div>
  <div class="animate__wrapper">
    <transition name="custom-classes-transition"
                enter-active-class="animate__animated animate__slideInDown animate__faster"
                leave-active-class="animate__animated animate__slideOutUp animate__faster">
      <div v-show="variantsOpen">

        {{-- New Meta Input --}}

        <p-card>
          <template #content>
            <div class="row">
              <div class="col">
                <span class="p-float-label">
                  <p-inputtext v-model="newMeta"
                               id="new_meta_input"
                               @keypress.enter.prevent="addNewMeta"
                               ref="newMetaInput"></p-inputtext>
                  <label for="new_meta_input">New Meta</label>
                </span>
              </div>
              <div class="col">
                <button class="btn btn-light"
                        type="button"
                        @@click="addNewMeta">
                  <i class="bi bi-plus"></i>
                </button>
              </div>
            </div>
          </template>
        </p-card>

        {{-- Variant Editing --}}
        <p-card v-for="(variant, index) in one2m.variants.data || []"
                :key="`variant-${index}`"
                class="my-2"
                :class="{
            'to-delete': isMarkedForDeletion(variant.sku, 'variants'),
          }">
          <template #header>
            <div class="row">
              <div class="col-auto ms-auto">
                <!-- Existing variants -->
                <button v-if="
                    variant.item_id &&
                    !isMarkedForDeletion(variant.sku, 'variants')
                  "
                        class="btn btn-danger"
                        type="button"
                        @@click="markForDeletion(variant.sku, 'variants')">
                  <i class="bi bi-trash text-white"></i>
                </button>
                <button v-else-if="
                    variant.item_id &&
                    isMarkedForDeletion(variant.sku, 'variants')
                  "
                        class="btn btn-light"
                        type="button"
                        @@click="markForDeletion(variant.sku, 'variants')">
                  <i class="bi bi-arrow-counterclockwise"></i>
                </button>

                <!-- New, uncommitted variants -->
                <button v-else
                        class="btn btn-danger"
                        type="button"
                        @@click="one2m.variants.data.splice(index, 1)">
                  <i class="bi bi-trash text-white"></i>
                </button>
              </div>
            </div>
          </template>
          <template #content>
            <div class="row variant-input-row">
              <!-- SKU -->
              <div class="col">
                <span class="p-float-label">
                  <p-inputtext :id="`sku-input-${index}`"
                               class="sku-input"
                               v-model="one2m.variants.data[index].sku"></p-inputtext>
                  <label :for="`sku-input-${index}`">SKU</label>
                </span>
              </div>

              <!-- Price -->
              <div class="col">
                <span class="p-float-label">
                  <p-inputnumber mode="currency"
                                 currency="PHP"
                                 locale="en-US"
                                 :id="`price-input-${index}`"
                                 class="price-input"
                                 v-model="one2m.variants.data[index].price"></p-inputnumber>
                  <label :for="`price-input-${index}`">Price</label>
                </span>
              </div>

              <!-- Stock -->
              <div class="col">
                <span class="p-float-label">
                  <p-inputnumber :id="`stock-input-${index}`"
                                 class="stock-input"
                                 v-model="one2m.variants.data[index].stock"></p-inputnumber>
                  <label :for="`stock-input-${index}`">Stock</label>
                </span>
              </div>

              <!-- Weight -->
              <div class="col">
                <span class="p-float-label">
                  <p-inputnumber :id="`weight-input-${index}`"
                                 class="weight-input"
                                 v-model="one2m.variants.data[index].weight"></p-inputnumber>
                  <label :for="`weight-input-${index}`">Weight (g)</label>
                </span>
              </div>

              <template v-if="dimensionsRequired">
                <!-- Length -->
                <div class="col">
                  <span class="p-float-label">
                    <p-inputnumber :id="`length-input-${index}`"
                                   class="length-input"
                                   v-model="one2m.variants.data[index].length"></p-inputnumber>
                    <label :for="`length-input-${index}`">Length (cm)</label>
                  </span>
                </div>

                <!-- Width -->
                <div class="col">
                  <span class="p-float-label">
                    <p-inputnumber :id="`width-input-${index}`"
                                   class="width-input"
                                   v-model="one2m.variants.data[index].width"></p-inputnumber>
                    <label :for="`width-input-${index}`">Width (cm)</label>
                  </span>
                </div>

                <!-- Height -->
                <div class="col">
                  <span class="p-float-label">
                    <p-inputnumber :id="`height-input-${index}`"
                                   class="height-input"
                                   v-model="one2m.variants.data[index].height"></p-inputnumber>
                    <label :for="`height-input-${index}`">Height (cm)</label>
                  </span>
                </div>
              </template>

              <!-- Meta -->
              <div class="col"
                   v-for="field of meta"
                   :key="`variant-${index}-${field}`">
                <span class="p-float-label">
                  <p-inputtext :id="`${field}-input-${index}`"
                               :class="`${field}-input`"
                               v-model="one2m.variants.data[index].meta[field]"></p-inputtext>
                  <label :for="`${field}-input-${index}`">@{{
                    field
                    }}</label>
                </span>
              </div>
            </div>
          </template>
        </p-card>
        <p-card class="my-2"
                id="add-variant-p-card"
                @@click="addNewVariant">
          <template #content>
            <p class="text-center">Add Variant +</p>
          </template>
        </p-card>

      </div>
    </transition>
  </div>
</div>
@endsection

@section('app')
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/datatable/datatable.min.js"></script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/column/column.min.js"></script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/columngroup/columngroup.min.js">
</script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/dropdown/dropdown.min.js"></script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/card/card.min.js"></script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/inputtext/inputtext.min.js"></script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/multiselect/multiselect.min.js">
</script>
<script>
  const SHOP_DIMENSIONS_REQUIRED = {{ config('app.shop.dimensions_required') ? 'true' : 'false' }};
</script>
<script defer
        type="module"
        src="{{ asset('js/admin/items-item.js') }}"></script>
@endsection