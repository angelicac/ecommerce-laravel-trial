@extends('layouts.admin.show')

@section('form')
<div class="row">
  <div class="col">
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text"
             class="form-control"
             id="name"
             name="name"
             v-model="entity.name">
      <x-forms.error :field="'name'"></x-forms.error>
    </div>
  </div>
</div>

<div class="row">
  <h2>Items</h2>
</div>
<div class="row">
  <table class="table">
    <thead>
      <tr>
        <th></th>
        <th>SKU</th>
        <th>Original Price</th>
        <th>Price</th>
        <th>
          <button class="btn btn-sm btn-primary"
                  type="button"
                  :disabled="submitting"
                  @@click.prevent="addToM2m('items')"><i class="bi bi-plus"></i></button>
        </th>
      </tr>
    </thead>
    <tbody>
      <tr v-for="(item, i) in m2m.items"
          :key="i">
        <td>
          {{-- <button class=" btn btn-danger"
                  @@click="markForDeletion(item.id, 'items')"
                  type="button">
            <svg xmlns=" http://www.w3.org/2000/svg"
                 width="16"
                 height="16"
                 fill="currentColor"
                 class="bi bi-x"
                 viewBox="0 0 16 16">
              <path
                    d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
            </svg>
          </button> --}}
        </td>
        <td>
          <p-dropdown v-model="m2m.items[i].sku"
                      data-key="sku"
                      :options="options[i] ?? []"
                      :option-label="(option) => `${option.name} - ${option.sku}`"
                      option-value="sku"
                      placeholder="Select an item"
                      :filter="true"
                      @@filter="(event) => search(event, i)"
                      :filter-fields="['name', 'sku']"
                      :loading="loading[i] ?? false"
                      :show-clear="true"
                      filter-placeholder="Find Item"
                      @@change="setPrice($event, i)" />
        </td>
        <td>
          <p-inputnumber v-model.number="m2m.items[i].original_price"
                         mode="currency"
                         currency="PHP"
                         locale="en-US"
                         disabled />
        </td>
        <td>
          <p-inputnumber v-model.number="m2m.items[i].price"
                         mode="currency"
                         currency="PHP"
                         locale="en-US" />
        </td>

        <td></td>
      </tr>
    </tbody>
  </table>
</div>
@endsection

@section('app')
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/dropdown/dropdown.min.js">
</script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/inputnumber/inputnumber.min.js">
</script>
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/confirmdialog/confirmdialog.min.js">
</script>
<script type="module">
  import useAdminShow, { components } from "/js/modules/useAdminShow.js";
  const { createApp, ref } = Vue;

  window.app = createApp({
    setup() {
      const id = ref(ID);
      const controller = ref('price-codes');
      const title = ref('Price Code');
      const one2m = ref({});
      const m2m = ref({});
      const loading = ref(false);

      // Items
      const options = ref({});

      const search = _.debounce(async (event, index) => {
        loading.value = true;
        try {
          const response = await axios.get("/admin/item-variants", {
            params: {
              name__like: event.value,
            },
          });
          options.value[index] = response.data.data;
        } catch (e) {
          console.log(e);
        } finally {
          loading.value = false;
        }
      }, 300);

      const setItems = (responseData) => {
        m2m.value.items = [];
        const items = responseData.data.items;
        if (items)
          for (let i = 0; i < items.length; i++) {
            let item = items[i];
            item.name = item.item.name;
            item.original_price = item.price;
            item.price = item.pivot.price;
            options.value[i] = [item];
            m2m.value.items.push(item);
          }
      };

      const setPrice = (event, index) => {
        for (let item of options.value[index]) {
          if (item.sku === event.value) {
            m2m.value.items[index].original_price = item.price;
            m2m.value.items[index].price = item.price;
          }
        }
      };

      const exports = useAdminShow({
        id,
        controller,
        title,
        one2m,
        m2m,
        afterGetData: setItems,
      });

      Object.assign(exports, {
        options,
        search,
        setItems,
        setPrice,
      });
      return exports;
    },
    components: {
      ...components,
      'p-dropdown': primevue.dropdown,
      'p-inputnumber': primevue.inputnumber,
    }
  });
</script>
@endsection