@extends('layouts.admin.index')

@section('app')
<script type="module">
  import useAdminIndex, { components } from "/js/modules/useAdminIndex.js";
    const { createApp, ref } = Vue;

    window.app = createApp({
        setup() {
            const controller = ref('customers');
            const columns = ref([
                {
                  label: 'Name',
                  link: true,
                  formatter: (data, row) => {
                    return `${row.first_name} ${row.last_name}`;
                  }
                },
                {
                  label: "Email",
                  field: 'email'
                },
                {
                  label: "Contact",
                  field: 'contact_number'
                },
                {
                  label: "Birth Date",
                  field: 'birth_date',
                },
                {
                  label: "Gender",
                  field: 'gender'
                },
            ]);
            const exports = useAdminIndex({controller, columns});
            return exports;
        },
        components
    })
</script>
@endsection