@extends('layouts.admin.show')

@section('header')
<div class="row">
  <div class="col">
    <h1>
      Viewing Customer @{{entity.first_name}} @{{entity.last_name}}
    </h1>
  </div>
</div>
@endsection

@section('form')
<div class="container">
  <div class="row">
    <div class="col">
      <dl>
        <dt>First Name</dt>
        <dd>@{{entity.first_name}}</dd>
        <dt>Last Name</dt>
        <dd>@{{entity.last_name}}</dd>
        <dt>Email</dt>
        <dd>@{{entity.email}}</dd>
        <dt>Contact Number</dt>
        <dd>@{{entity.contact_number}}</dd>
        <dt>Birth Date</dt>
        <dd>@{{entity.birth_date}}</dd>
        <dt>Gender</dt>
        <dd>@{{entity.gender}}</dd>
      </dl>
    </div>

    <div class="col">
      <div class="row">
        <div class="col">
          <h2>Set Price Code for Customer</h2>
        </div>
        <div class="col">
          <button class="btn btn-primary"
                  type="submit">Save</button>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <p-dropdown option-label="name"
                      :filter="true"
                      :options="options"
                      option-value="id"
                      @@filter="search"
                      v-model="entity.price_code_id"
                      :disabled="submitting"
                      class="w-100"
                      id="price_code_id"
                      name="price_code_id" />
        </div>
      </div>
      <div class="row mt-2">
        <div class="col">
          <div class="alert alert-success alert-dismissible fade show"
               role="alert"
               v-if="displaySaved">
            <i class="fa fa-check"></i> <em>Saved!</em>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('app')
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/dropdown/dropdown.min.js">
</script>
<script type="module">
  import useAdminShow, { components } from "/js/modules/useAdminShow.js";
  const { createApp, ref, onMounted } = Vue;

  window.app = createApp({
    setup() {
      const id = ref(ID);
      const controller = ref('customers');
      const options = ref([]);
      const entities = ref([]);
      const searchItem = ref('');
      const loading = ref(false);
      const showDefaultSubmitButton = ref(false);
      
      const search = _.debounce(async function (event) {
        loading.value = true;
        try {
          const response = await axios.get(`/admin/price-codes/`, {
            params: {
              global_search: event.value
            }
          });
          options.value = response.data.data;
        } catch (e) {
          console.log(e);
        } finally {
          loading.value = false;
        }
      }, 300);

      const saving = ref(false);
      const save = async (event) => {
        const data = new FormData(event.target);
        saving.value = true;

        try {
          const response = await axios.put(`/admin/customers/${this.id}`, data);
        } catch (e) {
          console.log(e);
        } finally {
          saving.value = false;
        }
      };

      const exports = useAdminShow({
        id,
        controller,
        options,
        entities,
        loading,
        showDefaultSubmitButton
      });

      onMounted(() => { search(false); });

      Object.assign(exports, {
        search,
        saving,
        save,
        options
      });

      return exports;
    },
    components: {
      ...components,
      'p-dropdown': primevue.dropdown
    }
  })

</script>
@endsection