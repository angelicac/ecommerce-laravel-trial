@extends('layouts.admin.show')

@section('form')
<div class="form-group">
  <label for="name">Name</label>
  <input type="text"
         class="form-control"
         id="name"
         name="name"
         v-model="entity.name"
         required>
  <x-forms.error :field="'name'"></x-forms.error>
</div>
@endsection

@section('app')
<script type="module">
  import useAdminShow, { components } from "/js/modules/useAdminShow.js";
  const { createApp, ref } = Vue;

  window.app = createApp({
    setup() {
      const id = ref(ID);
      const controller = ref('brands');
      const title = ref('Brand');
      const exports = useAdminShow({
        id,
        controller,
        title
      });
      return exports;
    },
    components,
  });
</script>
@endsection