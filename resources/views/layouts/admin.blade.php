@php
$routes = [
'admin.home' => 'Dashboard',
'admin.brands.index' => 'Brands',
'admin.categories.index' => 'Categories',
'admin.items.index' => 'Items',
'admin.overall-promotions.index' => 'Overall Promo',
'admin.price-codes.index' => 'Price Codes',
'admin.coupon-promos.index' => 'Coupon Promos',
'admin.customers.index' => 'Customers',
'admin.orders.index' => 'Orders',
'admin.shipping.index' => 'Shipping'
];
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible"
        content="IE=edge">
  <meta name="viewport"
        content="width=device-width, initial-scale=1">
  <meta name="description"
        content="ECommerce" />
  <meta name="keywords"
        content="Home, Shop, Contact Us, Cart, " />
  <meta name="author"
        content="www.technomancer.biz" />
  <meta property="og:title"
        content="Ecommerce" />
  <meta property="og:description"
        content="" />
  <meta property="og:image"
        content="opengraph.jpg?v=1" />
  <meta property="og:type"
        content="website" />

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
        crossorigin="anonymous">
  <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap"
        rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons"
        rel="stylesheet"
        type="text/css">
  <link href="https://cdn.jsdelivr.net/npm/animate.css@^4.0.0/animate.min.css"
        rel="stylesheet"
        type="text/css">
  <link href="https://unpkg.com/primevue/resources/themes/saga-blue/theme.css"
        rel="stylesheet">
  <link href="https://unpkg.com/primevue/resources/primevue.min.css"
        rel="stylesheet">
  <link href="https://unpkg.com/primeicons/primeicons.css"
        rel="stylesheet">
  @yield('styles')
  <link rel="stylesheet"
        href="{{ URL::asset('css/admin/style.css') }}">
  <title>
    {{ $title ?? 'Admin' }} | E-Commerce
  </title>

  @include('layouts.ico')

  <style>
    /* src: https://getbootstrap.com/docs/5.0/examples/dashboard/dashboard.css */
    body {
      font-size: .875rem;
    }

    .feather {
      width: 16px;
      height: 16px;
      vertical-align: text-bottom;
    }

    @media (max-width: 767.98px) {
      .sidebar {
        top: 5rem;
      }
    }

    .sidebar-sticky {
      position: relative;
      top: 0;
      height: calc(100vh - 48px);
      padding-top: .5rem;
      overflow-x: hidden;
      overflow-y: auto;
      /* Scrollable contents if viewport is shorter than content. */
    }

    .sidebar .nav-link {
      color: #333;
    }

    .sidebar .nav-link .feather {
      margin-right: 4px;
      color: #727272;
    }

    .sidebar .nav-link.active {
      color: #2470dc;
    }

    .sidebar .nav-link:hover .feather,
    .sidebar .nav-link.active .feather {
      color: inherit;
    }

    .sidebar-heading {
      font-size: .75rem;
      text-transform: uppercase;
    }

    /*
         * Navbar
         */
    .navbar-brand {
      padding-top: .75rem;
      padding-bottom: .75rem;
      font-size: 1rem;
      background-color: rgba(0, 0, 0, .25);
      box-shadow: inset -1px 0 0 rgba(0, 0, 0, .25);
    }

    .navbar .navbar-toggler {
      top: .25rem;
      right: 1rem;
    }

    .navbar .form-control {
      padding: .75rem 1rem;
      border-width: 0;
      border-radius: 0;
    }

    .form-control-dark {
      color: #fff;
      background-color: rgba(255, 255, 255, .1);
      border-color: rgba(255, 255, 255, .1);
    }

    .form-control-dark:focus {
      border-color: transparent;
      box-shadow: 0 0 0 3px rgba(255, 255, 255, .25);
    }
  </style>
</head>

<body>
  <header class="navbar navbar-dark sticky-top flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3"
       href="#">E-Commerce</a>
    <form action="{{ url('logout') }}"
          method="POST">
      @csrf
      <button type="submit"
              class="btn logout-link px-3">Logout</button>
    </form>
    <button class="navbar-toggler position-absolute d-md-none collapsed"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu"
            aria-controls="sidebarMenu"
            aria-expanded="false"
            aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  </header>
  <div class="container-fluid">
    <div class="row pb-5">
      <nav id="sidebarMenu"
           class="col-md-3 col-lg-2 d-md-block sidebar collapse">
        <div class="position-sticky pt-3">
          <ul class="nav flex-column">
            @foreach ($routes as $name => $label)
            <li class="nav-item">
              <a href="{{ route($name) }}"
                 class="nav-link">{{ $label }}</a>
            </li>
            @endforeach
          </ul>
        </div>
      </nav>

      <main role="main"
            class="col-md-9 ms-sm-auto col-lg-10 px-md-4"
            id="main">
        @yield('content')
      </main>
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
          crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  @if (config('app.debug')))
  <script src="https://unpkg.com/vue@next"></script>
  @else
  <script src="https://unpkg.com/vue@3.1.4/dist/vue.runtime.global.prod.js"></script>
  @endif
  <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"></script>
  <script src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/core/core.min.js"></script>
  <script defer
          src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/usetoast/usetoast.min.js">
  </script>
  <script defer
          src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/useconfirm/useconfirm.min.js">
  </script>
  <script defer
          src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/toastservice/toastservice.min.js">
  </script>
  <script defer
          src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/confirmationservice/confirmationservice.min.js">
  </script>
  <script defer
          src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/toast/toast.min.js">
  </script>
  <script src="https://unpkg.com/dayjs@1.8.21/dayjs.min.js"></script>
  <script src="{{ URL::asset('/js/vue-init.js') }}"></script>
  <script>
    const ID = {{ $entity ?? null ? $entity->id : 0 }};
        const ADDRESS_API = "{{ config('app.address_api') }}";
        const BASE_URL = "{{ url('') }}";
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        axios.defaults.baseURL = BASE_URL + '/api';
  </script>
  @yield('scripts')
  <script type="module"
          src="{{ asset('js/admin/default.js') }}"
          defer>
  </script>

</body>

</html>