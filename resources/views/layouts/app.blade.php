<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport"
        content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible"
        content="IE=edge">
  <meta name="description"
        content="Technomancer E-Commerce" />
  <meta name="keywords"
        content="E-Commerce, Shop, Online Shopping" />
  <meta name="author"
        content="www.technomancer.biz" />
  <meta property="og:title"
        content="Technomancer E-Commerce" />
  <meta property="og:description"
        content="" />
  <meta property="og:type"
        content="website" />

  <!-- CSRF Token -->
  <meta name="csrf-token"
        content="{{ csrf_token() }}">

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
  <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">

  <title>{{ config('app.name', 'Laravel') }}</title>

  @include('layouts.ico')

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"
          defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch"
        href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito"
        rel="stylesheet">

  <!-- Styles -->
  <link href="https://unpkg.com/primevue/resources/themes/saga-blue/theme.css"
        rel="stylesheet">
  <link href="https://unpkg.com/primevue/resources/primevue.min.css"
        rel="stylesheet">
  <link href="https://unpkg.com/primeicons/primeicons.css"
        rel="stylesheet">

  <link href="{{ asset('css/app.css') }}"
        rel="stylesheet">
  @yield('styles')

</head>

<body>
  <div id="main">
    <div id="loader"
         v-if="false">
      <div class="spinner-border text-light"
           role="status">
        <span class="visually-hidden">Loading...</span>
      </div>
    </div>
    <header>
      <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
          <a class="navbar-brand"
             href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
          </a>
          <button class="navbar-toggler"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent"
                  aria-expanded="false"
                  aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse"
               id="navbarSupportedContent">
            <form class="flex-grow-1"
                  action="{{ route('shop.index') }}">
              <input class="form-control"
                     type="text"
                     placeholder="Search"
                     aria-label="Search"
                     name="search">
            </form>
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav me-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ms-auto">
              <li class="nav-item">
                <a @class(['nav-link', 'active'=> Route::current()->getName() === 'index']) aria-current="page" href="{{
                  route('index')
                  }}">Home</a>
              </li>
              <li class="nav-item">
                <a @class(['nav-link', 'active'=> Route::current()->getName() ==='shop.index']) aria-current="page"
                  href="{{
                  route('shop.index') }}">Shop</a>
              </li>
              <li class="nav-item">
                <a @class(['nav-link', 'active'=> Route::current()->getName() ==='contact-us']) href="{{
                  route('contact-us')
                  }}">Contact
                  Us</a>
              </li>
              <li class="nav-item">
                <a class="nav-link"
                   href="{{ route('cart.index') }}">
                  <template v-if="cartState.totalQuantity">
                    <i class="bi bi-cart-fill"></i> @{{ cartState.totalQuantity }}
                  </template>
                  <template v-else>
                    <i class="bi bi-cart"></i>
                  </template>
                </a>
              </li>

              <!-- Authentication Links -->
              @guest
              @if (Route::has('login'))
              <li class="nav-item">
                <a class="nav-link"
                   href="{{ route('login') }}">{{ __('Login') }}</a>
              </li>
              @endif

              @if (Route::has('register'))
              <li class="nav-item">
                <a class="nav-link"
                   href="{{ route('register') }}">{{ __('Register') }}</a>
              </li>
              @endif
              @else
              <li class="nav-item dropdown">
                <a id="navbarDropdown"
                   class="nav-link dropdown-toggle"
                   href="#"
                   role="button"
                   data-bs-toggle="dropdown"
                   aria-haspopup="true"
                   aria-expanded="false"
                   v-pre>
                  {{ Auth::user()->name }}
                </a>

                <ul class="dropdown-menu dropdown-menu-right"
                    aria-labelledby="navbarDropdown">
                  <li>
                    <a class="dropdown-item"
                       href="{{ route('customer.index') }}">
                      My Account
                    </a>
                  </li>
                  <li>
                    <hr class="dropdown-divider">
                  </li>
                  <li>
                    <a class="dropdown-item"
                       href="{{ route('logout') }}"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                    </a>
                    <form id="logout-form"
                          action="{{ route('logout') }}"
                          method="POST"
                          class="d-none">
                      @csrf
                    </form>
                  </li>
                </ul>
              </li>
              @endguest
            </ul>
          </div>
        </div>
      </nav>
    </header>

    <main class="py-4">
      @yield('content')
    </main>
    <footer class="footer-copyright">
      <p class="footer-company">Copyright &copy;
        {{ (new DateTime())->format('Y') }} All rights reserved | Powered by:
        <a href="https://www.technomancer.biz/"
           target="_blank">
          <img src="{{ asset('img/techno.png') }}"> Technomancer
        </a> | <a href="#"
           @@click.prevent="showModal('mailing_list_modal')">Join our mailing list</a>!
      </p>
    </footer>
    <p-toast position="bottom-right"
             group="br"></p-toast>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
          crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  @if (config('app.env') === 'production')
  <script src="https://unpkg.com/vue@3.2/dist/vue.runtime.global.prod.js"></script>
  @else
  <script src="https://unpkg.com/vue@next"></script>
  @endif
  <script defer
          src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/core/core.min.js"></script>
  <script defer
          src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/usetoast/usetoast.min.js">
  </script>
  <script defer
          src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/toastservice/toastservice.min.js">
  </script>
  <script defer
          src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/toast/toast.min.js">
  </script>
  <script>
    const BASE_URL = "{{ url('') }}";
    const ADDRESS_API = "{{ config('app.address_api') }}";
    const RECAPTCHA_ENABLED = {{ config('app.recaptcha.enabled') ? 'true' : 'false' }};
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    axios.defaults.baseURL = BASE_URL + '/api';
  </script>
  @yield('scripts')
  <script type="module"
          src="{{ asset('js/default.js') }}"
          defer>
  </script>
</body>

</html>