@extends('layouts.admin')
@section('content')
<h1 class="h1 px-2">{{ $title ?? '' }}</h1>
@if ($show_add ?? false)
<a href="{{ url($insert_view_link) }}"
   class="btn btn-sm btn-success">
  {{ $insert_view_link_text }}
</a>

@endif

@if ($allow_delete)
<div class="mt-2">
  @if (request('deleted'))
  <a href="{{ route($list_view_link) }}"
     class="text-decoration-none">
    &lt; Back
  </a>
  @else
  <a href="{{ route($list_view_link, ['deleted' => 1]) }}"
     class="text-decoration-none">
    Recycle Bin &gt;
  </a>
  @endif
</div>
@endif

<hr>
<p-datatable :value="entities"
             paginator
             paginator-position="both"
             :rows="100"
             dataKey="id"
             row-hover
             :loading="loading"
             :filters="filters"
             filter-display="row"
             lazy
             @@page="getData"
             @@sort="getData"
             @@filter="getData"
             :total-records="totalRecords"
             v-model:sort-field="search.sortField"
             v-model:sort-order="search.sortOrder"
             removable-sort>
  <template #header>
    <div class="row">
      <div class="col-auto ms-auto">
        <p-inputtext v-model="globalSearch"
                     placeholder="Keyword Search" />
      </div>
    </div>
  </template>

  <p-column v-for="col of columns"
            :field="col.field"
            :header="col.label"
            :key="col.field || col.label"
            :sortable="col.sortable"
            :class="{filterable: col.filterable}">


    {{-- Search Input --}}
    <template #filter="{filterModel,filterCallback}">
      <template v-if="col.filterable">
        <template v-if="col.filterable === FILTER_DATE_RANGE">
          <p-calendar v-model="filters[col.field].value"
                      selection-mode="range" />
        </template>
        <template v-else-if="col.filterable === FILTER_NUMBER_RANGE">
          <p-inputnumber v-model="filters[col.field].value[0]"
                         mode="decimal"
                         :minFractionDigits="2"
                         placeholder="From">
          </p-inputnumber>
          <p-inputnumber v-model="filters[col.field].value[1]"
                         mode="decimal"
                         :minFractionDigits="2"
                         placeholder="To">
          </p-inputnumber>
        </template>
        <template v-else>
          <p-inputtext type="text"
                       v-model="filters[col.field].value"
                       @@keydown.enter="filterCallback()"
                       class="p-column-filter"
                       :placeholder="`Search by ${col.field}`"
                       @@change="getData" />
        </template>

      </template>
    </template>

    {{-- Cell Content --}}
    <template #body="props">
      <span v-if="col.renderer"
            v-html="col.renderer(props.data[col.field], props.data)"></span>
      <span v-else-if="col.link">
        <template v-if="col.formatter">
          <a :href="`${itemUrl}/${props.data.id}`"
             class="btn btn-link">@{{ col.formatter(props.data[col.field],
            props.data) }}</a>
        </template>
        <template v-else>
          <a :href="`${itemUrl}/${props.data.id}`"
             class="btn btn-link">@{{ props.data[col.field] }}</a>
        </template>
      </span>
      <span v-else-if="col.formatter">
        @{{ col.formatter(props.data[col.field], props.data) }}
      </span>
      <span v-else>
        @{{ props.data[col.field] }}
      </span>
    </template>
  </p-column>

  {{-- Restore Column --}}
  @if (request('deleted'))
  <p-column header="Restore">
    <template #body="{ data, index }">
      <button class="btn btn-info"
              @@click.prevent="confirmRestore(data.id)"
              :id="`restore-button-${index}`">
        <i class="bi bi-arrow-counterclockwise"></i>
      </button>
    </template>
  </p-column>
  @endif

</p-datatable>
@endsection

@section('scripts')
<script src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/datatable/datatable.min.js">
</script>
<script src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/column/column.min.js"></script>
<script src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/columngroup/columngroup.min.js">
</script>
<script src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/calendar/calendar.min.js"></script>
<script src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/confirmdialog/confirmdialog.min.js">
</script>
<script src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/useconfirm/useconfirm.min.js">
</script>
<script src="{{ URL::asset('/js/admin/admin-list-mixin.js') }}"></script>

@sectionMissing('app')
<script>
  const App = Vue.createApp({
                mixins: [AdminListMixin],
                data() {
                    return {
                        controller: '',
                        columns: [{
                            label: 'Name',
                            field: 'name',
                            link: true,
                        }]
                    };
                }
            });
</script>
@else
@yield('app')
@endif

@endsection