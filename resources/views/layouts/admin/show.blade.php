@extends('layouts.admin')

@section('content')

@sectionMissing('header')
<div class="row">
  <div class="col">
    <h1 id="admin-show-heading">
      <span v-if="entity.id">
        Editing @{{ title }} @{{ entity.name }}
      </span>
      <span v-else> New @{{ title }} </span>
    </h1>
  </div>
  <div class="col-auto">
    <div class="spinner-border text-success"
         role="status"
         v-if="loading || submitting">
      <span class="visually-hidden">Loading...</span>
    </div>
  </div>
</div>
@else
@yield('header')
@endif

<div class="container">
  <form action=""
        @@submit.prevent="submit"
        method="POST">
    <fieldset :disabled="submitting">

      @yield('form')

      <div class="row"
           v-if="showDefaultSubmitButton">
        <div class="col-auto">
          <button type="submit"
                  class="btn btn-primary">Submit</button>
        </div>
        <div class="col">
          <div class="alert alert-success alert-dismissible fade show"
               role="alert"
               v-if="displaySaved">
            <i class="fa fa-check"></i> <em>Saved!</em>
            <button type="button"
                    class="btn-close"
                    data-bs-dismiss="alert"
                    aria-label="Close"
                    @@click="displaySaved = false">
              &times;
            </button>
          </div>
        </div>
      </div>
    </fieldset>
  </form>
</div>

<div class="row"
     v-if="entity.id && allowDelete">
  <div class="col-auto ms-auto">
    <button class="btn btn-danger"
            @click.prevent="confirmDelete"
            id="delete-button">
      <i class="bi bi-trash text-white"></i>
    </button>
  </div>
</div>
<p-confirmdialog></p-confirmdialog>
@endsection

@section('scripts')
<script defer
        src="https://unpkg.com/primevue{{ '@' . config('app.primevue.version') }}/confirmdialog/confirmdialog.min.js">
</script>
@yield('app')
@endsection