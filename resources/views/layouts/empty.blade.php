<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport"
        content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible"
        content="IE=edge">
  <meta name="description"
        content="Technomancer E-Commerce" />
  <meta name="keywords"
        content="E-Commerce, Shop, Online Shopping" />
  <meta name="author"
        content="www.technomancer.biz" />
  <meta property="og:title"
        content="Technomancer E-Commerce" />
  <meta property="og:description"
        content="" />
  <meta property="og:type"
        content="website" />

  <!-- CSRF Token -->
  <meta name="csrf-token"
        content="{{ csrf_token() }}">

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
        crossorigin="anonymous">
  <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">

  <title>{{ config('app.name', 'Laravel') }}</title>

  @include('layouts.ico')

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"
          defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch"
        href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito"
        rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}"
        rel="stylesheet">
  @yield('styles')

</head>

<body>
  <div id="main">
    <div id="loader"
         v-if="false">
      <div class="spinner-border text-light"
           role="status">
        <span class="visually-hidden">Loading...</span>
      </div>
    </div>

    <main class="py-4 d-flex flex-column justify-content-center">
      @yield('content')
    </main>
    <footer class="footer-copyright">
      <p class="footer-company">Copyright &copy;
        {{ (new DateTime())->format('Y') }} All rights reserved | Powered by:
        <a href="https://www.technomancer.biz/"
           target="_blank">
          <img src="{{ asset('img/techno.png') }}"> Technomancer
        </a>
      </p>
    </footer>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
          crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
  @if (config('app.env') === 'production')
  <script src="https://unpkg.com/vue@3.1.4/dist/vue.runtime.global.prod.js"></script>
  @else
  <script src="https://unpkg.com/vue@next"></script>
  @endif

  <script>
    const BASE_URL = "{{ url('') }}";
    const ADDRESS_API = "{{ config('app.address_api') }}";
    const RECAPTCHA_ENABLED = {{ config('app.recaptcha.enabled') ? 'true' : 'false' }};
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    axios.defaults.baseURL = BASE_URL + '/api';

  </script>
  <script src="{{ asset('js/vue-init.js') }}"></script>
  @yield('scripts')
  <script type="module">
    if (typeof window.app === 'undefined') {
        const { createApp } = Vue;
        window.app = createApp({});
    }
    const vm = window.app.mount('#main');
  </script>
</body>

</html>