<div class="text-danger form-errors" v-if="{{ $errorObject }}.{{ $field }}">
    <template v-for="error in {{ $errorObject }}.{{ $field }}">
        <span>
            @{{error}}
        </span>
        <br />
    </template>
</div>