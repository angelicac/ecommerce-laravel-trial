@extends('layouts.app')

@section('styles')
<style>
  .grid-item {
    display: grid;
    width: 100px;
    grid-template-columns: 100%;
  }

  .grid-item p {
    white-space: normal;
    font-size: 10px;
    text-align: center;
  }

  .product-image {
    background-size: cover;
    background-position: center center;
  }
</style>
@endsection

@section('content')

<!-- Start All Title Box -->
<div class="all-title-box back-pst"
     style=" background-image: url(https://www.x8wood.com/quantagoods.com/assets/images/shop.jpg?v=2);">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="res-txt">Order #@{{ order.number }}</h2>
      </div>
    </div>
  </div>
</div>
<!-- End All Title Box -->
<!-- Start Cart  -->
<div class="cart-box-main">
  <div class="container">
    <div class="row">

      <div class="col text-center">
        <h2>Status: @{{ order.status ? order.status.name : '' }}</h2>
        <p v-if="order.order_status === 'DELIVERED'">Delivered on: @{{ order.date_delivered }}</p>
      </div>

    </div>
    <div class="row my-5">
      <div class="col-lg-8 col-sm-12">
        <div class="table-main table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>Image</th>
                <th>Product Name</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="(item, i) in order.items">
                <td class="thumbnail-img">
                  <a :href="siteUrl(`shop/item/${item.id}`)">
                    <div class="product-image ratio ratio-1x1"
                         :style="{backgroundImage: itemBackgroundImageApi(item.item_id)}">
                    </div>
                  </a>
                </td>
                <td class="name-pr">
                  <p>
                    <span class="font-weight-bold">@{{ item.item.name }} (@{{ item.sku }})</span><br>
                    <small v-if="item.meta.length > 0">(@{{variantSummary(item)}})</small>
                  </p>

                  <span v-if="item.pivot.price < item.price">
                    <del>
                      @{{ formatPrice(item.price) }}
                    </del>
                    @{{ formatPrice(item.pivot.price) }}
                  </span>
                  <span v-else>
                    @{{ formatPrice(item.pivot.price) }}
                  </span>
                  <p>x@{{item.pivot.quantity}}</p>
                  <p>
                    Total
                    <span class="text-success mx-2 font-weight-bold">
                      @{{ formatPrice(item.pivot.quantity * item.pivot.price) }}
                    </span>
                  </p>
                </td>
              </tr>
            </tbody>
          </table>

        </div>

      </div>
      <div class="col-lg-4 col-sm-12">
        <div class="order-box">
          <h3>Order summary</h3>
          <div class="d-flex">
            <h4>Sub Total</h4>
            <div class="ml-auto">
              @{{ formatPrice(order.original_total) }}
            </div>
          </div>
          <div class="d-flex">
            <h4>Discount</h4>
            <div class="ml-auto">
              @{{ formatPrice(discount) }}
            </div>
          </div>
          <hr class="my-1">
          <div class="d-flex">
            <h4>Coupon Discount</h4>
            <div class="ml-auto">
              @{{ formatPrice(order.coupon_discount_total) }}
            </div>
          </div>
          <div class="d-flex">
            <h4>Shipping Cost</h4>
            <div class="ml-auto">
              @{{ formatPrice(order.shipping_fee) }}
            </div>
          </div>
          <hr>
          <div class="d-flex gr-total">
            <h5>Grand Total</h5>
            <div class="ml-auto h5 text-success">
              @{{ formatPrice(order.total) }}
            </div>
          </div>
          <hr>
        </div>
        <div class="address">
          <dl>
            <dt>Date & Time</dt>
            <dd>@{{ order.datetime }}</dd>
            <dt>First Name</dt>
            <dd>@{{ order.first_name }}</dd>
            <dt>Last Name</dt>
            <dd>@{{ order.last_name }}</dd>
            <dt>Email</dt>
            <dd>@{{ order.email }}</dd>
            <dt>Contact Number</dt>
            <dd>@{{ order.contact_number }}</dd>
            <dt>Address</dt>
            <dd>@{{ order.address || order.street }}</dd>
            <dt>Region</dt>
            <dd>@{{ order.region || order.region_id }}</dd>
            <dt>Province</dt>
            <dd>@{{ order.province || order.province_id }}</dd>
            <dt>City</dt>
            <dd>@{{ order.city || order.city_id }}</dd>
            <dt>Barangay</dt>
            <dd>@{{ order.barangay || order.barangay_id }}</dd>
            <dt>Zip Code</dt>
            <dd>@{{ order.zip_code}}</dd>
            <dt>Delivery Instructions</dt>
            <dd>@{{ order.delivery_instructions }}</dd>
          </dl>

        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- End Cart -->
@endsection

@section('scripts')
<script type="module">
  import cartStore from "/js/store/cartStore.js";
  import { formatPrice, siteUrl } from "/js/modules/useHelpers.js"
  import { itemBackgroundImageApi } from "/js/modules/useItemImage.js"
  import { resolveAddress } from "/js/modules/useAddress.js";

  const ORDER_NUMBER = '{{ $order_number }}';
  const { createApp, ref, onMounted, computed } = Vue;
  window.app = createApp({
    setup() {

      const exports = {
        cartState: cartStore,
        formatPrice,
        siteUrl,
        itemBackgroundImageApi
      };

      const order = ref({});
      const getData = async () => {
        try {
          const response = await axios.get(`/orders/data/${ORDER_NUMBER}`);
          order.value = response.data.data;

          const address = await resolveAddress({
            region_id: order.value.region_id,
            province_id: order.value.province_id,
            city_id: order.value.city_id,
            barangay_id: order.value.barangay_id,
          });
          console.log(address);
          order.value.region = address.region.name;
          order.value.province = address.province.name;
          order.value.city = address.city.name;
          order.value.barangay = address.barangay.name;

        } catch (e) {
          console.log(e);
        }
      }
    const variantSummary = item => {
      const meta = [];
      for (let m of item.meta) {
        meta.push(`${m.name}: ${m.value}`);
      }
      return meta.join(', ');
    }  

    const discount = computed(() => {
      return (order.value.original_total || 0)
        - (order.value.total || 0)
        - (order.value.coupon_discount_total || 0)
        + (order.value.shipping_fee || 0);

    });

    onMounted(getData);

    Object.assign(exports, {
      order,
      discount,
      variantSummary
    });

    return exports;
  }
})
</script>
@endsection