import cartStore from "/js/store/cartStore.js";
if (typeof window.app === 'undefined') {
  const { createApp } = Vue;
  window.app = createApp({
    setup() {
      const exports = {
        cartState: cartStore.state
      };
      return exports;
    },
    components: {
      'p-toast': primevue.toast
    }
  });
}
window.vm = window.app
  .component('p-toast', primevue.toast)
  .use(primevue.config.default)
  .use(primevue.toastservice)
  .use({
    install: (app, options) => {
      app.mixin({
        mounted() {
          cartStore.getCart();
        }
      });
    }
  })
  .mount('#main');
