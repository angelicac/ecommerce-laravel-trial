const { createApp, onMounted, ref } = Vue;

import cartStore from '../store/cartStore.js';
import { itemBackgroundImage } from '../modules/useItemImage.js';

window.app = createApp({
  setup() {
    const exports = {
      cartState: cartStore.state,
      itemBackgroundImage
    };
    const items = ref([]);
    const gallery = ref([]);

    const getItems = async () => {
      const params = {
        page: 0,
        count: 12,
        sort: 'random'
      };

      try {
        const response = await axios.get('/items', { params });

        items.value = response.data.data;

      } catch (e) {
        console.log(e);
      }
    };

    Object.assign(exports, {
      items,
      gallery
    });

    onMounted(getItems);

    return exports;
  },
});