const { createApp, ref, onMounted, computed } = Vue;
const { useToast } = primevue.usetoast;
import useForm from "../modules/useForm.js";
import cartStore from "../store/cartStore.js";

window.app = createApp({
  setup() {
    const exports = {
      cartState: cartStore.state,
    };

    const toast = useToast();

    const formData = ref({
      name: "",
      email: "",
      subject: "",
      message: "",
    });

    const formRules = ref({
      name: [
        {
          type: "required",
        },
      ],
      email: [
        {
          type: "required",
        },
      ],
      subject: [
        {
          type: "required",
        },
      ],
      message: [
        {
          type: "required",
        },
      ],
    });

    const formResponse = ref("");
    const formSuccess = ref(false);
    const formErrors = ref({});

    const formFeedbackClasses = computed(() => {
      return formSuccess.value ? "text-success" : "text-danger";
    });

    const submit = async (event) => {
      useForm.formVerify(formData, formRules, formErrors);
      if (!useForm.formValid(formErrors)) return;
      try {
        const response = await axios.post('/contact-us', formData.value);
        formResponse.value = response.data.data;
        formData.value.name = "";
        formData.value.email = "";
        formData.value.subject = "";
        formData.value.message = "";
        toast.add({
          severity: "success",
          detail: "Message sent!",
          group: "br",
        });
      } catch (e) {
        console.log(e);
        toast.add({
          severity: "error",
          detail: e.response.data.message,
          group: "br",
        });
      }
    };

    exports.formData = formData;
    exports.formSuccess = formSuccess;
    exports.formResponse = formResponse;
    exports.formErrors = formErrors;
    exports.formFeedbackClasses = formFeedbackClasses;
    exports.submit = submit;

    return exports;
  },
  components: {
    'p-toast': primevue.toast
  }
});