const ShopMixin = {
  data() {
    return {
      cart: [],
      total: 0,
      cart_total: 0,
      total_quantity: 0,
      discount_total: 0,
      coupon: {},

      price_formatter: Intl.NumberFormat('en-PH', {
        style: 'currency',
        currency: 'PHP'
      }),

      // for tracking
      order_number: ''
    };
  },
  computed: {
    discount() {
      return (this.original_total || 0) -
        (this.total || 0) -
        (this.discount_total || 0) +
        (this.shipping_fee || 0);
    }
  },
  methods: {
    async get_cart() {
      console.log('get_cart');
      const response = await axios.get(`/cart`);
      this.cart = response.data.data;
      this.total = response.data.total;
      this.discount_total = response.data.discount_total;
      this.coupon = response.data.coupon || {};
      this.original_total = response.data.original_total;
      this.total_quantity = response.data.total_quantity;
      this.after_get_cart();
    },
    after_get_cart() { },
    site_url(url) {
      let unix_time = Math.floor((+new Date()) / 1000);
      return `${BASE_URL}${url}?v=${unix_time}`;
    },
    nl2br(str) {
      return str.replace(/(\\r)*\\n/g, '<br/ >');
    },
    gallery_image(item) {
      let url = '';
      if (item.image_file_name)
        url = item.image_file_name;
      // return `url(${this.site_url(url)}), url(${this.site_url('assets/images/placeholder.png')})`;
    },
    item_background_image(item) {
      let url = '';
      if (item.images && item.images.length)
        url = item.images[0].file_name;
      return `url('${this.site_url(url)}'), url(${this.site_url('/img/placeholder-300x300.png')})`;
    },
    item_background_image_api(item_id) {
      const url = `/api/item/image/${item_id}`;
      return `url(${this.site_url(url)}), url(${this.site_url('/img/placeholder-300x300.png')})`;
    },
    format_price(price) {
      return Intl.NumberFormat('en-PH', {
        style: 'currency',
        currency: 'PHP'
      }).format(price);
    }
  },
  created() {
    this.get_cart();
  },
};

const HelperMixin = {
  methods: {
    isEmptyObject(obj) {
      return Object.keys(obj).length === 0;
    }
  }
};

const ToastMixin = {
  data() {
    return {
      toast_mixin_data: {},
      toast_mixin_counter: 0,
      toast_mixin_toasts: {}
    };
  },
  methods: {
    make_toast(message, type) {
      const counter = this.toast_mixin_counter++;
      this.toast_mixin_data[counter] = {
        message: message,
        type: type,
      };
      const options = {
        autohide: !type,
      };
      this.$nextTick(() => {
        this.toast_mixin_toasts[counter] = new bootstrap.Toast(this.$refs[`toast-mixin-${counter}`][0], options);
        this.toast_mixin_toasts[counter].show();
      });
      return counter;
    },
  }
};

const ModalMixin = {
  data() {
    return {
      modals: {}
    };
  },
  methods: {
    createModal(ref) {
      if (!(ref in this.$refs)) {
        console.warn(`Cannot instantiate modal ${ref}, ref not found.`);
        return;
      }
      this.modals[ref] = new bootstrap.Modal(this.$refs[ref]);
    },
    showModal(ref) {
      if (!(ref in this.modals)) {
        console.warm(`Modal ${ref} not instantiated`);
        return;
      }
      this.modals[ref].show();
    },
    hideModal(ref) {
      if (!(ref in this.modals)) {
        console.warm(`Modal ${ref} not instantiated`);
        return;
      }
      this.modals[ref].hide();
    }
  }
};

const TimeoutMixin = {
  data() {
    return {
      intervals: {},
      timeouts: {},
    };
  },
  methods: {
    setInterval(handler, timeout, name) {
      const id = setInterval(handler, timeout);
      if (name) this.intervals[name] = id;
    },
    clearInterval(name) {
      if (this.intervals.hasOwnProperty(name))
        clearInterval(this.intervals[name]);
    },
    setTimeout(handler, timeout, name) {
      const id = setTimeout(handler, timeout);
      if (name) this.timeouts[name] = id;
    },
    clearTimeout(name) {
      if (this.timeouts.hasOwnProperty(name))
        clearTimeout(this.timeouts[name]);
    }
  }
};

const FormMixin = {
  data() {
    return {
      form_data: {},
      form_errors: {},
      form_rules: {},

      // constants
      genders: ['Male', 'Female', 'Unspecified']
    };
  },
  computed: {
    form_valid() {
      return this.isEmptyObject(this.form_errors);
    }
  },
  methods: {
    add_error(field, message) {
      if (field in this.form_errors)
        this.form_errors[field].push(message);
      else
        this.form_errors[field] = [message];
    },
    form_verify(form_object) {
      if (!form_object)
        form_object = 'form_data';
      this.form_errors = {};
      for (let key in this[form_object]) {
        if (key in this.form_rules) {
          for (let rule of this.form_rules[key]) {
            if (rule.type === 'required') {
              if (this[form_object][key].trim().length === 0)
                this.add_error(key, 'This field is required');
            } else if (rule.type === 'min_length') {
              if (this[form_object][key].trim().length < rule.value)
                this.add_error(key, `Please enter at least ${rule.value} characters`);
            } else if (rule.type === 'max_length') {
              if (this[form_object][key].trim().length < rule.value)
                this.add_error(key, `Please enter at most ${rule.value} characters`);
            } else if (rule.type === 'pattern') {
              let pattern;
              if (rule.value instanceof RegExp)
                pattern = rule.value;
              else if (typeof rule.value === "string")
                pattern = new RegExp(rule.value);
              else continue;
              if (!pattern.test(this[form_object][key]))
                this.add_error(key, `Input does not match pattern`);
            }
          }
        }
      }
    },
  }
};

const AddressMixin = {
  methods: {
    /**
     * 
     * @param {Array} addresses 
     * @returns 
     */
    async resolve_addresses(addresses) {
      try {
        const response = await axios.get(`${ADDRESS_API}/resolve`, {
          params: {
            regions: addresses.map(address => address.region_id),
            provinces: addresses.map(address => address.province_id),
            cities: addresses.map(address => address.city_id),
            barangay: addresses.map(address => address.barangay_id),
          }
        });
        return response.data.entities;
      } catch (e) {
        console.log(e);
        return [];
      }
    },
    /**
     * 
     * @param {Object} address 
     * @param {string} address.region_id
     * @param {string} address.province_id
     * @param {string} address.city_id
     * @param {string} address.barangay_id
     * @returns 
     */
    async resolve_address(address) {
      const addr = (await this.resolve_addresses([address]))[0] ?? null;
      return addr;
    }
  }
};

const MailingListMixin = {
  data() {
    return {
      mailing_list_message: '',
      mailing_list_class: '',
      mailing_list_submitting: false
    };
  },
  methods: {
    async subscribe_to_mailing_list(event) {
      this.mailing_list_message = '';
      const form_data = new FormData(event.target);
      form_data.append(CSRF_NAME, get_csrf());
      this.mailing_list_submitting = true;
      try {
        const response = await axios.post(event.target.action, form_data);
        if (response.status === 201)
          this.mailing_list_message = 'You have successfully subscribed';
        refresh_csrf_from_response(response.data);
      } catch (e) {
        console.log(e);
        if (e.response) switch (e.response.status) {
          case 409:
            this.mailing_list_message = 'You are already subscribed';
            break;
          default:
            this.mailing_list_message = e.response.data.message || 'There was an unknown error.';

        }
        this.mailing_list_class = 'text-danger';
        refresh_csrf_from_response(e.response.data);
      } finally {
        this.mailing_list_submitting = false;
      }

    }
  },
  mounted() {
    this.createModal('mailing_list_modal');
  }
};

const mixins = [ShopMixin, HelperMixin, TimeoutMixin, ModalMixin, MailingListMixin, ToastMixin];
