if (typeof window.app === 'undefined') {
  const { createApp } = Vue;
  window.app = createApp({});
}
window.vm = window.app
  .component('p-toast', primevue.toast)
  .use(primevue.config.default)
  .use(primevue.toastservice)
  .use(primevue.confirmationservice)
  .mount('#main');
