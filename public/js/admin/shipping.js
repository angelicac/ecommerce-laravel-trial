const { createApp, ref, onMounted } = Vue;
const { useConfirm } = primevue.useconfirm;

const App = {
  setup() {

    // Default Shipping Fee

    const default_shipping_fee = ref(0);
    const new_default_shipping_fee = ref(0);
    const editing_default_shipping_fee = ref(false);
    const saving_default_shipping_fee = ref(false);
    const confirm = useConfirm();

    const get_default_shipping_fee = async () => {
      const response = await axios.get('/admin/shipping/default');
      default_shipping_fee.value = response.data.data;
      new_default_shipping_fee.value = default_shipping_fee.value;
    };

    const update_default_shipping_fee = async () => {
      try {
        saving_default_shipping_fee.value = true;
        const response = await axios.post('/admin/shipping/default', {
          default_shipping_fee: new_default_shipping_fee.value
        });
        default_shipping_fee.value = new_default_shipping_fee.value;
        get_default_shipping_fee();
      } catch (e) {
        console.log(e);
      } finally {
        saving_default_shipping_fee.value = false;
        editing_default_shipping_fee.value = false;
      }
    };

    const confirm_update_default_shipping_fee = () => {
      confirm.require({
        message: 'Are you sure you want to update the default shipping fee?',
        header: 'Update confirmation',
        icon: 'pi pi-exclamation-triangle',
        accept() {
          console.log('updating shipping fee');
          update_default_shipping_fee();
        },
        reject() {
          console.log('a');
        }
      });
    };

    // Tabs

    const active_tab = ref(0);

    // Regions

    const regions = ref([]);
    const selected_region = ref(null);
    const region_shipping_fee = ref(0);
    const region_fees = ref([]);
    const get_regions = async () => {
      const response = await axios.get(`${ADDRESS_API}/regions`);
      regions.value = response.data.data.entities;
    };

    const save_region_shipping_fee = async () => {
      const response = await axios.post('/admin/shipping/fee', {
        region_id: selected_region.value.id,
        name: selected_region.value.name,
        shipping_fee: region_shipping_fee.value
      });
      get_region_shipping_fees();
    };

    const get_region_shipping_fees = async () => {
      const response = await axios.get('/admin/shipping/regions');
      region_fees.value = response.data.data;
    };

    // Provinces

    const provinces = ref([]);
    const selected_province = ref(null);
    const province_shipping_fee = ref(0);
    const province_fees = ref([]);
    const get_provinces = async () => {
      const response = await axios.get(`${ADDRESS_API}/provinces`);
      provinces.value = response.data.data.entities;
    };

    const save_province_shipping_fee = async () => {
      const response = await axios.post('/admin/shipping/fee', {
        province_id: selected_province.value.id,
        region_id: selected_province.value.data.region_id,
        name: selected_province.value.name,
        shipping_fee: province_shipping_fee.value
      });
      get_province_shipping_fees();
    };

    const get_province_shipping_fees = async () => {
      const response = await axios.get('/admin/shipping/provinces');
      province_fees.value = response.data.data;
    };

    // Cities

    const cities = ref([]);
    const cities_loading = ref(false);
    const selected_city = ref(null);
    const city_shipping_fee = ref(0);
    const city_fees = ref([]);
    const get_cities = _.debounce(async (event) => {
      cities_loading.value = true;
      const data = {
        params: null
      };
      if (event && event.value) {
        data.params = { q: event.value };
      }
      const response = await axios.get(`${ADDRESS_API}/cities`, data);
      cities.value = response.data.data.entities;
      cities_loading.value = false;
    }, 300);

    const cities_label = (city) => {
      return `${city.name} (${city.data.province})`;
    };

    const save_city_shipping_fee = async () => {
      const response = await axios.post('/admin/shipping/fee', {
        city_id: selected_city.value.id,
        province_id: selected_city.value.data.province_id,
        region_id: selected_city.value.data.region_id,
        name: cities_label(selected_city.value),
        shipping_fee: city_shipping_fee.value
      });
      get_city_shipping_fees();
    };

    const get_city_shipping_fees = async () => {
      const response = await axios.get('/admin/shipping/cities');
      city_fees.value = response.data.data;
    };

    onMounted(() => {
      get_default_shipping_fee();
      get_regions();
      get_provinces();
      get_cities();
      get_region_shipping_fees();
      get_province_shipping_fees();
      get_city_shipping_fees();
    });

    return {
      default_shipping_fee,
      new_default_shipping_fee,
      editing_default_shipping_fee,
      get_default_shipping_fee,
      update_default_shipping_fee,
      confirm_update_default_shipping_fee,
      saving_default_shipping_fee,

      active_tab,

      regions,
      selected_region,
      region_shipping_fee,
      save_region_shipping_fee,
      region_fees,

      provinces,
      selected_province,
      province_shipping_fee,
      save_province_shipping_fee,
      province_fees,

      cities,
      selected_city,
      city_shipping_fee,
      save_city_shipping_fee,
      city_fees,
      cities_loading,
      get_cities,
      cities_label
    };
  },
  components: {
    "p-confirmdialog": primevue.confirmdialog,
    "p-button": primevue.button,
    "p-tabview": primevue.tabview,
    "p-tabpanel": primevue.tabpanel,
    "p-dropdown": primevue.dropdown,
    "p-inputnumber": primevue.inputnumber,
  }
};
vm = createApp(App)
  .use(primevue.config.default)
  .use(primevue.confirmationservice)
  .mount('#main');