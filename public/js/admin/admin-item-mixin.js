const AdminItemMixin = {
  mixins: [HelperMixin, FormMixin],
  components: {
    'p-calendar': primevue.calendar,
    'p-column': primevue.column,
    'p-datatable': primevue.datatable,
    'p-inputtext': primevue.inputtext,
    'p-dropdown': primevue.dropdown,
    'p-inputnumber': primevue.inputnumber,
  },
  data() {
    return {

      id: 0,
      entity: {},
      controller: '',

      // form
      loading: false,
      submitting: false,
      display_saved: false,
      hide_saved_timeout_id: 0,

      // Place errors here
      form_errors: {

      },

      one2m: {},
      m2m: {},
    };
  },
  mounted() {
    this.get_data();
  },
  computed: {
    entity_with_relations() {
      const payload = { ...this.entity, one2m: {}, m2m: {} };
      for (key in this.one2m) {
        payload.one2m[key] = this.one2m[key];
      }
      for (key in this.m2m) {
        payload.m2m[key] = this.m2m[key];
      }
      return payload;
    },
    entity_payload() {
      return this.entity_with_relations;
    },
    item_url() {
      return `/admin/${this.controller}`;
    },
    store_url() {
      return `/admin/${this.controller}`;
    },
    update_url() {
      return `/admin/${this.controller}`;
    }
  },
  methods: {
    /**
     * Gets the entity identified by the id data property
     * 
     * @returns void
     */
    async get_data() {
      if (!this.item_url || !this.id) return;
      this.loading = true;
      let response;
      try {
        response = await axios.get(`${this.item_url}/${this.id}`);
        this.entity = response.data.data;
      } catch (e) {
        console.log(e);
      } finally {
        this.loading = false;
        this.after_get_data(response.data);
      }
    },
    /**
     * A hook that runs after get_data() finishes executing
     */
    after_get_data(data) { },
    /**
     * A hook that runs after update() finishes executing
     */
    after_update() { },
    /**
     * A hook that runs after submit() finishes executing
     */
    after_submit() { },
    /**
     * A hook that's meant to do custom verification on the entity
     * This should modify the form_errors object
     */
    verify_entity() { },
    // one2m functions
    add_to_one2m(key) {
      if (!(key in this.one2m)) this.one2m[key] = { data: [], delete: [] };
      this.one2m[key].data.push({});
    },
    add_to_m2m(key) {
      if (!(key in this.m2m)) this.m2m[key] = [];
      this.m2m[key].push({});
    },
    markForDeletion(id, relation) {
      const index = this.one2m[relation].delete.indexOf(id);
      if (index > -1) {
        this.one2m[relation].delete.splice(index, 1);
      } else {
        this.one2m[relation].delete.push(id);
      }
    },
    isMarkedForDeletion(id, relation) {
      return this.one2m[relation].delete.includes(id);
    },
    async submit(event) {
      this.form_errors = {};

      this.form_verify('entity');
      this.verify_entity();

      if (!this.isEmptyObject(this.form_errors)) {
        return;
      }

      this.submitting = true;

      const result = this.id ? await this.update() : await this.create();
      this.submitting = false;
      if (result)
        this.after_submit();
    },
    async update(form_data) {
      try {
        const response = await axios.put(`${this.update_url}/${this.id}`, { ...this.entity_payload });
        this.after_update(form_data);

        this.get_data();
        this.display_saved = true;
        this.hide_saved();

        return true;
      } catch (e) {
        console.log(e);
        if (e.response.data.errors) {
          for (const field in e.response.data.errors) {
            const errors = e.response.data.errors[field];
            if (Array.isArray(errors)) {
              this.form_errors[field] = errors;
            } else {
              this.form_errors[field] = [errors];
            }
          }
        }
        return false;
      }
    },
    async create() {

      try {
        const response = await axios.post(`${this.store_url}`, { ...this.entity_payload });
        console.log(response);
        if (this.item_url) {
          this.id = response.data.id;
          history.pushState({}, '', `${this.item_url}/${this.id}`);
        }

        this.get_data();
        this.display_saved = true;
        this.hide_saved();

        return true;
      } catch (e) {
        console.log(e);
        console.log("Error inserting");
        if (e.response.data.errors) {
          for (const field in e.response.data.errors) {
            const errors = e.response.data.errors[field];
            if (Array.isArray(errors)) {
              this.form_errors[field] = errors;
            } else {
              this.form_errors[field] = [errors];
            }
          }
        }

        return false;
      }
    },
    hide_saved() {
      clearInterval(this.hide_saved_timeout_id);
      this.hide_saved_timeout_id = setTimeout(this._hide_saved, 300000);
    },
    _hide_saved() {
      this.display_saved = false;
    },
    site_url(url) {
      let unix_time = Math.floor((+new Date()) / 1000);
      return `${BASE_URL}${url}?v=${unix_time}`;
    },
    nl2br(str) {
      return str.replace(/(\\r)*\\n/g, '<br/ >');
    }
  },
};