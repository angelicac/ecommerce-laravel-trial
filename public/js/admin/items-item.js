import useAdminShow, { components } from "../modules/useAdminShow.js";

const { createApp, ref, computed, onMounted, onBeforeMount, nextTick } = Vue;
window.app = createApp({
  setup() {
    const exports = {};

    const id = ref(ID);
    const controller = ref('items');
    const title = ref('Item');

    const one2m = ref({
      variants: {
        delete: [],
        data: [],
      },
      images: {
        delete: [],
        data: [],
      }
    });
    const m2m = ref({
      categories: []
    });
    const loading = ref(false);
    const meta = ref([]);

    const afterGetData = (responseData) => {
      m2m.value.categories = [];
      for (let category of responseData.data.categories) {
        m2m.value.categories.push({ category_id: category.pivot.category_id });
      }
      one2m.value.images = { delete: [], data: [] };
      one2m.value.variants = { delete: [], data: [] };
      for (let variant of responseData.data.variants) {
        const m = {};
        for (let meta of variant.meta) {
          m[meta.name] = meta.value;
        }
        variant.meta = m;
        one2m.value.variants.data.push(variant);
      }
      meta.value = responseData.meta;
    };

    Object.assign(exports, {
      id,
      loading,
      one2m,
      m2m,
      afterGetData,
      // insertViewLink: props.insertViewLink,
    });

    // Brands

    const brands = ref([]);

    const getBrands = async () => {
      loading.value = true;
      try {
        const response = await axios.get(`/admin/brands?rows=100`);
        brands.value = response.data.data;
      } catch (e) {
        console.log(e);
      } finally {
        loading.value = false;
      }
    };

    Object.assign(exports, { brands, getBrands });

    // Categories

    const options = ref([]);

    const search = _.debounce(async (input) => {
      loading.value = true;
      try {
        const response = await axios.get(`/admin/categories`, {
          params: {
            global_search: input,
            rows: 100,
          },
        });
        options.value = response.data.data;
      } catch (e) {
        console.log(e);
      } finally {
        loading.value = false;
      }
    }, 300);

    Object.assign(exports, { options, search });

    // Images

    const images = ref([]);
    const imagesToDelete = ref({});
    const imageFocused = ref("");
    const imagesOpen = ref(false);
    const imagePreviewModalOpen = ref(false);

    const imageInput = ref(false);

    const updateImages = (event) => {
      images.value = images.value.concat(Array.from(event.target.files));
    };

    const removeImage = (index) => {
      images.value.splice(index, 1);
    };

    const generateImage = (file) => {
      return URL.createObjectURL(file);
    };

    const viewImage = (url) => {
      imageFocused.value = "";
      imageFocused.value = url;
      imagePreviewModalOpen.value = true;
    };

    const imageSizesTotal = computed(() => {
      return images.value.reduce((val, image) => val + image.size, 0);
    });

    const uploadImages = async () => {
      console.log("Uploading images");
      const formData = new FormData();
      for (let image of images.value) {
        formData.append("images[]", image);
      }
      try {
        const response = await axios.post(
          `/admin/items/upload-images/${id.value}`,
          formData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        );
        images.value = [];
        imagesToDelete.value = {};
      } catch (e) {
        console.log(e);
      }
    };

    Object.assign(exports, {
      images,
      imagesToDelete,
      imageFocused,
      imagesOpen,
      updateImages,
      removeImage,
      generateImage,
      viewImage,
      imageSizesTotal,
      uploadImages,
      imagePreviewModalOpen,
    });

    // Variants

    const newMetaInput = ref(null);
    const newMeta = ref("");
    const variantsOpen = ref(true);
    const selectedRows = ref(null);

    const addNewVariant = () => {
      one2m.value.variants.data.push({
        sku: "",
        price: 0,
        stock: 0,
        weight: 0,
        meta: {},
      });
    };
    const addNewMeta = async () => {
      if (!newMeta.value) {
        return;
      }
      meta.value.push(newMeta.value);
      newMeta.value = "";
      await nextTick();
    };

    Object.assign(exports, {
      meta,
      newMetaInput,
      newMeta,
      variantsOpen,
      addNewMeta,
      addNewVariant,
      selectedRows,
    });

    // Dimensions
    const dimensionsRequired = ref(
      SHOP_DIMENSIONS_REQUIRED === "true"
    );
    exports.dimensionsRequired = dimensionsRequired;

    onMounted(() => {
      one2m.value.variants = {
        data: [{ sku: null, price: null, stock: null, weight: null, meta: {} }],
        delete: [],
      };
      search();
      getBrands();
    });

    onBeforeMount(() => {
      one2m.value.images = { delete: [], data: [] };
      one2m.value.variants = { deelete: {}, data: [] };
    });

    Object.assign(exports, useAdminShow({
      id,
      controller,
      title,
      one2m,
      m2m,
      afterGetData
    }));

    return exports;
  },
  components: {
    ...components,
    'p-dropdown': primevue.dropdown,
    'p-multiselect': primevue.multiselect,
    'p-datatable': primevue.datatable,
    'p-column': primevue.column,
    'p-columngroup': primevue.columngroup,
    'p-inputtext': primevue.inputtext,
    'p-inputnumber': primevue.inputnumber,
    'p-card': primevue.card,
  }
});

// const App = Vue.createApp({
//   mixins: [AdminItemMixin],
//   components: {
//     'p-column': primevue.column,
//     'p-datatable': primevue.datatable,
//     'p-multiselect': primevue.multiselect,
//     'p-dropdown': primevue.dropdown
//   },
//   data() {
//     return {
//       id: ID,
//       controller: 'items',
//       images: [],
//       images_to_delete: {},
//       image_modal: null,
//       image_focused: '',

//       options: [],
//       brands: [],

//       loading: false,

//       saving: false,
//       isVisible: [{
//         text: 'true',
//         value: '1'
//       },
//       {
//         text: 'false',
//         value: '0'
//       }
//       ],

//       meta: [],

//       images_open: false,

//       // variants
//       variants_open: true,
//       new_meta_input: '',
//       selected_rows: null,
//     };
//   },
//   computed: {
//     image_sizes_total() {
//       return this.images.reduce((val, image) => val + image.size, 0);
//     },
//   },
//   methods: {
//     generateImage(file) {
//       return URL.createObjectURL(file);
//     },
//     search(event) {
//       clearTimeout(this.timeout_id);
//       this.clearTimeout('search');
//       this.setTimeout(this.async_search.bind(null, event.target.input), 300, 'search');
//     },
//     async async_search(input) {
//       this.loading = true;
//       try {
//         const response = await axios.get(`/admin/categories`, {
//           params: {
//             global_search: input,
//             rows: 100
//           }
//         });
//         this.options = response.data.data;
//       } catch (e) {
//         console.log(e);
//       } finally {
//         this.loading = false;
//         this.category_key++;
//       }
//     },
//     async get_brands() {
//       this.loading = true;
//       try {
//         const response = await axios.get(`/admin/brands?rows=100`);
//         this.brands = response.data.data;
//       } catch (e) {
//         console.log(e);
//       } finally {
//         this.loading = false;
//       }
//     },
//     async after_submit() {
//       const form_data = new FormData();
//       for (let image of this.images) {
//         form_data.append('images[]', image);
//       }
//       this.saving = true;
//       try {
//         const response = await axios.post(`/admin/items/upload-images/${this.id}`,
//           form_data, {
//           headers: {
//             'Content-Type': 'multipart/form-data'
//           }
//         });
//         this.images = [];
//         this.images_to_delete = {};
//       } catch (e) {
//         console.log(e);
//       } finally {
//         this.saving = false;
//         this.get_data();
//       }
//     },
//     // images
//     updateImages(event) {
//       this.images = this.images.concat(Array.from(event.target.files));
//     },
//     removeImage(index) {
//       this.images.splice(index, 1);
//     },
//     viewImage(url) {
//       this.image_focused = '';
//       this.image_focused = url;
//       this.image_modal.show();
//     },
//     after_get_data(data) {
//       this.m2m.categories = [];
//       for (let category of this.entity.categories) {
//         this.m2m.categories.push({ category_id: category.pivot.category_id });
//       }
//       this.one2m.images = { delete: [], data: [] };
//       this.one2m.variants = {
//         delete: [],
//         data: [],
//       };
//       for (let variant of this.entity.variants) {
//         const m = {};
//         for (let meta of variant.meta) {
//           m[meta.name] = meta.value;
//         }
//         variant.meta = m;
//         this.one2m.variants.data.push(variant);
//       }
//       this.meta = data.meta;
//     },
//     async add_new_meta() {
//       if (!this.new_meta_input) {
//         return;
//       }
//       this.meta.push(this.new_meta_input);
//       this.new_meta_input = '';
//       await this.$nextTick();
//       this.$refs.new_meta_input.focus();
//     },
//     add_new_variant() {
//       this.one2m.variants.data.push({
//         sku: '',
//         price: '',
//         stock: 0,
//         meta: {}
//       });
//     },
//   },
//   mounted() {
//     // Add at least one variant
//     this.one2m.variants = {
//       data: [{ sku: null, price: null, stock: null, meta: {} }],
//       delete: []
//     };
//     this.image_modal = new bootstrap.Modal(this.$refs.imageModal);
//     this.async_search();
//     this.get_brands();
//   },
//   beforeMount() {
//     this.entity.brand_id = this.entity.brand_id || null;
//     this.one2m.images = { delete: [], data: [] };
//     this.one2m.variants = {
//       delete: [],
//       data: [],
//     };
//   }
// });