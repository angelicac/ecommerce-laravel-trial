const FILTER_DATE_RANGE = 'date_range';
const FILTER_NUMBER_RANGE = 'number_range';

const AdminListMixin = {
  components: {
    'p-column': primevue.column,
    'p-datatable': primevue.datatable,
    'p-inputtext': primevue.inputtext,
    'p-calendar': primevue.calendar,
    'p-inputnumber': primevue.inputnumber
  },
  computed: {
    linear_filters() {
      const filters = {};
      for (let filter in this.filters) {
        if (this.filters[filter].type === FILTER_DATE_RANGE) {
          if (!this.filters[filter].value || !this.filters[filter].value.length) continue;
          filters[`${filter}__ge`] = dayjs(this.filters[filter].value[0]).format('YYYY/MM/DD');
          if (!this.filters[filter].value.length > 1 || !this.filters[filter].value[1]) continue;
          filters[`${filter}__le`] = dayjs(this.filters[filter].value[1]).format('YYYY/MM/DD');
        } else if (this.filters[filter].type === FILTER_NUMBER_RANGE) {
          if (!this.filters[filter].value || !this.filters[filter].value.length) continue;
          filters[`${filter}__ge`] = this.filters[filter].value[0];
          if (!this.filters[filter].value.length > 1 || !this.filters[filter].value[1]) continue;
          filters[`${filter}__le`] = this.filters[filter].value[1];
        } else {
          filters[filter] = this.filters[filter].value;
        }
      }
      return filters;
    },
    ajax() {
      return `/admin/${this.controller}`;
    },
    item_url() {
      return `${BASE_URL}/admin/${this.controller}`;
    }
  },
  data() {
    return {
      FILTER_DATE_RANGE: FILTER_DATE_RANGE,
      FILTER_NUMBER_RANGE: FILTER_NUMBER_RANGE,
      columns: [],
      entities: [],
      loading: false,

      filters: {},

      global_search: '',

      totalRecords: 0,
      search: {
        page: 0,
        rows: 100,
        sortField: '',
        sortOrder: -1,
      },
    };
  },
  methods: {
    get_data: _.debounce(async function (event) {

      if (event) {
        // If the event has a page attribute, it means it is the page event
        // If it is not the page event, reset the page to 0
        this.search.page = event.page ?? 0;
        this.search.rows = event.rows ?? this.search.rows;
      }

      if (!this.ajax) return;
      this.loading = true;
      try {
        const response = await axios.get(`${this.ajax}`, {
          params: { ...this.search, ...this.linear_filters, global_search: this.global_search }
        });
        console.log(response);
        this.entities = response.data.data;
        this.totalRecords = response.data.totalRecords;
      } catch (e) {
        console.log(e);
      } finally {
        this.loading = false;
      }
    }, 300),
  },
  beforeMount() {
    for (let column of this.columns)
      if (column.filterable) {
        if (column.filterable === FILTER_DATE_RANGE) {
          this.filters[column.field] = {
            value: null,
            type: FILTER_DATE_RANGE
          };
        } else if (column.filterable === FILTER_NUMBER_RANGE) {
          this.filters[column.field] = {
            value: [],
            type: FILTER_NUMBER_RANGE
          };
        } else {
          this.filters[column.field] = {
            value: null,
            matchMode: primevue.api.FilterMatchMode.CONTAINS
          };
        }
      }
  },
  mounted() {
    this.get_data();
  },
  watch: {
    linear_filters() {
      this.get_data();
    },
    global_search() {
      this.get_data();
    }
  }
};