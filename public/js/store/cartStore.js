const { reactive, computed } = Vue;

export default {
  state: reactive({
    cart: [],
    total: 0,
    discountTotal: 0,
    coupon: {},
    originalTotal: 0,
    totalQuantity: 0,
  }),
  discount: computed(function () {
    return (this.original_total || 0) -
      (this.total || 0) -
      (this.discount_total || 0) +
      (this.shipping_fee || 0);
  }),
  async getCart() {
    const response = await axios.get(`/cart`);
    this.state.cart = response.data.data;
    this.state.total = response.data.total;
    this.state.discountTotal = response.data.discount_total;
    this.state.coupon = response.data.coupon || {};
    this.state.originalTotal = response.data.original_total;
    this.state.totalQuantity = response.data.total_quantity;
  }
};