export async function resolveAddresses(addresses) {
  try {
    const response = await axios.get(`${ADDRESS_API}/resolve`, {
      params: {
        regions: addresses.map(address => address.region_id),
        provinces: addresses.map(address => address.province_id),
        cities: addresses.map(address => address.city_id),
        barangay: addresses.map(address => address.barangay_id),
      }
    });
    return response.data.entities;
  } catch (e) {
    console.log(e);
    return [];
  }
}

export async function resolveAddress(address) {
  const addr = (await resolveAddresses([address]))[0] ?? null;
  return addr;
};

export default { resolveAddress, resolveAddresses };