export function asset(path) {
  return `${BASE_URL}/${path}`;
};
export function formatPrice(price) {
  return Intl.NumberFormat('en-PH', {
    style: 'currency',
    currency: 'PHP'
  }).format(price);
};
export function siteUrl(path) {
  let url = `${BASE_URL}${path}`;
  const unixTime = Math.floor((+new Date()) / 1000);
  url += `?v=${unixTime}`;
};

export default { asset, formatPrice, siteUrl };