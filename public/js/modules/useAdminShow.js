export const components = {
  'p-confirmdialog': primevue.confirmdialog
};

const {
  defineComponent,
  onBeforeMount,
  onMounted,
  watch,
  ref,
  computed,
} = Vue;
import useForm from "./useForm.js";
const { useConfirm } = primevue.useconfirm;
const { useToast } = primevue.usetoast;

const FILTER_DATE_RANGE = "date_range";
const FILTER_NUMBER_RANGE = "number_range";

/**
 * 
 * @param {Object} props 
 * @param {string|number} props.id
 * @param {string} props.title
 * @param {string} props.controller
 * @param {Function} props.afterGetData
 * @param {Object} props.entityWithRelations
 * @param {Object} props.entityPayload
 * @param {boolean} props.loading
 * @param {string} props.insertViewLink
 * @param {string} props.listViewLink
 * @param {Object} props.one2m
 * @param {Object} props.m2m
 * @param {boolean} props.showDefaultSubmitButton
 * @param {boolean} props.allowDelete
 * @param {Object} props.formRules
 * @param {Object} props.entity
 * @returns {Object}
 */
export default function useAdminShow(props) {
  // Defaults

  props.loading = props.loading ?? ref(false);
  props.showDefaultSubmitButton = props.showDefaultSubmitButton ?? ref(true);
  props.allowDelete = props.allowDelete ?? ref(false);

  const exports = {};
  // Data Retrieval

  const entity = props.entity ?? ref({});
  const loading = props.loading.value || ref(false);
  const itemUrl = computed(() => {
    return `/admin/${props.controller.value}`;
  });

  const getData = async () => {
    if (!itemUrl.value || !props.id.value) return;
    loading.value = true;
    let response;
    try {
      response = await axios.get(`${itemUrl.value}/${props.id.value}`);
      entity.value = response.data.data;
    } catch (e) {
      console.log(e);
    } finally {
      loading.value = false;
      if (props.afterGetData) props.afterGetData(response.data);
    }
  };

  Object.assign(exports, {
    entity,
    loading,
    itemUrl,
    getData
  });

  // Form Input

  const addToOne2m = (key) => {
    if (!(key in one2m.value)) one2m.value[key] = { data: [], delete: [] };
    one2m.value[key].data.push({});
  };

  const addToM2m = (key) => {
    if (!(key in props.m2m.value)) props.m2m.value[key] = [];
    props.m2m.value[key].push({});
  };

  const markForDeletion = (id, relation) => {
    const index = props.one2m.value[relation].delete.indexOf(id);
    if (index > -1) {
      props.one2m.value[relation].delete.splice(index, 1);
    } else {
      props.one2m.value[relation].delete.push(id);
    }
  };

  const isMarkedForDeletion = (id, relation) => {
    return props.one2m.value[relation].delete.includes(id);
  };

  exports.addToOne2m = addToOne2m;
  exports.addToM2m = addToM2m;
  exports.markForDeletion = markForDeletion;
  exports.isMarkedForDeletion = isMarkedForDeletion;
  exports.one2m = props.one2m;
  exports.m2m = props.m2m;
  exports.showDefaultSubmitButton = props.showDefaultSubmitButton;

  // Submission

  const formErrors = ref({});
  const submitting = ref(false);
  const displaySaved = ref(false);

  const entityWithRelations =
    props.entityWithRelations ||
    computed(() => {
      const payload = { ...entity.value, one2m: {}, m2m: {} };
      if (props.one2m) for (let key in props.one2m.value) {
        payload.one2m[key] = props.one2m.value[key];
      }
      if (props.m2m) for (let key in props.m2m.value) {
        payload.m2m[key] = props.m2m.value[key];
      }
      return payload;
    });

  const entityPayload =
    props.entityPayload ||
    computed(() => {
      return entityWithRelations.value;
    });

  const updateUrl = computed(() => {
    return `/admin/${props.controller.value}`;
  });

  const hideSavedTimeoutId = ref(0);
  const hideSaved = () => {
    clearInterval(hideSavedTimeoutId.value);
    hideSavedTimeoutId.value = setTimeout(() => {
      displaySaved = false;
    }, 300000);
  };

  const update = async () => {
    try {
      const response = await axios.put(`${updateUrl.value}/${props.id.value}`, {
        ...entityPayload.value,
      });

      getData();
      displaySaved.value = true;
      hideSaved();

      return true;
    } catch (e) {
      console.log(e);
      if (e.response.data.errors) {
        for (const field in e.response.data.errors) {
          const errors = e.response.data.errors[field];
          if (Array.isArray(errors)) {
            formErrors.value[field] = errors;
          } else {
            formErrors.value[field] = [errors];
          }
        }
      }
      return false;
    }
  };

  const storeUrl = computed(() => {
    return `/admin/${props.controller.value}`;
  });
  const create = async () => {
    try {
      const response = await axios.post(`${storeUrl.value}`, {
        ...entityPayload.value,
      });
      props.id.value = response.data.id;

      history.pushState({}, '', `${itemUrl.value}/${props.id.value}`);

      getData();
      displaySaved.value = true;
      hideSaved();

      return true;
    } catch (e) {
      console.log(e);
      console.log("Error inserting");
      if (e.response.data.errors) {
        for (const field in e.response.data.errors) {
          const errors = e.response.data.errors[field];
          if (Array.isArray(errors)) {
            formErrors.value[field] = errors;
          } else {
            formErrors.value[field] = [errors];
          }
        }
      }
      return false;
    }
  };

  const submit = async (event) => {
    formErrors.value = {};
    if (props.formRules)
      useForm.formVerify(entity, props.formRules, formErrors);

    if (!useForm.formValid(formErrors)) return;

    submitting.value = true;

    const result = props.id.value ? await update() : await create();

    submitting.value = false;
    if (result && props.afterSubmit) {
      const after = props.afterSubmit();
      await Promise.resolve(after);
      getData();
    }
  };

  // Delete

  const confirm = useConfirm();
  const toast = useToast();

  const deleteUrl = computed(() => {
    return `/admin/${props.controller.value}`;
  });

  const deleteRequest = async () => {
    try {
      submitting.value = true;
      const response = await axios.delete(`${deleteUrl.value}/${props.id}`);
      Inertia.get(route(props.listViewLink));
      toast.add({
        detail: "Deleted",
        group: "br",
      });
    } catch (e) {
      console.log(e);
      toast.add({
        severity: "error",
        detail: "There was an error deleting the object",
        group: "br",
      });
    } finally {
      submitting.value = false;
    }
  };

  const confirmDelete = () => {
    confirm.require({
      message: "Are you sure you want to delete this object?",
      header: "Delete Confirmation",
      icon: "pi pi-exclamation-triangle",
      accept: deleteRequest,
    });
  };

  Object.assign(exports, {
    confirmDelete,
    allowDelete: props.allowDelete,
  });

  watch(() => props.id, getData);

  exports.formErrors = formErrors;
  exports.submit = submit;
  exports.submitting = submitting;
  exports.displaySaved = displaySaved;

  exports.title = props.title;

  onMounted(() => {
    getData();
  });

  return exports;
}
