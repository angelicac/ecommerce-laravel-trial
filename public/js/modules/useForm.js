const { isEmpty } = _;


export function addError(formErrors, field, message) {
  if (field in formErrors) {
    formErrors.value[field].push(message);
  } else {
    formErrors.value[field] = [message];
  }
};
export function formValid(formErrors) {
  return isEmpty(formErrors.value);
};
export function formVerify(formData, formRules, formErrors) {
  formErrors.value = {};
  for (let key in formData.value) {
    if (key in formRules.value) {
      for (let rule of formRules.value[key]) {
        const value = formData.value[key] || '';
        if (rule.type === 'required') {
          if (value.trim().length === 0)
            addError(formErrors, key, 'This field is required');
        } else if (rule.type === 'min_length') {
          if (value.trim().length < rule.value)
            addError(formErrors, key, `Please enter at least ${rule.value} characters`);
        } else if (rule.type === 'max_length') {
          if (value.trim().length < rule.value)
            addError(formErrors, key, `Please enter at most ${rule.value} characters`);
        } else if (rule.type === 'pattern') {
          let pattern;
          if (rule.value instanceof RegExp)
            pattern = rule.value;
          else if (typeof rule.value === "string")
            pattern = new RegExp(rule.value);
          else continue;
          if (!pattern.test(value))
            addError(formErrors, key, `Input does not match pattern`);
        }
      }
    }
  }
};

export const genders = ['Male', 'Female', 'Unspecified'];

export default { addError, formValid, formVerify, genders };