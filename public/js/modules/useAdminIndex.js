export const components = {
  'p-column': primevue.column,
  'p-datatable': primevue.datatable,
  'p-inputtext': primevue.inputtext,
  'p-calendar': primevue.calendar,
  'p-inputnumber': primevue.inputnumber
};

const { onBeforeMount, onMounted, watch, ref, computed, } = Vue;
const { useConfirm } = primevue.useconfirm;
const { useToast } = primevue.usetoast;

/**
 * 
 * @param {Object} props 
 * @param {string} props.controller
 * @param {Array} props.columns
 * @param {string} props.title
 * @param {boolean} props.showAdd
 * @param {string} props.insertViewLink
 * @param {string} props.insertViewLinkText
 * @param {boolean} props.allowDelete
 * @param {string} props.listViewLink
 * @returns {Object}
 */
export default function useAdminIndex(props) {
  // Data Retrieval

  const entities = ref([]);
  const totalRecords = ref(0);
  const loading = ref(false);

  const globalSearch = ref("");

  const ajax = computed(() => {
    return `/admin/${props.controller.value}`;
  });
  const deleted = computed(() => {
    return new URLSearchParams(location.search).has("deleted");
  });

  const itemUrl = computed(() => {
    return `${BASE_URL}/admin/${props.controller.value}`;
  });

  const search = ref({
    page: 0,
    rows: 100,
    sortField: "",
    sortOrder: -1,
  });

  const filters = ref({});

  const linearFilters = computed(() => {
    const linearFilter = {};
    for (let filter in filters.value) {
      if (filters.value[filter].type === FILTER_DATE_RANGE) {
        if (
          !filters.value[filter].value ||
          !filters.value[filter].value.length
        )
          continue;
        linearFilter[`${filter}__ge`] = dayjs(
          filters.value[filter].value[0]
        ).format("YYYY/MM/DD");
        if (
          !filters.value[filter].value.length > 1 ||
          !filters.value[filter].value[1]
        )
          continue;
        linearFilter[`${filter}__le`] = dayjs(
          filters.value[filter].value[1]
        ).format("YYYY/MM/DD");
      } else if (filters.value[filter].type === FILTER_NUMBER_RANGE) {
        if (
          !filters.value[filter].value ||
          !filters.value[filter].value.length
        )
          continue;
        linearFilter[`${filter}__ge`] = filters.value[filter].value[0];
        if (
          !filters.value[filter].value.length > 1 ||
          !filters.value[filter].value[1]
        )
          continue;
        linearFilter[`${filter}__le`] = filters.value[filter].value[1];
      } else {
        linearFilter[filter] = filters.value[filter].value;
      }
    }
    return linearFilter;
  });

  const getData = _.debounce(async (event) => {
    if (event) {
      // If the event has a page attribute, it means it is the page event
      // If it is not the page event, reset the page to 0
      search.value.page = event.page ?? 0;
      search.value.rows = event.rows ?? search.value.rows;
    }

    if (!ajax.value) return;
    loading.value = true;
    try {
      const params = {
        ...search.value,
        ...linearFilters.value,
        global_search: globalSearch.value,
      };
      if (deleted.value) {
        params["deleted"] = true;
      }
      const response = await axios.get(`${ajax.value}`, { params });

      entities.value = response.data.data;
      totalRecords.value = response.data.totalRecords;
    } catch (e) {
      console.log(e);
    } finally {
      loading.value = false;
    }
  }, 300);

  // Restore

  const confirm = useConfirm();
  const toast = useToast();

  const restoreUrl = computed(() => {
    return `/admin/${props.controller}/restore`;
  });

  const restoreRequest = async (id) => {
    try {
      loading.value = true;
      const response = await axios.patch(`${restoreUrl.value}/${id}`);
      toast.add({ detail: "Restored", group: "br" });
      await getData();
    } catch (e) {
      console.log(e);
      toast.add({
        severity: "error",
        detail: "There was an error restoring the object",
        group: "br",
      });
      loading.value = false;
    }
  };

  const confirmRestore = (id) => {
    confirm.require({
      message: "Are you sure you want to restore this object?",
      header: "Restore Confirmation",
      icon: "pi pi-pi-exclamation-triangle",
      accept: restoreRequest.bind(null, id),
    });
  };

  onBeforeMount(() => {
    for (let column of props.columns.value)
      if (column.filterable) {
        if (column.filterable === FILTER_DATE_RANGE) {
          filters.value[column.field] = {
            value: null,
            type: FILTER_DATE_RANGE,
          };
        } else if (column.filterable === FILTER_NUMBER_RANGE) {
          filters.value[column.field] = {
            value: [],
            type: FILTER_NUMBER_RANGE,
          };
        } else {
          filters.value[column.field] = {
            value: null,
            matchMode: primevue.api.FilterMatchMode.CONTAINS,
          };
        }
      }
  });

  onMounted(() => {
    getData();
  });

  watch(linearFilters, getData);
  watch(globalSearch, getData);
  return {
    entities,
    totalRecords,
    loading,
    globalSearch,
    filters,
    search,
    getData,
    itemUrl,
    deleted,
    confirmRestore,
    ...props
  };
}