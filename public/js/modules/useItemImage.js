import { siteUrl } from "./useHelpers.js";

export function itemBackgroundImage(item) {
  let url = '';
  if (item.images && item.images.length)
    url = item.images[0].file_name;
  return `url('${url}'), url(${siteUrl('/img/placeholder-300x300.png')})`;
}

export function itemBackgroundImageApi(itemId) {
  const url = `/api/item/image/${itemId}`;
  return `url(${url}), url(${siteUrl('/img/placeholder-300x300.png')})`;
}

export default { itemBackgroundImage, itemBackgroundImageApi };