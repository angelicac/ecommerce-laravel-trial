const { ref, createApp, watch, nextTick } = Vue;
import cartStore from "../store/cartStore.js";
const { useToast } = primevue.usetoast;
import { itemBackgroundImageApi } from "../modules/useItemImage.js";
import { formatPrice } from "../modules/useHelpers.js";

window.app = createApp({
  setup() {
    const exports = {
      cartState: cartStore.state,
      formatPrice: formatPrice
    };
    const toast = useToast();

    exports.itemBackgroundImageApi = itemBackgroundImageApi;
    // Variants

    const variantSummary = (item) => {
      const meta = [];
      for (let m of item.meta) {
        meta.push(`${m.name}: ${m.value}`);
      }
      return meta.join(", ");
    };

    exports.variantSummary = variantSummary;

    // Cart functi9ons

    const updating = ref(false);

    const add = async (item, quantity) => {
      updating.value = true;
      try {
        const response = await axios.post(`/cart/add/${item.sku}/${quantity}`);

        item.pivot.quantity += quantity;

        cartStore.getCart();
      } catch (e) {
        console.log(e);
        if (e.response.data.errors) {
          for (let error of e.response.data.errors) {
            toast.add({ severity: "error", detail: error, group: "br" });
          }
        }
      } finally {
        updating.value = false;
      }
    };

    const subtract = async (item, quantity) => {
      updating.value = true;
      try {
        const response = await axios.post(
          `/cart/remove/${item.sku}/${quantity}`
        );

        item.pivot.quantity -= quantity;

        cartStore.getCart();
      } catch (e) {
        console.log(e);
        if (e.response.data.errors) {
          for (let error of e.response.data.errors) {
            toast.add({ severity: "error", detail: error, group: "br" });
          }
        }
      } finally {
        updating.value = false;
      }
    };
    const remove = async (item) => {
      updating.value = true;
      try {
        const response = await axios.post(`/cart/remove-all/${item.sku}`);
        cartStore.getCart();
      } catch (e) {
        console.log(e);
      } finally {
        updating.value = false;
      }
    };

    exports.add = add;
    exports.subtract = subtract;
    exports.remove = remove;
    exports.updating = updating;

    // Coupons

    const couponCode = ref("");
    const couponApplied = ref(false);
    const showCouponMessage = ref(false);

    const applyCoupon = async (event) => {
      const formData = new FormData(event.target);
      try {
        const response = await axios.post(`/cart/set-coupon`, formData);
        couponApplied.value = true;
        cartStore.getCart();
      } catch (e) {
        console.log(e);
        couponApplied.value = false;
      } finally {
        showCouponMessage.value = true;
        nextTick(() => {
          setInterval(() => {
            showCouponMessage.value = false;
          }, 3000);
        });
      }
    };

    exports.couponCode = couponCode;
    exports.couponApplied = couponApplied;
    exports.showCouponMessage = showCouponMessage;
    exports.applyCoupon = applyCoupon;

    Object.assign(exports, { cartState: cartStore.state });
    exports.cartState.discount = cartStore.discount;
    return exports;
  },
});