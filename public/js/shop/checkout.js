const form_button = document.querySelector('form button[type="submit"]');

const { ref, createApp, computed, onMounted, watch } = Vue;
import cartStore from "../store/cartStore.js";
import useForm from "../modules/useForm.js";
import { formatPrice } from "../modules/useHelpers.js";

window.app = createApp({
  setup() {
    const exports = {
      cartState: cartStore.state,
      formatPrice
    };

    // Captcha

    exports.sitekey = SITE_KEY;
    exports.recaptchaEnabled = RECAPTCHA_ENABLED;

    // Cart

    exports.cart = cartStore.state;
    exports.cart.discount = cartStore.discount;

    // Form

    const formData = ref({
      first_name: "",
      last_name: "",
      email: "",
      contact_number: "",
      street: "",

      region_id: "",
      province_id: "",
      city_id: "",
      barangay_id: "",
      zip_code: "",
      captcha: "",

      shipping_fee: "",
    });

    const formRules = ref({
      first_name: [{ type: "required" }],
      last_name: [{ type: "required" }],
      contact_number: [{ type: "required" }],
      email: [{ type: "required" }],
      street: [{ type: "required" }],
      region_id: [{ type: "required" }],
      province_id: [{ type: "required" }],
      city_id: [{ type: "required" }],
      barangay_id: [{ type: "required" }],
      zip_code: [{ type: "required" }],
    });

    if (exports.recaptchaEnabled) {
      formRules.value.captcha = [{ type: "required" }];
    }

    const formErrors = ref({});

    const getCustomerData = async () => {
      try {
        const response = await axios.get("user");
        const fields = ["first_name", "last_name", "email", "contact_number"];
        for (let field of fields) formData.value[field] = response.data[field];
      } catch (e) {
        console.log(e);
      }
    };

    Object.assign(exports, {
      formData,
      formErrors,
      getCustomerData,
    });

    // Shipping

    const shippingFee = ref(0);
    const totalWithShipping = computed(() => {
      return exports.cart.total + shippingFee.value;
    });
    const getShippingFee = async () => {
      try {
        const response = await axios.get("/cart/calculate-shipping-fee", {
          params: {
            region_id: formData.value.region_id,
            province_id: formData.value.province_id,
            city_id: formData.value.city_id,
            barangay_id: formData.value.barangay_id,
            shipping_fee: formData.value.shipping_fee,
          },
        });
        shippingFee.value = response.data.data;
      } catch (e) {
        console.log(e);
      }
    };

    const shippingMethods = ref([]);
    const getShippingMethods = async () => {
      try {
        const response = await axios.get("/cart/shipping-methods", {
          params: {
            region_id: formData.value.region_id,
            province_id: formData.value.province_id,
            city_id: formData.value.city_id,
            barangay_id: formData.value.barangay_id,
          },
        });
        shippingMethods.value = response.data.data;
      } catch (e) {
        console.log(e);
      }
    };

    const isShippingMethodDisabled = (shippingMethod) => {
      if (
        shippingMethod.shipping_method_id ===
        shippingMethodsMap.FREE_SHIPPING
      ) {
        return exports.cart.total < shippingMethod.minimum_price;
      }
      return false;
    };

    const shippingMethodText = (shippingMethod) => {
      if (
        shippingMethod.shipping_method_id === shippingMethodsMap.FLAT_RATE
      ) {
        return `₱${shippingMethod.fee}`;
      } else if (
        shippingMethod.shipping_method_id ===
        shippingMethodsMap.FREE_SHIPPING
      ) {
        return `Minimum spend of ₱${shippingMethod.minimum_price} required`;
      } else if (
        shippingMethod.shipping_method_id === shippingMethodsMap.CALCULATED
      ) {
        return `Calculated based on your order`;
      }
    };

    Object.assign(exports, {
      shippingFee,
      totalWithShipping,

      shippingMethods,
      isShippingMethodDisabled,
      shippingMethodText,
    });

    // Addresses

    const regions = ref([]);
    const provinces = ref([]);
    const cities = ref([]);
    const barangay = ref([]);
    const addresses = ref([]);

    const loading = ref(false);

    const getRegions = async () => {
      try {
        const response = await axios.get(
          `${ADDRESS_API}/regions`
        );
        regions.value = response.data.data.entities;
      } catch (e) {
        console.log(e);
      }
    };

    const getProvinces = async () => {
      try {
        const response = await axios.get(
          `${ADDRESS_API}/provinces/${formData.value.region_id}`
        );
        provinces.value = response.data.data.entities;
      } catch (e) {
        console.log(e);
      }
    };
    const getCities = async (event) => {
      let search = "";
      if (event && event.value) search = event.value;
      if (event && search === "") return;
      loading.value = true;
      const response = await axios.get(
        `${ADDRESS_API}/cities/${formData.value.province_id}`,
        {
          params: {
            q: search,
          },
        }
      );
      cities.value = response.data.data.entities;
      loading.value = false;
    };
    const getBarangay = async (event) => {
      if (!formData.value.city_id) return;
      let search = "";
      if (event && event.value) search = event.value;
      if (event && search === "") return;
      const response = await axios.get(
        `${ADDRESS_API}/barangay/${formData.value.city_id}`,
        {
          params: {
            q: search,
          },
        }
      );
      barangay.value = response.data.data.entities;
    };

    const addressesResolved = ref(false);

    const getCustomerAddresses = async () => {
      try {
        const response = await axios.get("/customer/addresses");
        addresses.value = response.data.data;
      } catch (e) {
        console.log(e);
      }
    };

    const getAddresses = async () => {
      await getCustomerAddresses();
      try {
        const response = await axios.get(
          `${ADDRESS_API}/resolve`,
          {
            params: {
              regions: addresses.value.map((address) => address.region_id),
              provinces: addresses.value.map((address) => address.province_id),
              cities: addresses.value.map((address) => address.city_id),
              barangay: addresses.value.map((address) => address.barangay_id),
            },
          }
        );
        for (let i = 0; i < response.data.entities.length; i++) {
          const addr = response.data.entities[i];
          addresses.value[i].region = addr.region.name;
          addresses.value[i].province = addr.province.name;
          addresses.value[i].city = addr.city.name;
          addresses.value[i].barangay = addr.barangay.name;
        }
        addressesResolved.value = true;
      } catch (e) {
        console.log(e);
      }
    };

    const addressFilled = computed(() => {
      return (
        formData.value.region_id &&
        formData.value.province_id &&
        formData.value.city_id &&
        formData.value.barangay_id
      );
    });

    const region_id = computed(() => formData.value.region_id);
    const province_id = computed(() => formData.value.province_id);
    const city_id = computed(() => formData.value.city_id);
    const barangay_id = computed(() => formData.value.barangay_id);

    const setAddress = (index) => {
      if (!addressesResolved.value) return;
      const address = addresses.value[index];
      console.log(address);

      formData.value.region_id = address.region_id;
      provinces.value = [{ name: address.province, id: address.province_id }];

      formData.value.province_id = address.province_id;
      cities.value = [{ name: address.city, id: address.city_id }];

      formData.value.city_id = address.city_id;
      barangay.value = [{ name: address.barangay, id: address.barangay_id }];

      formData.value.barangay_id = address.barangay_id;
      formData.value.street = address.street;
      formData.value.zip_code = address.zip_code;
    };

    watch(region_id, () => getProvinces());
    watch(province_id, () => getCities());
    watch(city_id, () => getBarangay());
    watch(addressFilled, () => getShippingMethods());

    const shipping_fee = computed(() => formData.value.shipping_fee);
    watch(shipping_fee, getShippingFee);

    Object.assign(exports, {
      regions,
      provinces,
      cities,
      barangay,

      loading,

      getRegions,
      getProvinces,
      getCities,
      getBarangay,

      addresses,
      getCustomerAddresses,
      setAddress,
      addressesResolved,
      addressFilled,
    });

    // Recaptcha

    const onRecaptchaVerified = (response) => {
      formData.value.captcha = response;
    };

    exports.onRecaptchaVerified = onRecaptchaVerified;

    // Checkout

    const checkout = async (event) => {
      useForm.formVerify(formData, formRules, formErrors);

      if (!useForm.formValid(formErrors)) return;

      try {
        const response = await axios.post("/cart/checkout", formData.value);
        if (response.status === 201) {
          const number = response.data.number;
          window.location.href = `${BASE_URL}/order/track/${number}`;
        }
      } catch (e) {
        console.log(e);
        if (e.response.data.errors) {
          for (const field in e.response.data.errors) {
            const errors = e.response.data.errors[field];
            if (Array.isArray(errors)) {
              this.form_errors[field] = errors;
            } else {
              this.form_errors[field] = [errors];
            }
          }
        }
      }
    };

    onMounted(() => {
      getRegions();
      getAddresses();
      getCustomerData();
    });

    Object.assign(exports, {
      shippingFee,
      checkout,
    });

    return exports;
  },
  components: {
    'p-dropdown': primevue.dropdown,
  },
});