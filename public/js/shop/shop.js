const { createApp, onMounted, ref, watch, computed } = Vue;
const { tree, slider } = primevue;

import { itemBackgroundImage } from "../modules/useItemImage.js";
import cartStore from '../store/cartStore.js';


window.app = createApp({
  setup() {
    const exports = {
      cartState: cartStore.state
    };

    exports.itemBackgroundImage = itemBackgroundImage;

    const items = ref([]);
    exports.items = items;

    // Inputs

    const priceRange = ref([0, 100000]);
    exports.priceRange = priceRange;

    // Category Select

    const categories = ref(null);
    const selectedCategory = ref(0);
    const expandedCategories = ref({});
    const selectedKeys = ref({});
    const categoriesLoading = ref(false);

    const categoriesToNodeList = (categories) => {
      const nodes = [];
      for (let category of categories) {
        const node = {
          key: category.id,
          label: category.name,
          data: category.name,
          leaf: category.subcategories_count === 0,
        };
        if (category.subcategories) {
          node.children = categoriesToNodeList(category.subcategories);
        }
        nodes.push(node);
      }
      return nodes;
    };

    const categoriesRequest = async (parent_id) => {
      try {
        const response = await axios.get("/categories", {
          params: {
            sort: "name",
            sortOrder: "asc",
            parent_id: parent_id,
            category_id: selectedCategory.value,
          },
        });
        if (response.data.path) {
          for (let category_id of response.data.path) {
            expandedCategories.value[category_id] = true;
          }
        }
        return categoriesToNodeList(response.data.data);
      } catch (e) {
        console.log(e);
      }
    };

    const getCategories = async () => {
      categoriesLoading.value = true;
      categories.value = await categoriesRequest();
      categoriesLoading.value = false;
    };

    const selectCategory = (node) => {
      selectedCategory.value = node.key;
    };

    const expandCategory = async (node) => {
      if (!node.children) {
        categoriesLoading.value = true;
        node.children = await categoriesRequest(node.key);
        categoriesLoading.value = false;
      }
    };

    exports.categories = categories;
    exports.selectedCategory = selectedCategory;
    exports.expandedCategories = expandedCategories;
    exports.selectedKeys = selectedKeys;
    exports.categoriesLoading = categoriesLoading;
    exports.selectCategory = selectCategory;
    exports.expandCategory = expandCategory;

    // Filters

    const currentPage = ref(0);
    const totalItemCount = ref(0);
    const perPage = ref(12);

    const pages = computed(() => {
      return Math.ceil(totalItemCount.value / perPage.value);
    });

    const sort = ref("");
    const search = ref("");
    const selectedItemIndex = ref(-1);

    const turnPage = (pageNum) => {
      let pagesToTurn = 0;
      if (pageNum === 1 || pageNum === -1) {
        pagesToTurn = pageNum;
      }
      if (
        0 <= currentPage.value + pagesToTurn &&
        currentPage.value + pagesToTurn <= pages.value - 1
      )
        currentPage.value += pagesToTurn;
    };

    const loading = ref(false);

    const params = computed(() => {
      return new URLSearchParams({
        search: search.value,
        page: currentPage.value + 1,
        sort: sort.value,
        category: selectedCategory.value,
        price_min: priceRange.value[0],
        price_max: priceRange.value[1],
      }).toString();
    });

    const populateSearch = () => {
      const url = new URL(location.href);
      currentPage.value = (url.searchParams.get("page") ?? 1) - 1;
      search.value = url.searchParams.get("search") ?? "";
      const category = url.searchParams.get("category");
      selectedCategory.value = category ? parseInt(category) : 0;
      sort.value = url.searchParams.get("sort") ?? "";
      const price_min = url.searchParams.get("price_min");
      priceRange.value[0] = price_min
        ? parseInt(price_min)
        : priceRange.value[0];
      const price_max = url.searchParams.get("price_max");
      priceRange.value[1] = price_max
        ? parseInt(price_max)
        : priceRange.value[1];
    };

    const getData = _.debounce(async () => {
      loading.value = true;
      history.pushState(
        {},
        "",
        `${location.origin}${location.pathname}?${params.value}${location.hash}`
      );
      const parameters = {
        page: currentPage.value,
        count: perPage.value,
        price__ge: priceRange.value[0],
        price__le: priceRange.value[1],
      };
      if (selectedCategory.value)
        parameters.category_id = selectedCategory.value;
      if (sort.value) {
        parameters.sort = sort.value;
      }
      if (search.value) {
        parameters.name_like = search.value;
      }

      try {
        const response = await axios.get("/items", { params: parameters });
        items.value = response.data.data;
        totalItemCount.value = response.data.total;
      } catch (e) {
        console.log(e);
      } finally {
        loading.value = false;
      }
    }, 300);

    const resetPagination = () => {
      currentPage.value = 0;
      getData();
    };

    watch(selectedCategory, resetPagination);
    watch(sort, resetPagination);
    watch(search, resetPagination);
    watch(currentPage, getData);

    watch(priceRange, resetPagination, { deep: true });

    Object.assign(exports, {
      sort,
      search,
      selectedItemIndex,
      pages,
      currentPage,
      turnPage,
      loading,
    });

    onMounted(() => {
      for (let i = 0; i < perPage.value; i++) {
        items.value.push({ id: -i });
      }
      populateSearch();
      getData();
      getCategories();
      if (selectedCategory.value) {
        selectedKeys.value[selectedCategory.value] = true;
      }
    });

    return exports;
  },
  components: {
    'p-tree': tree,
    'p-slider': slider,
  }
});