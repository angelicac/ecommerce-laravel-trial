const { ref, computed, onMounted, watch, createApp } = Vue;
const { useToast } = primevue.usetoast;
import cartStore from "/js/store/cartStore.js";

window.app = createApp({
  components: {
    'vueper-slides': vueperslides.VueperSlides,
    'vueper-slide': vueperslides.VueperSlide,
    'p-dialog': primevue.dialog
  },
  setup() {
    const exports = {
      cartState: cartStore.state
    };

    exports.itemId = ref(ID);

    const toast = useToast();

    // Data retrieval
    const entity = ref({});
    const meta = ref([]);

    const get = async () => {
      try {
        const response = await axios.get(`/item/${exports.itemId.value}`);
        for (let variant of response.data.entity.variants) {
          for (let meta of variant.meta) {
            variant[meta.name] = meta.value;
          }
        }
        entity.value = response.data.entity;
        meta.value = response.data.meta;
      } catch (e) {
        console.log(e);
      }
    };

    exports.entity = entity;

    // Variant

    const selection = ref({});

    const selectedSku = computed(() => {
      if (!entity.value.variants) return "";
      if (entity.value.variants.length === 1)
        return entity.value.variants[0].sku;
      if (Object.keys(selection.value).length !== meta.value.length) {
        return "";
      }
      for (let variant of entity.value.variants) {
        if (_.isMatch(variant, selection.value)) return variant.sku;
      }
      return "";
    });

    const selectedVariant = computed(() => {
      if (entity.value.variants && entity.value.variants.length)
        for (let variant of entity.value.variants) {
          if (variant.sku === selectedSku.value) {
            return variant;
          }
        }
      return null;
    });

    const variantOptions = computed(() => {
      const options = {};
      if (meta.value)
        for (let field of meta.value) {
          options[field] = [];
          for (let variant of entity.value.variants) {
            let value = variant[field];
            if (!options[field].includes(value)) {
              options[field].push(value);
            }
          }
        }
      return options;
    });

    const setSelection = (field, option) => {
      if (selection.value[field] === option) {
        delete selection.value[field];
      } else if (canExist(field, option)) {
        selection.value[field] = option;
      }
    };

    const canExist = (field, option) => {
      const selected = { ...selection.value };
      selected[field] = option;
      for (let variant of entity.value.variants) {
        if (_.isMatch(variant, selected)) return true;
      }
      return false;
    };

    exports.canExist = canExist;
    exports.selection = selection;
    exports.setSelection = setSelection;
    exports.variantOptions = variantOptions;
    exports.selectedSku = selectedSku;
    exports.selectedVariant = selectedVariant;

    // Cart

    const quantity = ref(1);

    const increment = (q) => {
      if (!selectedSku.value) return;
      if (quantity.value + q < 1) return;
      if (quantity.value + q > selectedVariant.value.remaining_stock) return;
      quantity.value += q;
    };

    const canAddToCart = computed(() => {
      return selectedSku.value && selectedVariant.value.remaining_stock;
    });

    const addingToCart = ref(false);
    const showAddToCartModal = ref(false);

    const addToCart = async () => {
      try {
        addingToCart.value = true;
        const response = await axios.post(
          `/cart/add/${selectedSku.value}/${quantity.value}`
        );
        cartStore.getCart();

        showAddToCartModal.value = true;

        for (let variant of entity.value.variants) {
          if (variant.sku === selectedSku.value) {
            variant.remaining_stock -= quantity.value;
          }
        }

        quantity.value = 1;

        setTimeout(() => {
          showAddToCartModal.value = false;
        }, 2000);
      } catch (e) {
        console.log(e);
        if (e.response.data.errors) {
          for (let error of e.response.data.errors) {
            toast.add({ severity: "error", detail: error, group: "br" });
          }
        }
        if (e.response.status === 401) {
          toast.add({
            severity: "error",
            detail: "You need to be logged in first",
            group: "br",
          });
        }
      } finally {
        addingToCart.value = false;
      }
    };

    exports.addToCart = addToCart;
    exports.showAddToCartModal = showAddToCartModal;
    exports.addingToCart = addingToCart;
    exports.canAddToCart = canAddToCart;
    exports.quantity = quantity;
    exports.increment = increment;

    // Images

    const slides = ref(null);

    const imageIndex = ref(0);
    const currentImage = computed(() => {
      if (!entity.value.images || !entity.value.images.length) return {};
      return entity.value.images[imageIndex.value];
    });
    const focusImage = (i) => {
      imageIndex.value = i;
      slides.value.goToSlide(i);
    };

    exports.focusImage = focusImage;
    exports.currentImage = currentImage;
    exports.slides = slides;

    onMounted(() => {
      get();
    });

    return exports;
  },
});