import cartStore from "/js/store/cartStore.js";
import { formVerify, formValid } from "/js/modules/useForm.js";

const { createApp, ref, onMounted, watch, computed } = Vue;

window.app = createApp({
  setup() {
    const exports = {
      cartState: cartStore
    };

    // Data Retrieval

    const addresses = ref([]);

    const get = async () => {
      try {
        const response = await axios.get("/customer/addresses");
        addresses.value = response.data.data;
      } catch (e) {
        console.log(e);
      }
    };

    const getAddresses = async () => {
      await get();
      try {
        const response = await axios.get(
          `${ADDRESS_API}/resolve`,
          {
            params: {
              regions: addresses.value.map((address) => address.region_id),
              provinces: addresses.value.map((address) => address.province_id),
              cities: addresses.value.map((address) => address.city_id),
              barangay: addresses.value.map((address) => address.barangay_id),
            },
          }
        );
        for (let i = 0; i < response.data.entities.length; i++) {
          const addr = response.data.entities[i];
          addresses.value[i].region = addr.region.name;
          addresses.value[i].province = addr.province.name;
          addresses.value[i].city = addr.city.name;
          addresses.value[i].barangay = addr.barangay.name;
        }
      } catch (e) { }
    };

    exports.addresses = addresses;
    // Form

    const formModalVisible = ref(false);

    const formData = ref({
      barangay_id: "",
      city_id: "",
      province_id: "",
      region_id: "",
      street: "",
      name: "",
      zip_code: "",
    });

    const formRules = ref({
      address: [
        {
          type: "required",
        },
      ],
      name: [
        {
          type: "required",
        },
      ],
      zip_code: [
        {
          type: "required",
        },
      ],
    });

    const formErrors = ref({});

    const formResponse = ref("");
    const formSuccess = ref(false);
    const formFeedbackClasses = computed(() => {
      return formSuccess.value ? "text-success" : "text-danger";
    });
    const submitting = ref(false);

    const submit = async (event) => {
      formVerify(formData, formRules, formErrors);
      if (!formValid(formErrors)) return;
      try {
        const response = await axios.post(
          "/customer/addresses",
          formData.value
        );
        formResponse.value = response.data.data;
        formSuccess.value = true;
        getAddresses();
      } catch (e) {
        console.log(e);
        if (e.response) {
          formResponse.value = e.response.data.data;
          formSuccess.value = false;
        }
      }
    };

    exports.formResponse = formResponse;
    exports.formSuccess = formSuccess;
    exports.formErrors = formErrors;
    exports.formModalVisible = formModalVisible;
    exports.submitting = submitting;
    exports.formFeedbackClasses = formFeedbackClasses;
    exports.submit = submit;

    // Address

    const regions = ref([]);
    const provinces = ref([]);
    const cities = ref([]);
    const barangay = ref([]);
    const loading = ref(false);

    const getRegions = async () => {
      try {
        const response = await axios.get(
          `${ADDRESS_API}/regions`
        );
        regions.value = response.data.data.entities;
      } catch (e) {
        console.log(e);
      }
    };

    const getProvinces = async () => {
      try {
        const response = await axios.get(
          `${ADDRESS_API}/provinces/${formData.value.region_id}`
        );
        provinces.value = response.data.data.entities;
      } catch (e) {
        console.log(e);
      }
    };

    const getCities = async (event) => {
      let search = "";
      if (event && event.value) search = event.value;
      if (event && search === "") return;
      loading.value = true;
      const response = await axios.get(
        `${ADDRESS_API}/cities/${formData.value.province_id}`,
        {
          params: {
            q: search,
          },
        }
      );
      cities.value = response.data.data.entities;
      loading.value = false;
    };
    const getBarangay = async (event) => {
      if (!formData.value.city_id) return;
      let search = "";
      if (event && event.value) search = event.value;
      if (event && search === "") return;
      const response = await axios.get(
        `${ADDRESS_API}/barangay/${formData.value.city_id}`,
        {
          params: {
            q: search,
          },
        }
      );
      barangay.value = response.data.data.entities;
    };

    const region_id = computed(() => formData.value.region_id);
    const province_id = computed(() => formData.value.province_id);
    const city_id = computed(() => formData.value.city_id);
    const barangay_id = computed(() => formData.value.barangay_id);

    watch(region_id, () => getProvinces());
    watch(province_id, () => getCities());
    watch(city_id, () => getBarangay());

    Object.assign(exports, {
      regions,
      provinces,
      cities,
      barangay,

      getRegions,
      getProvinces,
      getCities,
      getBarangay,
    });

    onMounted(() => {
      getRegions();
      getAddresses();
    });

    exports.formData = formData;

    return exports;
  },
  components: {
    'p-dropdown': primevue.dropdown,
    'p-dialog': primevue.dialog,
  },
});
