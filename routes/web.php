<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin;
use App\Http\Controllers\Auth\AdminLoginController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ShopController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/contact-us', [HomeController::class, 'contactUs'])->name('contact-us');

Route::get('/shop', [ShopController::class, 'index'])->name('shop.index');

Route::get('/shop/item/{id}/{sku?}', [ShopController::class, 'item'])->name('shop.item');

Route::get('/customer', [CustomerController::class, 'index'])->name('customer.index');
Route::get('/customer/addresses', [CustomerController::class, 'addresses'])->name('customer.addresses');
Route::get('/customer/orders', [CustomerController::class, 'orders'])->name('customer.orders');
Route::get('/customer/change-password', [CustomerController::class, 'change_password'])->name('customer.change_password');

Route::get('/cart', [CartController::class, 'index'])->name('cart.index');
Route::get('/cart/checkout', [CartController::class, 'checkout'])->name('cart.checkout');

Route::get('/order/track/{number}', [OrdersController::class, 'track'])->name('order.track');

// Admin routes

Route::get('/admin/login', [AdminLoginController::class, 'showLoginForm'])->name('admin.login');
Route::post('/admin/login', [AdminLoginController::class, 'login'])->name('admin.login');

Route::get('/admin', [Admin\HomeController::class, 'index'])->name('admin.home');

$admin_routes = [
    Admin\BrandsController::class,
    Admin\CategoriesController::class,
    Admin\ItemsController::class,
    Admin\OverallPromotionsController::class,
    Admin\PriceCodesController::class,
    Admin\CouponPromosController::class,
    Admin\CustomersController::class,
    Admin\OrdersController::class,
    Admin\ShippingController::class,
];

foreach ($admin_routes as $class) {
    Route::resource(
        '/admin/' . call_user_func($class . "::kebab"),
        $class,
        ['as' => 'admin'] // Give route names the 'admin' prefix, i.e. the route name "brands.index" would become "admin.brands.index"
    )->only(['index', 'show']);
}

Route::get('/admin/orders/invoice/{id}', [Admin\OrdersController::class, 'invoice'])->name('invoice');
