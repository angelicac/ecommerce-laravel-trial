<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Admin;
use App\Http\Controllers\Api\HomeApiController;
use App\Http\Controllers\Api\CartApiController;
use App\Http\Controllers\Api\CategoriesApiController;
use App\Http\Controllers\Api\CustomerApiController;
use App\Http\Controllers\Api\ItemsApiController;
use App\Http\Controllers\OrdersApiController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/contact-us', [HomeApiController::class, 'sendContactEmail'])->name('contact-us-email');

Route::get('/items', [ItemsApiController::class, 'index'])->name('items');
Route::get('/item/{id}', [ItemsApiController::class, 'item'])->name('item');
Route::get('/item/image/{item_id}/{index?}', [ItemsApiController::class, 'image'])->name('item');

Route::get('/categories', [CategoriesApiController::class, 'index'])->name('categories');

Route::match(['get', 'post'], '/customer/addresses', [CustomerApiController::class, 'addresses'])->name('customer.addresses');
Route::post('/customer/update', [CustomerApiController::class, 'update'])->name('customer.update');
Route::post('/customer/change-password', [CustomerApiController::class, 'change_password'])->name('customer.change-password');

Route::get('/cart', [CartApiController::class, 'index'])->name('cart.index');
Route::controller(CartApiController::class)->group(function () {
    Route::post('/cart/checkout', 'checkout')->name('cart.checkout');
    Route::post('/cart/add/{sku}/{quantity}', 'add')->name('cart.add');
    Route::post('/cart/remove/{sku}/{quantity}', 'remove')->name('cart.remove');
    Route::post('/cart/remove-all/{sku}', 'remove_all')->name('cart.remove-all');
    Route::post('/cart/set-coupon', 'set_coupon')->name('cart.set-coupon');
    Route::post('/cart/dismiss-notes/{sku}', 'dismiss_notes')->name('cart.dismiss-notes');
    Route::get('/cart/calculate-shipping-fee', 'get_shipping_fee')->name('cart.calculate-shipping-fee');
    Route::get('/cart/shipping-methods', 'get_shipping_methods')->name('cart.shipping-methods');
});

Route::get('/orders/data/{number}', [OrdersApiController::class, 'data'])->name('order.data');
Route::middleware(['auth'])->get('/orders', function () {
    return [
        'data' => Auth::user()->orders
    ];
});
// Admin
$admin_controllers = [
    Admin\BrandsApiController::class,
    Admin\CategoriesApiController::class,
    Admin\ItemsApiController::class,
    Admin\ItemVariantsApiController::class,
    Admin\OverallPromotionsApiController::class,
    Admin\PriceCodesApiController::class,
    Admin\CouponPromosApiController::class,
    Admin\CustomersApiController::class,
    Admin\OrdersApiController::class,
    Admin\ShippingApiController::class,
];

foreach ($admin_controllers as $class) {
    Route::resource(
        '/admin/' . call_user_func($class . "::kebab"),
        $class,
        ['as' => 'admin.api']
    )->except(['create', 'delete']);
}

Route::middleware(['auth.admin', 'admin'])->get('/admin/order-status', function () {
    return ['data' => DB::table('order_status')->orderBy('id')->get()];
});

// custom routes for each endpoint
Route::post('/admin/items/upload-images/{id}', [Admin\ItemsApiController::class, 'upload_images'])->name('admin-item-image-upload');

Route::post('/admin/orders/cancel/{id}', [Admin\OrdersApiController::class, 'cancel_order'])->name('admin.orders.cancel');

Route::controller(Admin\ShippingApiController::class)->group(function () {
    Route::get('/admin/shipping/default', 'default_shipping_fee')->name('admin.default-shipping-fee');
    Route::post('/admin/shipping/default', 'set_default_shipping_fee')->name('admin.default-shipping-fee');
    Route::post('/admin/shipping/fee', 'set_shipping_fee')->name('admin.set-shipping-fee');
    Route::get('/admin/shipping/regions', 'get_region_shipping_fees')->name('admin.shipping-fee-regions');
    Route::get('/admin/shipping/provinces', 'get_province_shipping_fees')->name('admin.shipping-fee-provinces');
    Route::get('/admin/shipping/cities', 'get_city_shipping_fees')->name('admin.shipping-fee-cities');
});
