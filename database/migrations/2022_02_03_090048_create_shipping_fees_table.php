<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateShippingFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_locations', function (Blueprint $table) {
            $table->id();
            $table->string('brgy_id')->nullable();
            $table->string('city_id')->nullable();
            $table->string('province_id')->nullable();
            $table->string('region_id')->nullable();
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create('shipping_methods', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->primary('id');

            $table->string('name');
        });

        DB::table('shipping_methods')->insert([
            'id' => 1,
            'name' => 'Flat Rate'
        ]);

        DB::table('shipping_methods')->insert([
            'id' => 2,
            'name' => 'Free Shipping'
        ]);

        DB::table('shipping_methods')->insert([
            'id' => 3,
            'name' => 'Calculated'
        ]);

        Schema::create('shipping_fees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('shipping_location_id');
            $table->unsignedBigInteger('shipping_method_id');
            $table->string('fee')->default('0');
            $table->decimal('minimum_price', 19)->nullable();

            $table->foreign('shipping_location_id')->references('id')->on('shipping_locations');
            $table->foreign('shipping_method_id')->references('id')->on('shipping_methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_fees');
        Schema::dropIfExists('shipping_methods');
        Schema::dropIfExists('shipping_locations');
    }
}
