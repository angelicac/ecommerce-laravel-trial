<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_status', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->primary('id');
            $table->string('name', 15)->unique();
        });


        DB::table('order_status')->insert([
            'name' => 'ORDERED',
            'id' => 1
        ]);
        DB::table('order_status')->insert([
            'name' => 'PAID',
            'id' => 2
        ]);
        DB::table('order_status')->insert([
            'name' => 'SHIPPING',
            'id' => 3
        ]);
        DB::table('order_status')->insert([
            'name' => 'DELIVERED',
            'id' => 4
        ]);
        DB::table('order_status')->insert([
            'name' => 'CANCELLED',
            'id' => 5
        ]);

        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('number', 32)->unique();
            $table->decimal('total', 19, 2);
            $table->decimal('original_total', 19, 2);
            $table->decimal('shipping_fee', 19, 2);
            $table->decimal('coupon_discount_total', 19, 2);
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('contact_number');
            $table->string('email');
            $table->string('street', 63);
            $table->string('region_id', 63);
            $table->string('province_id', 255);
            $table->string('city_id', 255);
            $table->string('barangay_id', 255);
            $table->string('zip_code', 5);
            $table->unsignedBigInteger('order_status')->default(1);
            $table->text('delivery_instructions');
            $table->unsignedBigInteger('coupon_promo_id')->nullable();
            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('users');
            $table->foreign('order_status')->references('id')->on('order_status');
            $table->foreign('coupon_promo_id')->references('id')->on('coupon_promos');
        });

        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->string('sku', 32);
            $table->decimal('price', 19, 2);
            $table->decimal('original_price', 19, 2);
            $table->integer('quantity');

            $table->foreign('sku')->references('sku')->on('item_variants');
            $table->foreign('order_id')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_status');
    }
}
