<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('name', 127);
            $table->text('description');
            $table->unsignedBigInteger('brand_id');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->boolean('is_visible')->default(true);
            $table->timestamps();
        });
        Schema::create('item_variants', function (Blueprint $table) {
            $table->string('sku', 32);
            $table->primary('sku');
            $table->unsignedBigInteger('item_id');
            $table->decimal('price', 19, 2);
            $table->unsignedBigInteger('stock')->default(0);


            // Dimensions

            $table->unsignedBigInteger('weight');

            $table->unsignedBigInteger('length')->nullable();
            $table->unsignedBigInteger('width')->nullable();
            $table->unsignedBigInteger('height')->nullable();

            $table->foreign('item_id')->references('id')->on('items');
        });
        Schema::create('item_variant_meta', function (Blueprint $table) {
            $table->id();
            $table->string('sku', 32);
            $table->string('name', 32);
            $table->string('value', 32);
            $table->foreign('sku')->references('sku')->on('item_variants')->cascadeOnDelete();
        });
        Schema::create('item_categories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('category_id');
            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('category_id')->references('id')->on('categories');
        });
        Schema::create('item_images', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('item_id');
            $table->foreign('item_id')->references('id')->on('items');
            $table->string('file_name', 256);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_variant_meta');
        Schema::dropIfExists('item_variants');
        Schema::dropIfExists('item_images');
        Schema::dropIfExists('item_categories');
        Schema::dropIfExists('items');
    }
}
