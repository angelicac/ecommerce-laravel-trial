<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartItemVariantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_item_variant', function (Blueprint $table) {
            $table->foreignId('cart_id')
                ->constrained()
                ->cascadeOnDelete();
            $table->string('sku', 32);
            $table->text('notes')->nullable();
            $table->foreign('sku')
                ->references('sku')
                ->on('item_variants')
                ->cascadeOnDelete();
            $table->integer('quantity')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_item_variant');
    }
}
