<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePriceCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_codes', function (Blueprint $table) {
            $table->id();
            $table->string('name', 127)->unique();
            $table->timestamps();
        });

        Schema::create('price_code_items', function (Blueprint $table) {
            $table->id();
            $table->string('sku', 32);
            $table->foreign('sku')->references('sku')->on('item_variants');
            $table->unsignedBigInteger('price_code_id');
            $table->foreign('price_code_id')->references('id')->on('price_codes');
            $table->decimal('price', 19);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('price_code_id')->nullable();
            $table->foreign('price_code_id')->references('id')->on('price_codes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_code_items');
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['price_code_id']);
            $table->dropColumn('price_code_id');
        });
        Schema::dropIfExists('price_codes');
    }
}
