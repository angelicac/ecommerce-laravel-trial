<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCouponPromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_promo_types', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->primary('id');
            $table->string('name', 32);
        });

        DB::table('coupon_promo_types')->insert([
            'id' => 1,
            'name' => 'Flat'
        ]);
        DB::table('coupon_promo_types')->insert([
            'id' => 2,
            'name' => 'Reduction'
        ]);

        Schema::create('coupon_promos', function (Blueprint $table) {
            $table->id();
            $table->string('code', 32)->unique();
            $table->unsignedBigInteger('type');
            $table->foreign('type')->references('id')->on('coupon_promo_types');
            $table->decimal('rate_threshold', 19, 2)->nullable();
            $table->decimal('rate_reduction', 19, 2)->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();
        });

        Schema::create('coupon_promo_items', function (Blueprint $table) {
            $table->id();
            $table->string('sku', 32);
            $table->decimal('price', 19, 2);
            $table->unsignedBigInteger('coupon_promo_id');
            $table->foreign('sku')->references('sku')->on('item_variants');
            $table->foreign('coupon_promo_id')->references('id')->on('coupon_promos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_promo_items');
        Schema::dropIfExists('coupon_promos');
        Schema::dropIfExists('coupon_promo_types');
    }
}
