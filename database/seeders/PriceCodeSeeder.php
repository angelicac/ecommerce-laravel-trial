<?php

namespace Database\Seeders;

use App\Models\ItemVariant;
use App\Models\PriceCode;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PriceCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = ItemVariant::limit(100)->get();
        $price_codes = PriceCode::factory()->count(10)->create();
        $price_code_items = [];
        foreach ($price_codes as $price_code) {
            $number_of_items = random_int(1, 3);
            for ($j = 0; $j < $number_of_items; $j++) {
                $price_percentage = random_int(5, 8) / 10;
                $item = $items->random();
                $price_code_items[] = [
                    'sku' => $item->sku,
                    'price' => ($item->price) * $price_percentage,
                    'price_code_id' => $price_code->id
                ];
            }
        }
        DB::table('price_code_items')->insert($price_code_items);
    }
}
