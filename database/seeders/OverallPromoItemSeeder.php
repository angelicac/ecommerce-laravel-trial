<?php

namespace Database\Seeders;

use App\Models\ItemVariant;
use Illuminate\Database\Seeder;
use App\Models\OverallPromoItem;

class OverallPromoItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = ItemVariant::limit(100)->get();

        $overall_promo_items = [];
        for ($i = 0; $i < 10; $i++) {
            $item = $items->random();
            $overall_promo_items[] = [
                'sku' => $item->sku,
                'price' => $item->price - ($item->price * (random_int(1, 10) / 100)),
            ];
        }
        OverallPromoItem::factory()->createMany($overall_promo_items);
    }
}
