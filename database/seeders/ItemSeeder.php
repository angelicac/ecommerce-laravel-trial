<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Item;
use App\Models\ItemImage;
use App\Models\ItemVariant;
use App\Models\ItemVariantMeta;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemSeeder extends Seeder
{
    // faker image parameters
    const IMAGE_HEIGHT = 100;
    const IMAGE_WIDTH = 100;
    const IMAGE_CATEGORY = null;
    const IMAGE_FULL_PATH = false; // especially important so that the full path isn't stored in the db

    /**
     * IMPORTANT: the max bounds
     * for these are hard coded below.
     * Make sure to adjust them when adding
     * or removing options
     */
    const VARIANT_SAMPLE_OPTIONS = [
        'size' => [
            'small',
            'medium',
            'big',
            'giant',
        ],
        'color' => [
            'black',
            'white',
            'red',
            'blue',
            'green',
            'yellow',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));

        $brand_table = Brand::all();
        // temporarily set to just 10 items for quick testing
        for ($i = 0; $i < 10; $i++) {
            $brand = $brand_table->random();
            $item = Item::factory()->create([
                'brand_id' => $brand->id
            ]);

            $number_of_variants = random_int(1, 3);
            $base_price = random_int(1, 10) * 100;
            $item_variant_data = [];
            for ($j = 0; $j < $number_of_variants; $j++) {
                $item_variant_data[] = [
                    'item_id' => $item->id,
                    'sku' => $faker->unique()->ean13(),
                    'price' => $base_price + random_int(1, 10) * random_int(1, 10),
                    'weight' => random_int(1, 1000),
                    'stock' => random_int(10, 100),
                ];
            }
            $item_variants = ItemVariant::factory()->createMany($item_variant_data);

            $item_variant_meta_data = [];
            if ($number_of_variants > 1) {
                foreach ($item_variants as $item_variant) {
                    $item_variant_meta_data[] = [
                        'sku' => $item_variant->sku,
                        'name' => 'size',
                        'value' => self::VARIANT_SAMPLE_OPTIONS['size'][random_int(0, 3)]
                    ];
                    $item_variant_meta_data[] = [
                        'sku' => $item_variant->sku,
                        'name' => 'color',
                        'value' => self::VARIANT_SAMPLE_OPTIONS['color'][random_int(0, 5)]
                    ];
                }
            }
            ItemVariantMeta::factory()->createMany($item_variant_meta_data);

            // $number_of_categories = random_int(1, 3);
            // set to 1 until faker image performance can be addressed
            $number_of_categories = 1;
            $categories = DB::table('categories')->inRandomOrder()->limit($number_of_categories)->get();
            $item_categories = [];
            foreach ($categories as $category) {
                $item_categories[] = [
                    'item_id' => $item->id,
                    'category_id' => $category->id
                ];
            }
            DB::table('item_categories')->insert($item_categories);

            $number_of_images = random_int(1, 3);
            $item_images = [];
            for ($j = 0; $j < $number_of_images; $j++) {
                $item_images[] = [
                    'item_id' => $item->id
                ];
            }
            ItemImage::factory()->createMany($item_images);
        }
        DB::commit();
    }
}
