<?php

namespace Database\Seeders;

use App\Models\CouponPromo;
use App\Models\CouponPromoItem;
use App\Models\CouponPromoType;
use DateInterval;
use DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CouponPromoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));

        DB::beginTransaction();
        $promo = new CouponPromo();
        $type = random_int(1, 2);
        $code = $faker->promotionCode;
        $promo->code = $code;
        $promo->type = $type;
        $start_date = new DateTime();
        $days_offset = random_int(0, 60);
        if (random_int(0, 1)) {
            $start_date->add(new DateInterval("P{$days_offset}D"));
        } else {
            $start_date->sub(new DateInterval("P{$days_offset}D"));
        }
        $end_date = $start_date;
        $days = random_int(1, 5) * 7;
        $end_date->add(new DateInterval("P{$days}D"));
        $promo->start_date = $start_date->format('Y-m-d');
        $promo->end_date = $end_date->format('Y-m-d H:i:s');
        if ($type === CouponPromoType::REDUCTION) {
            $rate_threshold = random_int(1, 10) * 100;
            $rate_reduction = $rate_threshold * (random_int(10, 50) / 100);
            $promo->rate_threshold = $rate_threshold;
            $promo->rate_reduction = $rate_reduction;
        }
        $promo->save();
        if ($type === CouponPromoType::FLAT) {
            $number_of_items = random_int(1, 5);
            $items = DB::table('item_variants')->inRandomOrder()->limit($number_of_items)->get()->all();
            for ($i = 0; $i < $number_of_items; $i++) {
                $item = array_pop($items);
                $coupon_promo_item = new CouponPromoItem();
                $coupon_promo_item->sku = $item->sku;
                $coupon_promo_item->coupon_promo_id = $promo->id;
                $coupon_promo_item->price = $item->price * (random_int(1, 8) / 10);
                $coupon_promo_item->save();
            }
        }
        DB::commit();
    }
}
