<?php

namespace Database\Factories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $order_number = Order::get_next_order_number();
        return [
            'number' => $order_number,
            'total' => 0,
            'original_total' => 0,
            'coupon_discount_total' => 0,
            'shipping_fee' => 0,
            'first_name' => 'Test',
            'last_name' => 'Test',
            'contact_number' => '09123456789',
            'email' => 'admin@admin.com',
            'delivery_instructions' => '',
            'street' => 'street',
            'region_id' => '',
            'province_id' => '',
            'city_id' => '',
            'barangay_id' => '',
            'zip_code' => '',
            'customer_id' => 1,
        ];
    }
}
