<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ItemImageFactory extends Factory
{
    // faker image parameters
    const IMAGE_HEIGHT = 100;
    const IMAGE_WIDTH = 100;
    const IMAGE_CATEGORY = null;
    const IMAGE_FULL_PATH = false; // especially important so that the full path isn't stored in the db

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));
        return [
            'file_name' => $faker->image(
                storage_path('app/public/img/items'),
                self::IMAGE_HEIGHT,
                self::IMAGE_WIDTH,
                self::IMAGE_CATEGORY,
                self::IMAGE_FULL_PATH
            )
        ];
    }
}
