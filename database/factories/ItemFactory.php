<?php

namespace Database\Factories;

use App\Models\Brand;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $number_of_paragraphs = random_int(1, 3);
        return [
            'name' => $this->faker->unique()->company(),
            'description' => $number_of_paragraphs,
            'brand_id' => Brand::inRandomOrder()->first()->id
        ];
    }
}
