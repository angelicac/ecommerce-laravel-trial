<?php

namespace Database\Factories;

use DateInterval;
use DateTime;
use Illuminate\Database\Eloquent\Factories\Factory;

class OverallPromoItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $start_date = new DateTime();
        $days_offset = random_int(0, 60);
        $end_date = (
            random_int(0, 1) ?
                $start_date->add(new DateInterval("P{$days_offset}D")) :
                $start_date->sub(new DateInterval("P{$days_offset}D"))
        );
        $days = random_int(1, 5) * 7;
        $end_date->add(new DateInterval("P{$days}D"));

        /**
         * To pass in:
         * sku
         * price
         */
        return [
            'start_date' => $start_date,
            'end_date' => $end_date
        ];
    }
}
