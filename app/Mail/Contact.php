<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    public $paragraphs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        public $name,
        public $email,
        public $mail_subject,
        public $message,
    ) {
        $this->paragraphs = preg_split("/\R/", $this->message);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.contact', [
            'name' => $this->name,
            'email' => $this->email,
            'subject' => $this->mail_subject,
            'paragraphs' => $this->paragraphs,
        ])->subject("Contact Us: {$this->mail_subject}");
    }
}
