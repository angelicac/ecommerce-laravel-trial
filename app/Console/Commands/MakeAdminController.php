<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MakeAdminController extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'make:admin-controller {name} {model?}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    $name = $this->argument('name');
    $name_for_blade = Str::kebab($name);
    $name_for_controller = Str::studly($name_for_blade);
    $model = $this->argument('model') ?? Str::singular($name);

    $item_blade = <<<END
        @extends('layouts.admin.show')

        @section('content')
        <div class="row">
          <div class="col">
            <h1>
              <span v-if="entity.id">
                Editing @{{ entity.name }}
              </span>
              <span v-else>
                New
              </span>
            </h1>
          </div>
          <div class="col-auto">
          <div class="spinner-border text-success" role="status" v-if="loading || submitting">
              <span class="visually-hidden">Loading...</span>
          </div>
          </div>
        </div>
        <div class="container">
          <form action="" @@submit.prevent="submit" method="POST">
            <fieldset :disabled="submitting">
              <input type="hidden" name="id" v-model="entity.id">
              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" v-model="entity.name" required>
                <x-forms.error :field="'name'"></x-forms.error>
              </div>
              <div class="row">
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <div class="col">
                  <div class="alert alert-success alert-dismissible fade show" role="alert" v-if="display_saved">
                  <i class="fa fa-check"></i> <em>Saved!</em>
                  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"
                    @@click="display_saved=false">&times;</button>
                  </div>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      
        @endsection

        @section('app')
        <script>
        const App = Vue.createApp({
            mixins: [AdminItemMixin],
            data() {
                return {
                    id: ID,
                    controller: '$name_for_blade',
                };
            },
            computed: {
                entity_payload() {
                    const payload = { ...this.entity, one2m: {}, m2m: {} };
                    for (key in this.one2m) {
                      payload.one2m[key] = this.one2m[key];
                    }
                    for (key in this.m2m) {
                      payload.m2m[key] = this.m2m[key];
                    }
                    return payload;
                },
            }
        });
        </script>
        @endsection
        END;

    $list_blade = <<<END
        @extends('layouts.admin.index')

        @section('app')
        <script>
            const App = Vue.createApp({
                mixins: [AdminListMixin],
                data() {
                    return {
                        controller: '$name_for_blade',
                        columns: [{
                            label: 'Name',
                            field: 'name',
                            link: true,
                        }]
                    };
                }
            });
        </script>
        @endsection
        END;

    $controller = <<<END
        <?php

        namespace App\Http\Controllers\Admin;

        use App\Models\\$model;

        class {$name_for_controller}Controller extends AdminController
        {

            public const CONTROLLER_MODEL = {$model}::class;

        }
        END;

    $api_controller = <<<END
        <?php

        namespace App\Http\Controllers\Api\Admin;

        use App\Models\\$model;
        use Illuminate\Http\Request;
        use Illuminate\Contracts\Container\BindingResolutionException;
        use Illuminate\Http\JsonResponse;
        use Illuminate\Support\Facades\DB;
        use Illuminate\Validation\Rule;
        
        class {$name_for_controller}ApiController extends AdminApiController
        {
            protected const CONTROLLER_MODEL = {$model}::class;

            protected \$search_ajax_keys = [];

            protected function get_store_validation_rules(array \$data): array
            {
                return [];
            }
        
            protected function get_update_validation_rules(array \$data, string|int \$id): array
            {
                return [];
            }
        
            /**
             * 
             * @param Request \$request 
             * @return JsonResponse 
             * @throws BindingResolutionException 
             */
            public function store(Request \$request, &\$entity = null)
            {
                DB::beginTransaction();
                parent::store(\$request, entity: \$entity);
                DB::commit();
                return response()->json(['id' => \$entity->id], 201);
            }

            public function show(Request \$request, \$id = 0)
            {
                return parent::show(\$request, \$id);
            }

            public function update(Request \$request, \$id, &\$entity = null)
            {
                DB::beginTransaction();
                \$response = parent::update(\$request, \$id, entity: \$entity);
                if (\$response->status() === 404) return \$response;
                DB::commit();
                return response()->json(['message' => 'Updated!']);
            }

        }
        END;
    if (!is_file($filename = "app/Http/Controllers/Admin/{$name_for_controller}Controller.php"))
      file_put_contents($filename, $controller);

    if (!is_file($filename = "app/Http/Controllers/Api/Admin/{$name_for_controller}ApiController.php"))
      file_put_contents($filename, $api_controller);

    if (!is_dir($dirname = "resources/views/admin/$name_for_blade"))
      mkdir($dirname, 0775);

    if (!is_file($filename = "resources/views/admin/$name_for_blade/item.blade.php"))
      file_put_contents($filename,  $item_blade);

    if (!is_file($filename = "resources/views/admin/$name_for_blade/list.blade.php"))
      file_put_contents($filename,  $list_blade);
    return 0;
  }
}
