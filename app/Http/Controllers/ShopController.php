<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index()
    {
        return view('shop.index');
    }

    public function item($id)
    {
        $item = Item::find($id);
        if (!$item) {
            return redirect(route('shop.index'));
        }
        return view('shop.item', [
            'item_id' => $item->id
        ]);
    }
}
