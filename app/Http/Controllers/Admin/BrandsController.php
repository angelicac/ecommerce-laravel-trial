<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;

class BrandsController extends AdminController
{
    public const CONTROLLER_MODEL = Brand::class;
}
