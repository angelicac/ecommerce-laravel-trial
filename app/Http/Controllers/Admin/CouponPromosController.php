<?php

namespace App\Http\Controllers\Admin;

use App\Models\CouponPromo;

class CouponPromosController extends AdminController
{

    public const CONTROLLER_MODEL = CouponPromo::class;

}