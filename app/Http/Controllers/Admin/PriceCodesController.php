<?php

namespace App\Http\Controllers\Admin;

use App\Models\PriceCode;

class PriceCodesController extends AdminController
{

    public const CONTROLLER_MODEL = PriceCode::class;

}