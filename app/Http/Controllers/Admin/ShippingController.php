<?php

namespace App\Http\Controllers\Admin;

use App\Models\ShippingMethod;
use App\Models\ShippingLocation;

class ShippingController extends AdminController
{
    public const CONTROLLER_MODEL = ShippingLocation::class;

    protected function show_data()
    {
        return ['shipping_methods' => [
            'FLAT_RATE' => ShippingMethod::FLAT_RATE,
            'FREE_SHIPPING' => ShippingMethod::FREE_SHIPPING,
            'CALCULATED' => ShippingMethod::CALCULATED
        ]];
    }
}
