<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;

class CategoriesController extends AdminController
{

    public const CONTROLLER_MODEL = Category::class;
}
