<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;

class CustomersController extends AdminController
{

    public const CONTROLLER_MODEL = User::class;
}
