<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

abstract class AdminController extends Controller
{

    protected $list_view_show_add = true;
    protected $list_view_link = '';

    protected $insert_view_link = '';
    protected $insert_view_link_text = 'Add New';

    protected const ALLOW_DELETE = false;

    protected const CONTROLLER_MODEL = '';
    protected Model $controller_model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth.admin', 'admin']);
        if (static::CONTROLLER_MODEL) {
            $model = static::CONTROLLER_MODEL;
            $this->controller_model = new $model();
        }
        $this->insert_view_link = $this->insert_view_link ?: "admin/" . static::kebab() . "/show";
        $this->list_view_link = $this->list_view_link ?: "admin/" . static::kebab() . "/index";
    }

    // Views

    public function index(Request $request)
    {
        return view("admin." . static::kebab() . ".index", array_merge($this->index_data(), [
            'title' => Str::headline(static::kebab()),
            'show_add' => $this->list_view_show_add,
            'insert_view_link' => $this->insert_view_link,
            'insert_view_link_text' => $this->insert_view_link_text,
            'allow_delete' => static::ALLOW_DELETE,
            'list_view_link' => $this->list_view_link
        ]));
    }

    public function show(Request $request, $id = 0)
    {
        $entity = $this->controller_model->query()->where('id', $id)->get()->first();
        return view("admin." . static::kebab() . ".show", array_merge($this->show_data(), [
            'entity' => $entity,
            'id' => $id,
            'insert_view_link' => $this->insert_view_link,
            'list_view_link' => $this->list_view_link,
            'allow_delete' => static::ALLOW_DELETE,
        ]));
    }

    protected function index_data()
    {
        return [];
    }

    protected function show_data()
    {
        return [];
    }

    public static function kebab()
    {
        return Str::kebab(Str::replaceLast('Controller', '', class_basename(static::class)));
    }
}
