<?php

namespace App\Http\Controllers\Admin;

use App\Models\OverallPromoItem;

class OverallPromotionsController extends AdminController
{

    public const CONTROLLER_MODEL = OverallPromoItem::class;
}
