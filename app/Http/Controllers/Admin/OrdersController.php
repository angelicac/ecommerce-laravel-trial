<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use LaravelDaily\Invoices\Classes\InvoiceItem;
use LaravelDaily\Invoices\Classes\Party;
use App\Invoices\OrderInvoice;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class OrdersController extends AdminController
{

    public const CONTROLLER_MODEL = Order::class;

    protected $list_view_show_add = false;

    protected function generate_invoice(Order $order)
    {
        $client = new Party([
            'name'          => 'Technomancer',
        ]);

        $response = Http::get(config('app.address_api') . '/resolve', [
            'regions[]' => $order->region_id,
            'provinces[]' => $order->province_id,
            'cities[]' => $order->city_id,
            'barangay[]' => $order->barangay_id
        ]);

        $region = Arr::get($response->json(), 'entities.0.region.name');
        $province = Arr::get($response->json(), 'entities.0.province.name');
        $city = Arr::get($response->json(), 'entities.0.city.name');
        $barangay = Arr::get($response->json(), 'entities.0.barangay.name');

        $customer = new Party([
            'name'          => "{$order->first_name} {$order->last_name}",
            'address' => "{$order->street} {$barangay} {$city} {$province} {$region} {$order->zip_code}",
            'phone' => $order->contact_number,
            'custom_fields' => [
                'email' => $order->email
            ]
        ]);

        $items = [];

        foreach ($order->items as $item) {
            $items[] = (new InvoiceItem())
                ->title($item->item->name)
                ->description($item->sku)
                ->pricePerUnit($item->pivot->original_price)
                ->quantity($item->pivot->quantity)
                ->discount(($item->pivot->original_price - $item->pivot->price) * $item->pivot->quantity);
        }


        $invoice = OrderInvoice::make('receipt')
            ->template('invoice')
            // ability to include translated invoice status
            // in case it was paid
            ->sequence($order->number)
            ->seller($client)
            ->buyer($customer)
            ->date($order->created_at)
            ->dateFormat('Y/m/d')
            ->currencySymbol('₱')
            ->currencyCode('PHP')
            ->currencyFormat('{SYMBOL}{VALUE}')
            ->currencyThousandsSeparator(',')
            ->currencyDecimalPoint('.')
            ->filename("invoices/invoice-{$order->number}")
            ->totalAmount($order->total)
            ->addItems($items)
            ->logo(public_path('img/logo-512x512.png'))
            ->couponDiscount($order->coupon_discount_total)
            ->totalDiscount($order->discount)
            ->originalTotal($order->original_total)
            // You can additionally save generated invoice to configured disk
            ->save('local');

        return $invoice;
    }

    public function invoice($id)
    {

        $order = Order::whereId($id)->first();

        if (!$order) return response('', 404);

        if (!Storage::exists("invoices/invoice-{$order->number}.pdf") || config('app.regenerate_invoices')) {
            $this->generate_invoice($order);
        }

        return response()->file(Storage::path("invoices/invoice-{$order->number}.pdf"));
    }
}
