<?php

namespace App\Http\Controllers\Admin;

use App\Models\Item;

class ItemsController extends AdminController
{

    public const CONTROLLER_MODEL = Item::class;
}
