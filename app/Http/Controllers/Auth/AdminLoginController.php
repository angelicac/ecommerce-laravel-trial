<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AdminLoginController extends LoginController
{

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('auth.admin-login');
    }


    /**
     * Attempt to log the user into the application.
     * Override the AuthenticatesUsers::attemptLogin method
     * to check if the user is an admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $email = $request->input('email');
        $user = User::whereEmail($email)->first();
        if ($user?->user_type !== UserType::ADMIN) return false;
        return $this->guard()->attempt(
            $this->credentials($request),
            $request->filled('remember')
        );
    }
}
