<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('customer.index');
    }

    public function addresses(Request $request)
    {
        return view('customer.addresses');
    }

    public function orders()
    {
        return view('customer.orders');
    }

    public function change_password()
    {
        return view('customer.change-password');
    }
}
