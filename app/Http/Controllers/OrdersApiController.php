<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersApiController extends Controller
{
    public function data(string $number)
    {
        $order = Order::with('items')->with('status')->whereNumber($number)->first();
        if (!$order || Auth::user()?->id !== $order->customer_id) {
            return response()->json([], 404);
        }
        return [
            'data' => $order
        ];
    }
}
