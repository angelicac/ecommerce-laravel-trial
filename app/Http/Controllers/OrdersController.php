<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function track(string $number)
    {
        return view('order.track', [
            'order_number' => $number
        ]);
    }
}
