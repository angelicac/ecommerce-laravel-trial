<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class HomeApiController extends Controller
{
    public function sendContactEmail(Request $request)
    {
        $name = $request->post('name');
        $email = $request->post('email');
        $mail_subject = $request->post('subject');
        $message = $request->post('message');
        Log::debug("Sending email to " . config("mail.contact.recipient"));
        Mail::to(config('mail.contact.recipient'))
            ->send(new Contact($name, $email, $mail_subject, $message));
        return response()->json(['message' => 'Sent'], Response::HTTP_OK);
    }
}
