<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\CouponPromo;
use App\Models\CouponPromoItem;
use App\Models\CouponPromoType;
use App\Models\ItemVariant;
use App\Models\Order;
use App\Models\OverallPromoItem;
use App\Models\PriceCodeItem;
use App\Models\ShippingFee;
use App\Models\ShippingLocation;
use App\Models\ShippingMethod;
use App\Settings\SiteSettings;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Webit\Util\EvalMath\EvalMath;

class CartApiController extends Controller
{

    const COUPON_PRICE_APPLIED  = 0b1;
    const OVERALL_PROMO_PRICE_APPLIED = 0b10;
    const PRICE_CODE_APPLIED = 0b100;

    public function __construct()
    {
        if (!config('app.shop.allow_guests'))
            $this->middleware('auth')->except('index');
    }

    /**
     * Gets the cart of the current user
     *
     * @return Cart
     */
    protected function cart()
    {
        if (Auth::user())
            return Cart::firstOrCreate(['user_id' => Auth::user()->id]);
        return Cart::firstOrCreate(['session_id' => Session::getId()]);
    }

    protected function verify_captcha(string $captcha): bool
    {
        if (!config('app.recaptcha.enabled'))
            return true;

        $response = Http::post('https://www.google.com/recaptcha/api/siteverify?' . http_build_query([
            'secret' => config('app.recaptcha.secret_key'),
            'response' => $captcha
        ]));
        return $response->json('success');
    }

    public function index(Request $request)
    {
        $items = $this->cart()->contents;

        if (!config('app.shop.allow_guests') && !Auth::user()) {
            $items = [];
        }

        $total = 0;
        $discount_total = 0;
        $original_total = 0;
        $total_quantity = 0;
        $coupon_code = $request->session()->get('coupon', '');
        $coupon = CouponPromo::whereCode($coupon_code)->first();

        foreach ($items as &$item) {

            // Check stock
            if ($item->stock < $item->pivot->quantity) {
                // Need to update 
                $this->cart()->contents()->updateExistingPivot($item, [
                    'notes' => $item->pivot->notes =  "Quantity has been reduced from {$item->pivot->quantity} to {$item->stock} due to low stock.",
                    'quantity' => $item->pivot->quantity = $item->stock,
                ]);
                $item->save();
            }

            $promo_applied = 0;

            $original_price = $item->price;
            $final_price = $item->price;

            if ($coupon_promo_item = CouponPromoItem::whereSku($item->sku)->whereCouponPromoId($coupon?->id)->first()) {
                if ($coupon_promo_item->price < $final_price) {
                    $final_price = $coupon_promo_item->price;
                    $promo_applied = static::COUPON_PRICE_APPLIED;
                }
            }

            if ($overall_promo_item = OverallPromoItem::get_item_active_promo($item->sku)) {
                if ($overall_promo_item->price < $final_price) {
                    $final_price = $overall_promo_item->price;
                    $promo_applied = static::OVERALL_PROMO_PRICE_APPLIED;
                }
                $final_price = min($final_price, $overall_promo_item->price);
            }

            if ($price_code_item = PriceCodeItem::wherePriceCodeId(Auth::user()?->price_code_id)->whereSku($item->sku)->first()) {
                if ($price_code_item->price < $final_price) {
                    $final_price = $price_code_item->price;
                    $promo_applied = static::PRICE_CODE_APPLIED;
                }
            }

            $item->final_price = $final_price;

            $item_total = $final_price * $item->pivot->quantity;
            $total += $item_total;
            $item_original_total = $original_price * $item->pivot->quantity;
            $original_total += $item_original_total;

            if ($promo_applied & static::COUPON_PRICE_APPLIED) {
                $discount_total += $item_original_total - $item_total;
            }

            $total_quantity += $item->pivot->quantity;
        }

        if ($coupon?->type === CouponPromoType::REDUCTION) {
            if ($total >= $coupon->rate_threshold) {
                $total -= $coupon->rate_reduction;
                $discount_total += $coupon->rate_reduction;
            }
        }

        $coupon_data = [];
        if ($coupon) {
            $coupon_data = [
                'id' => $coupon->id,
                'code' => $coupon->code,
                'message' => $coupon->type === CouponPromoType::FLAT
                    ? 'Discount has been applied to the appropriate items'
                    : 'You save ' . $coupon->rate_reduction . ' on spending ' . $coupon->rate_threshold
            ];
        }

        return [
            'data' => $items,
            'total' => $total,
            'discount_total' => $discount_total,
            'coupon' => $coupon_data,
            'original_total' => $original_total,
            'total_quantity' => $total_quantity,
        ];
    }

    public function set_coupon(Request $request)
    {
        $coupon = CouponPromo::whereCode($request->post('coupon_code', ''))->first();
        if (!$coupon) {
            $request->session()->put('coupon', '');
            return response()->json(['message' => 'Not found!'], 404);
        }
        $request->session()->put('coupon', $coupon->code);
        return ['message' => 'Coupon Set!'];
    }

    public function dismiss_notes(Request $request, string $sku)
    {
        $this->cart()->contents()->updateExistingPivot($sku, [
            'notes' => null,
        ]);
        return ['message' => 'Note dismissed'];
    }

    public function checkout(Request $request)
    {
        if (!$this->verify_captcha($request->post('captcha') ?? '')) {
            return response()->json([], 429);
        }

        Validator::make($request->all(), [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'contact_number' => ['required'],
            'email' => ['required'],
            'street' => ['required'],
            'region_id' => ['required'],
            'province_id' => ['required'],
            'city_id' => ['required'],
            'barangay_id' => ['required'],
            'zip_code' => ['required'],
        ])->validate();

        DB::beginTransaction();

        $data = $this->index($request);

        $order = new Order();
        do {
            $order->number = $order_number = Order::get_next_order_number();
        } while (Order::whereNumber($order_number)->exists());
        $order->total = $data['total'];
        $order->original_total = $data['original_total'];
        $order->coupon_discount_total = $data['discount_total'];
        $order->first_name = $request->post('first_name');
        $order->last_name = $request->post('last_name');
        $order->contact_number = $request->post('contact_number');
        $order->email = $request->post('email');
        $order->delivery_instructions = $request->post('delivery_instructions', '');
        $order->street = $request->post('street');
        $order->region_id = $request->post('region_id');
        $order->province_id = $request->post('province_id');
        $order->city_id = $request->post('city_id');
        $order->barangay_id = $request->post("barangay_id");
        $order->zip_code = $request->post('zip_code');
        $order->customer_id = Auth::user()?->id;

        $order->shipping_fee = $this->calculate_shipping_fee($request, $request->post());
        $order->total += $order->shipping_fee;

        if ($coupon_id = Arr::get($data, 'coupon.id')) {
            $order->coupon_promo_id = $coupon_id;
        }

        $order->save();

        foreach ($data['data'] as $item) {
            // If the item is out of stock, do not include it in the order
            if ($item->stock === 0) continue;
            $order->items()->attach([
                $item->sku => [
                    'price' => $item->final_price,
                    'original_price' => $item->price,
                    'quantity' => $item->pivot->quantity
                ]
            ]);

            $variant = ItemVariant::whereSku($item->sku)->first();
            $variant->stock -= $item->pivot->quantity;
            $variant->save();
        }

        $this->cart()->contents()->detach();

        $request->session()->put('coupon', null);

        DB::commit();
        return response()->json([
            'number' => $order->number
        ], 201);
    }

    protected function cart_change(string $sku)
    {
        return [
            'data' => [
                'quantity' => [
                    'total' => $this->cart()->quantity(),
                    'sku' => $this->cart()->quantity($sku)
                ]
            ]
        ];
    }

    public function add(string $sku, int $quantity)
    {
        try {
            $item = $this->cart()->contents()->wherePivot('sku', $sku)->first() ?? ItemVariant::whereSku($sku)->first();
            if ($item->stock - ($quantity + $item->pivot?->quantity) < 0) {
                return response()->json(['errors' => [
                    'Quantity requested is greater than stock'
                ]], 422);
            }
            $this->cart()->add($sku, $quantity);
            return $this->cart_change($sku);
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => 'There was an error'], 500);
        }
    }

    public function remove(string $sku, int $quantity)
    {
        try {
            $this->cart()->remove($sku, $quantity);
            return $this->cart_change($sku);
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => 'There was an error'], 500);
        }
    }
    public function remove_all(string $sku)
    {
        try {
            $this->cart()->remove_all($sku);
            return $this->cart_change($sku);
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => 'There was an error'], 500);
        }
    }

    public function get_shipping_methods(Request $request)
    {
        $region_id = $request->input('region_id');
        $province_id = $request->input('province_id');
        $city_id = $request->input('city_id');
        $location = null;
        if ($city_id) {
            $location ??= ShippingLocation::whereCityId($city_id)->first();
        }
        if ($province_id) {
            $location ??= ShippingLocation::whereProvinceId($province_id)->first();
        }
        if ($region_id) {
            $location ??= ShippingLocation::whereRegionId($region_id)->first();
        }

        return [
            'data' => $location ? $location->fees : []
        ];
    }

    public function get_shipping_fee(Request $request)
    {
        $data = Arr::only($request->input(), [
            'region_id',
            'province_id',
            'city_id',
            'barangay_id',
            'shipping_fee'
        ]);
        return [
            'data' => $this->calculate_shipping_fee($request, $data)
        ];
    }

    protected function calculate_shipping_fee(Request $request, array $form_data)
    {
        $cart = $this->index($request);
        $barangay_id = Arr::get($form_data, 'barangay_id');
        $city_id = Arr::get($form_data, 'city_id');
        $province_id = Arr::get($form_data, 'province_id');
        $region_id = Arr::get($form_data, 'region_id');
        $shipping_fee = Arr::get($form_data, 'shipping_fee');

        $fee = ShippingFee::whereId($shipping_fee)->first();

        $final_shipping_fee = 0;

        if ($fee->shipping_method_id === ShippingMethod::FLAT_RATE) {
            $final_shipping_fee = (float)$fee->fee;
        } else if ($fee->shipping_method_id === ShippingMethod::FREE_SHIPPING) {
            // TODO: Check if free shipping is applicable and decide what to do if not
            $final_shipping_fee = 0;
        } else if ($fee->shipping_method_id === ShippingMethod::CALCULATED) {
            $formula = $fee->fee;
            $formula = str_replace(
                ['[cost]', '[qty]', '[wt]', '[dim]'],
                [$cart['total'], $cart['total_quantity'], $cart['weight'], $cart['dim']],
                $formula
            );
            $m = new EvalMath();

            $final_shipping_fee = round((float)$m->evaluate($formula), 2);
        }

        return $final_shipping_fee;
    }
}
