<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;
use App\Models\ItemImage;
use App\Models\OverallPromoItem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ItemsApiController extends Controller
{
    public function index(Request $request, $id = 0)
    {
        if ($id) {
            $query = Item::whereId($id);
        } else {
            $query = Item::query();
            $page = $request->get('page', 0);
            $count = $request->get('count', 10);
            $order_by = $request->get('sort');

            $offset = $page * $count;
            if ($order_by === 'random') {
                $query = $query->inRandomOrder();
            }
        }
        $query->with(['variants', 'images', 'categories']);
        if (!is_null($name_like = $request->get('name_like', null))) {
            $query = $query->where('name', 'like', "%$name_like%");
        }
        if (!is_null($category_id = $request->get('category_id', null))) {
            $category = Category::find($category_id);

            $query->where(function ($query) use ($category) {
                $query->whereRelation('categories', 'category_id', $category->id);
                foreach ($category->all_subcategories() as $c) {
                    $query->orWhereRelation('categories', 'category_id', $c->id);
                }
            });

            // $query->whereRelation('categories', 'category_id', $category->id);
        }
        if (!is_null($brand_id = $request->get('brand_id', null))) {
            $query->where('brand_id', '=', $brand_id);
        }
        if (!is_null($price_max = $request->get('price__le', null))) {
            $query = $query->whereRelation('variants', 'price', '<=', $price_max);
        }
        if (!is_null($price_min = $request->get('price__ge', null))) {
            $query = $query->whereRelation('variants', 'price', '>=', $price_min);
        }

        $total = $query->count();

        $items = $query->offset($offset)->limit($count)->get();
        foreach ($items as &$item) {
            foreach ($item->variants as $index => $variant) {
                if ($index === 0) {
                    $item->price_min = $variant->price;
                    $item->price_max = $variant->price;
                } else {
                    $item->price_min = min($item->price_min, $variant->price);
                    $item->price_max = max($item->price_max, $variant->price);
                }
            }
        }

        return [
            'data' => $items,
            'count' => count($items),
            'total' => $total
        ];
    }

    public function item(Request $request, $id)
    {
        $item = Item::with('images')->find($id);
        if (!$item) {
            return response()->json(['message' => 'Not found!'], 404);
        }

        foreach ($item->variants as $index => &$variant) {
            $promo = OverallPromoItem::get_item_active_promo($variant->sku);
            if ($promo) {
                $variant->original_price = $variant->price;
                $variant->price = $promo->price;
            }
            if ($index === 0) {
                $item->price_min = $variant->price;
                $item->price_max = $variant->price;
            } else {
                $item->price_min = min($item->price_min, $variant->price);
                $item->price_max = max($item->price_max, $variant->price);
            }
            $cart_item = Auth::user()?->cart->contents()->wherePivot('sku', $variant->sku)->first();
            $in_cart = $cart_item?->pivot?->quantity ?? 0;
            $variant->remaining_stock = $variant->stock - $in_cart;
        }

        return [
            'entity' => $item,
            'meta' => $item->metaFields()
        ];
    }

    public function image(int $item_id, int $index = 0)
    {
        $item = Item::with('images')->find($item_id);
        if ($index > $item->images()->count()) {
            $index = 0;
        }
        $image = $item->images()->offset($index)->first();

        $path = $image ?
            Storage::path(ItemImage::UPLOAD_DIRECTORY . $image->getAttributes()['file_name'])
            : public_path('/img/placeholder-300x300.png');
        return response()->file($path);
    }
}
