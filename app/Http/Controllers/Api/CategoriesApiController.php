<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesApiController extends Controller
{

    /**
     * Undocumented function
     *
     * @param Category[] $categories
     * @param int[] $path
     * @return Category[]
     */
    private function build_category_tree_path($categories, $path)
    {
        $node = array_pop($path);
        foreach ($categories as &$category) {
            if ($category->id === $node) {
                $category->subcategories_count = $category->subcategories->count();
                $category->subcategories = $this->build_category_tree_path($category->subcategories, $path);
            }
        }
        return $categories;
    }

    public function index(Request $request)
    {
        $order_by = $request->get('sort', 'id');
        $order_by_dir = $request->get('sortOrder', 'asc');
        $parent_id = $request->get('parent_id');

        $category_id = $request->get('category_id');

        $categories = Category::query()->whereParentId($parent_id)->withCount('subcategories')->orderBy($order_by, $order_by_dir)->get();

        $response = [];

        if ($category_id) {
            /** @var Category */
            $last_category = Category::find($category_id);
            $response['path'] = $path_to_root = $last_category->get_path_to_root();

            $categories = $this->build_category_tree_path($categories, $path_to_root);
        }

        $response['data'] = $categories;

        return $response;
    }
}
