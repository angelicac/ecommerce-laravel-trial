<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CustomerApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addresses(Request $request)
    {
        if ($request->isMethod('GET')) {
            return [
                'data' => Auth::user()->addresses
            ];
        }
        $keys = ['name', 'barangay_id', 'city_id', 'province_id', 'region_id', 'street', 'zip_code'];
        $data = [];
        foreach ($keys as $key) {
            $data[$key] = $request->post($key);
        }
        Auth::user()->addresses()->create($data);
        return response()->json([
            'data' => 'Successfully registered address!'
        ], 201);
    }

    public function update(Request $request)
    {
        $data = $request->all();

        Validator::make($data, [
            'first_name' => ['required'],
            'last_name' => ['required'],
            'email' => ['required', Rule::unique(User::class, 'email')->ignore(Auth::user()->id)],
            'contact_number' => ['required'],
            'birth_date' => ['required'],
            'gender' => ['required']
        ])->validate();

        User::query()->whereId(Auth::user()->id)->update(Arr::only($data, [
            'first_name',
            'last_name',
            'email',
            'contact_number',
            'birth_date',
            'gender'
        ]));
    }

    public function change_password(Request $request)
    {
        $data = $request->all();

        Validator::make($data, [
            'current_password' => ['current_password'],
            'password' => ['required'],
            'password_conf' => ['required', 'same:password']
        ], [
            'password_conf.same' => 'Passwords do not match.'
        ])->validate();

        $user = User::whereId(Auth::user()->id)->first();
        if (Hash::check($data['password'], $user->password)) {
            return response()->json(['errors' => ['password' => ['Your new password is the same as your current password.']]], 422);
        }
        $user->password = Hash::make($data['password']);
        $user->save();
        Auth::setUser($user);
        return ['data' => 'Password changed successfully.'];
    }
}
