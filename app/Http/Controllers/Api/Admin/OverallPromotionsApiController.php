<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\ItemVariant;
use App\Models\OverallPromoItem;
use Illuminate\Http\Request;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class OverallPromotionsApiController extends AdminApiController
{
    protected const CONTROLLER_MODEL = OverallPromoItem::class;

    protected $search_ajax_keys = [];

    protected $index_relations_with = ['item'];
    protected $show_relations_with = ['item'];

    protected function get_store_validation_rules(array $data): array
    {
        return [
            'sku' => [
                'required',
                Rule::exists(ItemVariant::class)
            ],
            'price' => [
                'required'
            ],
            'start_date' => [
                'required',
                'date',
                'before_or_equal:end_date',
            ],
            'end_date' => [
                'required',
                'date',
                'after_or_equal:start_date',
            ]
        ];
    }

    protected function get_update_validation_rules(array $data, string|int $id): array
    {
        return $this->get_store_validation_rules($data);
    }

    /**
     * 
     * @param Request $request 
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function store(Request $request, &$entity = null)
    {
        DB::beginTransaction();
        parent::store($request, entity: $entity);
        DB::commit();
        return response()->json(['id' => $entity->id], 201);
    }

    public function show(Request $request, $id = 0)
    {
        return parent::show($request, $id);
    }

    public function update(Request $request, $id, &$entity = null)
    {
        DB::beginTransaction();
        $response = parent::update($request, $id, entity: $entity);
        if ($response->status() === 404) return $response;
        DB::commit();
        return response()->json(['message' => 'Updated!']);
    }
}
