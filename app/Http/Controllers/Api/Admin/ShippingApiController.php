<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\Shipping;
use App\Models\ShippingFee;
use App\Models\ShippingMethod;
use App\Models\ShippingLocation;
use App\Settings\SiteSettings;
use Doctrine\DBAL\Driver\PDO\Exception;
use Illuminate\Http\Request;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Webit\Util\EvalMath\EvalMath;

class ShippingApiController extends AdminApiController
{

    protected const CONTROLLER_MODEL = ShippingLocation::class;

    protected $show_relations_with = ['fees'];

    public function default_shipping_fee()
    {
        return ['data' => app(SiteSettings::class)->default_shipping_fee];
    }

    public function set_default_shipping_fee(SiteSettings $settings, Request $request)
    {
        $settings->default_shipping_fee = $request->post('default_shipping_fee');
        $settings->save();
        return [
            'message' => 'Updated!'
        ];
    }

    public function set_shipping_fee(Request $request)
    {
        $fee = new ShippingFee([]);
        $fee->save();
        ShippingFee::updateOrCreate([
            'brgy_id' => $request->post('brgy_id'),
            'city_id' => $request->post('city_id'),
            'province_id' => $request->post('province_id'),
            'region_id' => $request->post('region_id'),
        ], [
            'shipping_fee' => $request->post('shipping_fee'),
            'name' => $request->post('name')
        ]);
        return [
            'message' => 'Saved!'
        ];
    }

    public function get_region_shipping_fees(Request $request)
    {
        return [
            'data' => ShippingFee::whereBrgyId(null)->whereCityId(null)->whereProvinceId(null)->get()
        ];
    }

    public function get_province_shipping_fees(Request $request)
    {
        return [
            'data' => ShippingFee::whereBrgyId(null)->whereCityId(null)->whereNotNull('province_id')->get()
        ];
    }

    public function get_city_shipping_fees(Request $request)
    {
        return [
            'data' => ShippingFee::whereBrgyId(null)->whereNotNull(['province_id', 'city_id'])->get()
        ];
    }

    public function show(Request $request, $id = 0)
    {
        $query = $this->controller_model->query();
        foreach ($this->show_relations_with as $relation) {
            $query = $query->with($relation);
        }
        $entity = $query->where('id', $id)->first();

        return [
            'data' => $entity
        ];
    }

    protected function check_fees($fees): array
    {
        $errors = [];

        $counter = 0;

        foreach ($fees as $index => $fee) {
            $error_message = "";
            if ($fee['shipping_method_id'] === ShippingMethod::FLAT_RATE) {
                // Check if numeric
                $pattern = "/^[1-9]\d*(\.\d+)?$/";
                if (!preg_match($pattern, $fee['fee'])) {
                    $error_message = "Invalid price.";
                }
            } else if ($fee['shipping_method_id'] === ShippingMethod::CALCULATED) {
                $m = new EvalMath();
                $formula = str_replace(
                    ['[cost]', '[qty]', '[wt]', '[dim]'],
                    [10, 10, 10, 10],
                    $fee['fee']
                );
                try {
                    $result = $m->evaluate($formula);
                } catch (Exception) {
                    $error_message = "Invalid formula.";
                }
            }
            if ($error_message) {
                for (; $counter <= $index; $counter++) {
                    $errors[$counter] = [];
                }
                $errors[$index][] = $error_message;
            }
        }

        return $errors;
    }

    /**
     * 
     * @param Request $request 
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function store(Request $request, &$entity = null)
    {
        DB::beginTransaction();

        $fee_errors = $this->check_fees($request->post('one2m')['fees']['data']);

        if (count($fee_errors)) {
            return response()->json(['errors' => ['fees' => $fee_errors]], 422);
        }
        parent::store($request, entity: $entity);
        DB::commit();
        return response()->json(['id' => $entity->id], 201);
    }

    public function shipping_methods()
    {
        return [
            'data' => ShippingMethod::query()->get()->all()
        ];
    }
}
