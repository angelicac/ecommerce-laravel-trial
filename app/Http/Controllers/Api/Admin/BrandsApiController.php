<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class BrandsApiController extends AdminApiController
{

    protected const CONTROLLER_MODEL = Brand::class;

    protected $search_ajax_keys = ['global_search' => 'name__like'];

    protected function get_store_validation_rules(array $data): array
    {
        return [
            'name' => [
                'required',
                Rule::unique(Brand::class, 'name')
            ]
        ];
    }

    protected function get_update_validation_rules(array $data, string|int $id): array
    {
        return [
            'name' => [
                'required',
                Rule::unique(Brand::class, 'name')->ignore($id)
            ]
        ];
    }

    /**
     * 
     * @param Request $request 
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function store(Request $request, &$entity = null)
    {
        DB::beginTransaction();
        parent::store($request, entity: $entity);
        DB::commit();
        return response()->json(['id' => $entity->id], 201);
    }

    public function show(Request $request, $id = 0)
    {
        $query = $this->controller_model->query();
        foreach ($this->show_relations_with as $relation) {
            $query = $query->with($relation);
        }
        $entity = $query->where('id', $id)->first();

        return [
            'data' => $entity
        ];
    }

    public function update(Request $request, $id, &$entity = null)
    {
        DB::beginTransaction();
        $response = parent::update($request, $id, entity: $entity);
        if ($response->status() === 404) return $response;
        DB::commit();
        return response()->json(['message' => 'Updated!']);
    }
}
