<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\PriceCode;
use Illuminate\Http\Request;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class PriceCodesApiController extends AdminApiController
{
    protected const CONTROLLER_MODEL = PriceCode::class;

    protected $search_ajax_keys = ['global_search' => 'name__like'];

    protected $show_relations_with = ['items'];

    protected function get_store_validation_rules(array $data): array
    {
        return [
            'name' => [
                'required',
                Rule::unique(PriceCode::class, 'name')
            ]
        ];
    }

    protected function get_update_validation_rules(array $data, string|int $id): array
    {
        return [
            'name' => [
                'required',
                Rule::unique(PriceCode::class, 'name')->ignore($id)
            ]
        ];
    }

    /**
     * 
     * @param Request $request 
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function store(Request $request, &$entity = null)
    {
        DB::beginTransaction();
        parent::store($request, entity: $entity);
        DB::commit();
        return response()->json(['id' => $entity->id], 201);
    }

    public function show(Request $request, $id = 0)
    {
        return parent::show($request, $id);
    }

    public function update(Request $request, $id, &$entity = null)
    {
        DB::beginTransaction();
        $response = parent::update($request, $id, entity: $entity);
        if ($response->status() === 404) return $response;
        DB::commit();
        return response()->json(['message' => 'Updated!']);
    }
}
