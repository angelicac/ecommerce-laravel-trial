<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Http\Request;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CustomersApiController extends AdminApiController
{
    protected const CONTROLLER_MODEL = User::class;

    protected $search_ajax_keys = [];

    protected function get_store_validation_rules(array $data): array
    {
        return [];
    }

    protected function get_update_validation_rules(array $data, string|int $id): array
    {
        return [
            'price_code_id' => [
                'required',
            ]
        ];
    }

    protected $search_ajax_default_filter = [
        'user_type' => [
            'operation' => '!=',
            'value' => UserType::ADMIN
        ]
    ];

    /**
     * 
     * @param Request $request 
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function store(Request $request, &$entity = null)
    {
        DB::beginTransaction();
        parent::store($request, entity: $entity);
        DB::commit();
        return response()->json(['id' => $entity->id], 201);
    }

    public function show(Request $request, $id = 0)
    {
        return parent::show($request, $id);
    }

    public function update(Request $request, $id, &$entity = null)
    {
        DB::beginTransaction();
        $entity = $this->controller_model->query()->where('id', $id)->first();
        if (!$entity) return response()->json(['message' => "Entity does not exist"], 404);
        $data = $request->post();
        Validator::make($data, $this->get_update_validation_rules($data, $id))->validate();
        $entity->price_code_id = $data['price_code_id'];
        $entity->save();
        DB::commit();
        return response()->json(['message' => 'Updated!']);
    }
}
