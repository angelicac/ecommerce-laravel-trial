<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DomainException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

abstract class AdminApiController extends Controller
{

    protected $search_ajax_keys = [];
    protected $search_ajax_default_filter = [];

    protected $list_view_show_add = true;

    protected $insert_view_link = '';
    protected $insert_view_link_text = 'Add New';

    protected const CONTROLLER_MODEL = '';
    protected Model $controller_model;

    protected $show_relations_with = [];
    protected $index_relations_with = [];

    public const ALLOW_DELETE = false;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware([
            'auth.admin',
            'admin'
        ]);
        if (static::CONTROLLER_MODEL) {
            $model = static::CONTROLLER_MODEL;
            $this->controller_model = new $model();
        }
        $this->insert_view_link = $this->insert_view_link ?: "admin/" . strtolower(class_basename(static::class)) . "/show";
    }

    // Views

    public function index(Request $request)
    {
        $page = (int)$request->get('page', 0);
        $rows = (int)$request->get('rows', 10);
        $offset = $rows * $page;

        $query = $this->controller_model->query();

        if ($request->input('deleted')) {
            $query = $query->onlyTrashed();
        }

        foreach ($this->index_relations_with as $relation) {
            $query = $query->with($relation);
        }

        $filter = $this->get_search_filter($request);
        foreach ($filter as $col => ['operation' => $op, 'value' => $val]) {
            $query->where($col, $op, $val);
        }
        return [
            'totalRecords' => $query->count(),
            'data' => $query->offset($offset)->limit($rows)->get()
        ];
    }

    /**
     * 
     * @param Request $request 
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function store(Request $request, &$entity = null)
    {
        $data = [];
        foreach ($this->controller_model->fillable as $column) {
            if (!$request->has($column))
                continue;
            $data[$column] = $request->input($column);
        }
        Validator::make($request->all(), $this->get_store_validation_rules($request->all()))->validate();
        $entity = $this->controller_model->create($data);
        if (property_exists($this->controller_model, 'm2m')) {
            foreach ($request->post('m2m', []) as $relation => $relation_data) {
                if (array_key_exists($relation, $this->controller_model->m2m)) {
                    $class = $this->controller_model->m2m[$relation];
                    if (!is_null($class)) {
                        $fillable = (new $class)->fillable ?? [];
                        $relation_data = array_map(function ($data) use ($fillable) {
                            return Arr::only($data, $fillable);
                        }, $relation_data);
                    }
                    $entity->{$relation}()->attach($relation_data);
                }
            }
        }
        if (property_exists($this->controller_model, 'one2m')) {
            foreach ($request->post('one2m', []) as $relation => ['data' => $relation_data]) {
                if (in_array($relation, $this->controller_model->one2m)) {
                    $entity->{$relation}()->createMany($relation_data);
                }
            }
        }
        return response()->json(['id' => $entity->id], 201);
    }

    public function show(Request $request, $id = 0)
    {
        $query = $this->controller_model->query();
        foreach ($this->show_relations_with as $relation) {
            $query = $query->with($relation);
        }
        $entity = $query->where('id', $id)->first();

        return [
            'data' => $entity
        ];
    }

    public function update(Request $request, $id, &$entity = null)
    {
        DB::beginTransaction();
        $entity = $this->controller_model->query()->where('id', $id)->first();
        if (!$entity) return response()->json(['message' => "Entity does not exist"], 404);

        $data = [];
        foreach ($this->controller_model->fillable as $column) {
            if (!$request->has($column))
                continue;
            $data[$column] = $request->input($column);
        }
        Validator::make($request->all(), $this->get_update_validation_rules($request->all(), $id))->validate();
        $entity->update($data);
        if (property_exists($this->controller_model, 'm2m')) {
            foreach ($request->post('m2m', []) as $relation => $relation_data) {
                if (array_key_exists($relation, $this->controller_model->m2m)) {
                    $class = $this->controller_model->m2m[$relation];
                    if (!is_null($class)) {
                        $fillable = (new $class)->fillable ?? [];
                        $relation_data = array_map(function ($data) use ($fillable) {
                            return Arr::only($data, $fillable);
                        }, $relation_data);
                    }
                    $entity->{$relation}()->detach();
                    $entity->{$relation}()->attach($relation_data);
                }
            }
        }
        if (property_exists($this->controller_model, 'one2m')) {
            foreach ($request->post('one2m', []) as $relation => ['data' => $relation_data, 'delete' => $to_delete]) {
                if (in_array($relation, $this->controller_model->one2m)) {
                    foreach ($to_delete as $to_delete_id) {
                        $entity->{$relation}()->find($to_delete_id)->delete();
                    }
                    foreach ($relation_data as $relation_datum) {
                        if (array_key_exists('id', $relation_datum)) {
                            $entity->{$relation}()->where('id', '=', $relation_datum['id'])->update($relation_datum);
                        } else {
                            $entity->{$relation}()->create($relation_datum);
                        }
                    }
                }
            }
        }
        DB::commit();
        return response()->json(['message' => 'Updated!']);
    }

    public function destroy(Request $request, $id)
    {
        // Return 405 if not explicitly set to true
        if (!static::ALLOW_DELETE)
            return response()->json([], HttpResponse::HTTP_METHOD_NOT_ALLOWED);

        try {
            $entity = $this->controller_model->findOrFail($id);
            $entity->delete();
            return response()->json(['message' => 'Deleted'], status: HttpResponse::HTTP_NO_CONTENT);
        } catch (ModelNotFoundException $ex) {
            return response()
                ->json(['message' => 'Entity does not exist'], status: HttpResponse::HTTP_NOT_FOUND);
        }
    }

    public function restore(Request $request, $id)
    {
        try {
            $entity = $this->controller_model->onlyTrashed()->findOrFail($id);
            $entity->restore();
            return response()->json(['message' => "Entity restored!"]);
        } catch (ModelNotFoundException $ex) {
            return response()
                ->json(['message' => 'Entity does not exist'], status: HttpResponse::HTTP_NOT_FOUND);
        }
    }

    protected function get_search_filter(Request $request): array
    {
        $filter = [];
        $operations = [
            "" => '',
            "ne" => "!=",
            "gt" => ">",
            "ge" => ">=",
            "lt" => "<",
            "le" => "<=",
            "like" => "like",
        ];
        foreach ($this->search_ajax_keys as $key => $mapping) {
            if (is_integer($key)) {
                $param = $mapping;
            } else {
                $param = $key;
            }
            if ($value = $request->get($param)) {
                if (count($exploded = explode("__", $mapping)) === 2) {
                    [$col, $op_code] = $exploded;
                    $op = \Illuminate\Support\Arr::get($operations, $op_code, "=");
                } else {
                    $col = $mapping;
                    $op = '=';
                }
                if ($op === 'like') {
                    $value = "%$value%";
                }
                $filter[$col] = [
                    'operation' => $op,
                    'value' => $value
                ];
            }
        }
        foreach ($this->search_ajax_default_filter as $key => $value) {
            $filter[$key] = $value;
        }
        return $filter;
    }

    protected function get_store_validation_rules(array $data): array
    {
        return [];
    }

    protected function get_update_validation_rules(array $data, string|int $id): array
    {
        return [];
    }

    public static function kebab()
    {
        return Str::kebab(Str::replaceLast('ApiController', '', class_basename(static::class)));
    }

    public static function allow_delete()
    {
        return static::ALLOW_DELETE;
    }
}
