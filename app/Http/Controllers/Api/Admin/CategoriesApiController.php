<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Models\Category;
use Illuminate\Support\Facades\Log;

class CategoriesApiController extends AdminApiController
{
    protected const CONTROLLER_MODEL = Category::class;

    protected $search_ajax_keys = ['global_search' => 'name__like', 'id__ne'];

    protected $show_relations_with = ['parent_category'];

    protected function get_store_validation_rules(array $data): array
    {
        return [
            'name' => [
                'required',
                Rule::unique(Category::class, 'name')
            ]
        ];
    }

    protected function get_update_validation_rules(array $data, string|int $id): array
    {
        return [
            'name' => [
                'required',
                Rule::unique(Category::class, 'name')->ignore($id)
            ]
        ];
    }

    /**
     * 
     * @param Request $request 
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function store(Request $request, &$entity = null)
    {
        DB::beginTransaction();
        parent::store($request, entity: $entity);
        DB::commit();
        return response()->json(['id' => $entity->id], 201);
    }

    public function show(Request $request, $id = 0)
    {
        return parent::show($request, $id);
    }

    public function update(Request $request, $id, &$entity = null)
    {
        DB::beginTransaction();

        // Check if setting the parent_id would create an infinite loop

        $parent_id = $request->input('parent_id');
        $id = (int)$id;

        do {
            $parent = Category::find($parent_id);
            if ($parent?->id === $id)
                return response()->json(['errors' => ['parent_id' => [
                    'Invalid parent category.'
                ]]], 422);
            $parent_id = $parent?->parent_category?->id;
        } while (!is_null($parent));


        $response = parent::update($request, $id, entity: $entity);
        if ($response->status() === 404) return $response;
        DB::commit();
        return response()->json(['message' => 'Updated!']);
    }
}
