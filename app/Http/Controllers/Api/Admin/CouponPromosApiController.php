<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\CouponPromo;
use App\Models\CouponPromoType;
use Illuminate\Http\Request;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class CouponPromosApiController extends AdminApiController
{
    protected const CONTROLLER_MODEL = CouponPromo::class;

    protected $search_ajax_keys = [];

    protected $show_relations_with = ['items'];

    protected function get_store_validation_rules(array $data): array
    {
        return [
            'code' => [
                'required',
                Rule::unique(CouponPromo::class, 'code')
            ],
            'type' => [
                'required',
                Rule::exists(CouponPromoType::class, 'id')
            ],
            'start_date' => [
                'required',
                'date',
                'before_or_equal:end_date',
            ],
            'end_date' => [
                'required',
                'date',
                'after_or_equal:start_date',
            ],
            'rate_threshold' => [
                Rule::requiredIf(function () use ($data) {
                    return Arr::get($data, 'type', 0) === CouponPromoType::REDUCTION;
                })
            ],
            'rate_reduction' => [
                Rule::requiredIf(function () use ($data) {
                    return Arr::get($data, 'type', 0) === CouponPromoType::REDUCTION;
                })
            ],
        ];
    }

    protected function get_update_validation_rules(array $data, string|int $id): array
    {
        $rules = $this->get_store_validation_rules($data);
        $rules['code'] = [
            'required',
            Rule::unique(CouponPromo::class, 'code')->ignore($id)
        ];
        return $rules;
    }

    /**
     * 
     * @param Request $request 
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function store(Request $request, &$entity = null)
    {
        DB::beginTransaction();
        parent::store($request, entity: $entity);
        DB::commit();
        return response()->json(['id' => $entity->id], 201);
    }

    public function show(Request $request, $id = 0)
    {
        return parent::show($request, $id);
    }

    public function update(Request $request, $id, &$entity = null)
    {
        DB::beginTransaction();
        $response = parent::update($request, $id, entity: $entity);
        if ($response->status() === 404) return $response;
        DB::commit();
        return response()->json(['message' => 'Updated!']);
    }
}
