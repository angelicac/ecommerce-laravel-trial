<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\Brand;
use App\Models\Item;
use App\Models\ItemImage;
use App\Models\ItemVariant;
use DomainException;
use Illuminate\Http\Request;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class ItemsApiController extends AdminApiController
{
    protected const CONTROLLER_MODEL = Item::class;

    protected $search_ajax_keys = [];

    protected $show_relations_with = ['variants', 'categories', 'images'];

    protected function get_store_validation_rules(array $data): array
    {
        return [
            'name' => [
                'required',
                Rule::unique(Item::class, 'name')
            ],
            'brand_id' => [
                'required',
                Rule::exists(Brand::class, 'id')
            ],
            'description' => [
                'required'
            ],
        ];
    }

    protected function get_update_validation_rules(array $data, string|int $id): array
    {
        $rules = $this->get_store_validation_rules($data);
        $rules['name'] = [
            'required',
            Rule::unique(Item::class, 'name')->ignore($id)
        ];
        return $rules;
    }

    protected function checkVariants($variants, $id = 0): array
    {
        $errors = [];

        $counter = 0;

        $sku_map = [];
        foreach ($variants as $index => $variant) {
            $sku = $variant['sku'];

            if (!array_key_exists($sku, $sku_map)) {
                $sku_map[$sku] = [];
            }
            $sku_map[$sku][] = $index;

            // Check if the SKU exists for a different item

            if (ItemVariant::whereSku($sku)->where('item_id', '!=', $id)->exists()) {
                for (; $counter <= $index; $counter++) {
                    $errors[$counter] = [];
                }
                $errors[$index][] = 'SKU exists';
            }

            if (strlen($sku) === 0) {
                $errors[$index][] = 'SKU cannot be empty';
            }

            if (!is_numeric($variant['price'])) {
                $errors[$index][] = 'Invalid Price';
            }
        }

        $counter = 0;

        // Check if duplicate SKUs exist in the table
        foreach ($sku_map as $sku => $lines) {
            if (count($lines) > 1) {
                foreach ($lines as $i => $line) {
                    if ($i == 0) {
                        continue;
                    }
                    for (; $counter <= $line; $counter++) {
                        $errors[$counter] = $errors[$counter] ?? [];
                    }
                    $errors[$line][] = 'Duplicate SKU in table';
                }
            }
        }
        return $errors;
    }

    /**
     * 
     * @param Request $request 
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function store(Request $request, &$entity = null)
    {
        DB::beginTransaction();


        $variant_errors = $this->checkVariants($variants = $request->post('one2m')['variants']['data']);
        if (count($variant_errors)) {
            return response()->json(['errors' => ['variants' => $variant_errors]], 422);
        }

        parent::store($request, entity: $entity);

        foreach ($variants as $variant) {
            $variant['stock'] ??= 0;
            $item_variant = $entity->variants()->create($variant);
            foreach ($variant['meta'] ?? [] as $meta => $value) {
                $item_variant->meta()->create(['name' => $meta, 'value' => $value]);
            }
        }

        DB::commit();
        return response()->json(['id' => $entity->id], 201);
    }

    public function show(Request $request, $id = 0)
    {
        $query = $this->controller_model->query();
        foreach ($this->show_relations_with as $relation) {
            $query = $query->with($relation);
        }
        $entity = $query->where('id', $id)->first();

        return [
            'data' => $entity,
            'meta' => $entity->metaFields()
        ];
    }

    public function update(Request $request, $id, &$entity = null)
    {
        DB::beginTransaction();

        $variant_errors = $this->checkVariants($variants = $request->post('one2m')['variants']['data'], $id);
        if (count($variant_errors)) {
            return response()->json(['errors' => ['variants' => $variant_errors]], 422);
        }

        $response = parent::update($request, $id, entity: $entity);
        if ($response->status() === 404) return $response;

        foreach ($variants as $variant) {
            $item_variant = $entity->variants()->updateOrCreate(['sku' => $variant['sku']], $variant);
            $item_variant->meta()->delete();
            $meta = [];
            foreach ($variant['meta'] ?? [] as $name => $value) {
                $meta[] = ['name' => $name, 'value' => $value];
            }
            $item_variant->meta()->createMany($meta);
        }

        $variants_to_delete = $request->post('one2m')['variants']['delete'];
        foreach ($variants_to_delete as $sku) {
            $entity->variants()->find($sku)->delete();
        }

        DB::commit();
        return response()->json(['message' => 'Updated!']);
    }

    public function upload_images(Request $request, $id)
    {
        // TODO: Clean up older images
        DB::beginTransaction();
        $key = 'images';
        $item = Item::find($id);
        $images = [];
        foreach ($request->file($key) ?? [] as $image) {
            $item_image = $item->images()->create(['file_name' => '']);
            $filename = "item-{$id}-" . $item_image->id . "." . $image->getClientOriginalExtension();
            Storage::putFileAs(ItemImage::UPLOAD_DIRECTORY, $image, $filename);
            $item_image->file_name = $filename;
            $item_image->save();
        }
        DB::commit();
        return ['message' => 'Uploaded!'];
    }
}
