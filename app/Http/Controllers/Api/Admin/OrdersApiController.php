<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\ItemVariant;
use App\Models\Order;
use App\Models\OrderStatus;
use Illuminate\Http\Request;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class OrdersApiController extends AdminApiController
{
    protected const CONTROLLER_MODEL = Order::class;

    protected $search_ajax_keys = [];

    protected $show_relations_with = ['items'];

    protected function get_store_validation_rules(array $data): array
    {
        return [];
    }

    protected function get_update_validation_rules(array $data, string|int $id): array
    {
        return [
            'order_status' => [
                Rule::exists(OrderStatus::class, 'id'),
                // Don't allow cancelling through the update endpoint
                // since we'll have a different endpoint for that
                Rule::notIn([OrderStatus::CANCELLED])
            ]
        ];
    }

    /**
     * 
     * @param Request $request 
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function store(Request $request, &$entity = null)
    {
        // Disable route
        return response()->json([], 405);
    }

    public function show(Request $request, $id = 0)
    {
        return parent::show($request, $id);
    }

    public function update(Request $request, $id, &$entity = null)
    {
        DB::beginTransaction();
        $response = parent::update($request, $id, entity: $entity);
        if ($response->status() === 404) return $response;
        DB::commit();
        return response()->json(['message' => 'Updated!']);
    }

    public function cancel_order(Request $request, $id)
    {
        DB::beginTransaction();
        $order = Order::find($id);
        $order->order_status = OrderStatus::CANCELLED;
        $order->save();

        // TODO: Make this optional
        foreach ($order->items as $item) {
            $variant = ItemVariant::whereSku($item->sku)->first();
            $variant->stock += $item->pivot->quantity;
            $variant->save();
        }
        DB::commit();
        return response()->json(['message' => 'Updated!']);
    }
}
