<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\ItemVariant;
use Illuminate\Http\Request;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ItemVariantsApiController extends AdminApiController
{
    protected const CONTROLLER_MODEL = ItemVariant::class;

    protected $search_ajax_keys = ['name__like' => 'items.name__like'];

    protected $index_relations_with = ['item'];

    protected function get_store_validation_rules(array $data): array
    {
        return [];
    }

    protected function get_update_validation_rules(array $data, string|int $id): array
    {
        return [];
    }

    public function index(Request $request)
    {
        $page = (int)$request->get('page', 0);
        $rows = (int)$request->get('rows', 10);
        $offset = $rows * $page;

        $query = $this->controller_model->query();
        $query->join('items', 'items.id', 'item_variants.item_id');
        foreach ($this->index_relations_with as $relation) {
            $query = $query->with($relation);
        }

        $filter = $this->get_search_filter($request);
        foreach ($filter as $col => ['operation' => $op, 'value' => $val]) {
            $query->where($col, $op, $val);
        }
        return [
            'totalRecords' => $query->count(),
            'data' => $query->offset($offset)->limit($rows)->get(),
        ];
    }
    /**
     * 
     * @param Request $request 
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function store(Request $request, &$entity = null)
    {
        DB::beginTransaction();
        parent::store($request, entity: $entity);
        DB::commit();
        return response()->json(['id' => $entity->id], 201);
    }

    public function show(Request $request, $id = 0)
    {
        return parent::show($request, $id);
    }

    public function update(Request $request, $id, &$entity = null)
    {
        DB::beginTransaction();
        $response = parent::update($request, $id, entity: $entity);
        if ($response->status() === 404) return $response;
        DB::commit();
        return response()->json(['message' => 'Updated!']);
    }
}
