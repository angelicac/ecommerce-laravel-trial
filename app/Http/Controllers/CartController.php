<?php

namespace App\Http\Controllers;

use App\Models\ShippingMethod;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        return view('cart.index');
    }

    public function checkout()
    {
        return view('cart.checkout', ['shippingMethods' => [
            'FLAT_RATE' => ShippingMethod::FLAT_RATE,
            'FREE_SHIPPING' => ShippingMethod::FREE_SHIPPING,
            'CALCULATED' => ShippingMethod::CALCULATED
        ]]);
    }
}
