<?php

declare(strict_types=1);

namespace App\Invoices;

use LaravelDaily\Invoices\Invoice;

/**
 * @method static OrderInvoice make()
 */
class OrderInvoice extends Invoice
{

    /**
     * @var float
     */
    public $original_total;

    /**
     * @var float
     */
    public $coupon_discount;

    /**
     * @var float
     */
    public $reduction_discount;

    /**
     * @return $this
     */
    public function originalTotal(float $original_total)
    {
        $this->original_total = $original_total;

        return $this;
    }

    /**
     * @return $this
     */
    public function couponDiscount(float $coupon_discount)
    {
        $this->coupon_discount = $coupon_discount;
        return $this;
    }

    /**
     * @return $this
     */
    public function reductionDiscount(float $reduction_discount)
    {
        $this->reduction_discount = $reduction_discount;
        return $this;
    }

    protected function calculate()
    {
        // Override to only compute item prices
        // since we already calculate the total at checkout 
        $this->items->each(
            function ($item) use (&$total_amount, &$total_discount, &$total_taxes) {
                // Gates
                if ($item->hasTax() && $this->hasTax()) {
                    throw new \Exception('Invoice: you must have taxes only on items or only on invoice.');
                }

                $item->calculate($this->currency_decimals);

                (!$item->hasUnits()) ?: $this->hasItemUnits = true;

                if ($item->hasDiscount()) {
                    $total_discount += $item->discount;
                    $this->hasItemDiscount = true;
                }

                if ($item->hasTax()) {
                    $total_taxes += $item->tax;
                    $this->hasItemTax = true;
                }

                // Totals
                $total_amount += $item->sub_total_price;
            }
        );

        $this->applyColspan();
        return $this;
    }
}
