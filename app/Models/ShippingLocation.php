<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ShippingLocation
 *
 * @property int $id
 * @property string|null $brgy_id
 * @property string|null $city_id
 * @property string|null $province_id
 * @property string|null $region_id
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingLocation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingLocation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingLocation query()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingLocation whereBrgyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingLocation whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingLocation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingLocation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingLocation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingLocation whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingLocation whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingLocation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ShippingLocation extends Model
{

    public $fillable = ['brgy_id', 'city_id', 'province_id', 'region_id', 'shipping_fee', 'name'];

    public $one2m = [
        'fees'
    ];

    use HasFactory;

    public function fees()
    {
        return $this->hasMany(ShippingFee::class);
    }
}
