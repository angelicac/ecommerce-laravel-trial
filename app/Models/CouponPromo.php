<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CouponPromo
 *
 * @property int $id
 * @property string $code
 * @property int $type
 * @property string|null $rate_threshold
 * @property string|null $rate_reduction
 * @property string $start_date
 * @property string $end_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ItemVariant[] $items
 * @property-read int|null $items_count
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromo query()
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromo whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromo whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromo whereRateReduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromo whereRateThreshold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromo whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromo whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromo whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CouponPromo extends Model
{
    use HasFactory;

    public $m2m = [
        'items' => CouponPromoItem::class
    ];

    public $fillable = ['code', 'type', 'rate_threshold', 'rate_reduction', 'start_date', 'end_date'];

    public function items()
    {
        return $this->belongsToMany(ItemVariant::class, 'coupon_promo_items', 'coupon_promo_id', 'sku', 'id', 'sku')->withPivot('price')->with('item');
    }
}
