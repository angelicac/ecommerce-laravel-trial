<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CouponPromoType
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromoType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromoType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromoType query()
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromoType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromoType whereName($value)
 * @mixin \Eloquent
 */
class CouponPromoType extends Model
{
    use HasFactory;

    public const FLAT = 1;
    public const REDUCTION = 2;
}
