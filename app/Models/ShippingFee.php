<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ShippingFee
 *
 * @property int $id
 * @property int $shipping_location_id
 * @property int $shipping_method_id
 * @property string $fee
 * @property string $minimum_price
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingFee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingFee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingFee query()
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingFee whereFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingFee whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingFee whereMinimumPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingFee whereShippingLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShippingFee whereShippingMethodId($value)
 * @mixin \Eloquent
 */
class ShippingFee extends Model
{
    use HasFactory;

    public $timestamps = false;

    public $with = ['method'];

    public $fillable = ['shipping_location_id', 'shipping_method_id', 'fee', 'minimum_price'];

    public function method()
    {
        return $this->belongsTo(ShippingMethod::class, 'shipping_method_id', 'id');
    }
}
