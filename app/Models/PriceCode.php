<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PriceCode
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ItemVariant[] $items
 * @property-read int|null $items_count
 * @method static \Database\Factories\PriceCodeFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCode newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCode newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCode query()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCode whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCode whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCode whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PriceCode extends Model
{
    use HasFactory;

    public $fillable = [
        'name'
    ];

    public $m2m = [
        'items' => PriceCodeItem::class
    ];

    public function items()
    {
        return $this->belongsToMany(ItemVariant::class, 'price_code_items', 'price_code_id', 'sku', 'id', 'sku')->withPivot('price')->with('item');
    }
}
