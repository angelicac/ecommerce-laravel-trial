<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Item[] $items
 * @property-read int|null $items_count
 * @property-read Category|null $parent_category
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $subcategories
 * @property-read int|null $subcategories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $subcategories_recursive
 * @property-read int|null $subcategories_recursive_count
 * @method static \Database\Factories\CategoryFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Category extends Model
{
    use HasFactory;

    public $fillable = ['name', 'parent_id'];

    public function items()
    {
        return $this->belongsToMany(Item::class, 'item_categories', 'category_id', 'item_id');
    }

    public function parent_category()
    {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }

    public function subcategories()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function all_subcategories()
    {
        $subcategories = $this->subcategories;
        foreach ($this->subcategories as $subcategory) {
            $subcategories = $subcategories->concat($subcategory->all_subcategories());
        }
        return $subcategories;
    }

    public function subcategories_recursive()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')->with('subcategories_recursive');
    }

    /**
     * Undocumented function
     *
     * @return int[]
     */
    public function get_path_to_root()
    {
        $current = $this;
        $path = [];
        while ($current) {
            $path[] = $current->id;
            $current = $current->parent_category;
        }
        return $path;
    }
}
