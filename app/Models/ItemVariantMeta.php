<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ItemVariantMeta
 *
 * @property int $id
 * @property string $sku
 * @property string $name
 * @property string $value
 * @method static \Database\Factories\ItemVariantMetaFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariantMeta newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariantMeta newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariantMeta query()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariantMeta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariantMeta whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariantMeta whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariantMeta whereValue($value)
 * @mixin \Eloquent
 */
class ItemVariantMeta extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'item_variant_meta';

    public $fillable = ['name', 'value'];
}
