<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CouponPromoItem
 *
 * @property int $id
 * @property string $sku
 * @property float $price
 * @property int $coupon_promo_id
 * @property-read \App\Models\CouponPromo $coupon_promo
 * @property-read \App\Models\ItemVariant $item
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromoItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromoItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromoItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromoItem whereCouponPromoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromoItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromoItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CouponPromoItem whereSku($value)
 * @mixin \Eloquent
 */
class CouponPromoItem extends Model
{
    use HasFactory;

    public $timestamps = false;

    public $fillable = ['sku', 'price'];

    protected $casts = [
        'price' => 'float'
    ];

    public function coupon_promo()
    {
        return $this->belongsTo(CouponPromo::class);
    }

    public function item()
    {
        return $this->belongsTo(ItemVariant::class, 'sku', 'sku');
    }
}
