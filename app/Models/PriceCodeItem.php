<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PriceCodeItem
 *
 * @property int $id
 * @property string $sku
 * @property int $price_code_id
 * @property float $price
 * @property-read \App\Models\ItemVariant $item
 * @property-read \App\Models\PriceCode $price_code
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCodeItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCodeItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCodeItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCodeItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCodeItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCodeItem wherePriceCodeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PriceCodeItem whereSku($value)
 * @mixin \Eloquent
 */
class PriceCodeItem extends Model
{
    use HasFactory;

    public $timestamps = false;

    public $fillable = ['sku', 'price'];

    protected $casts = [
        'price' => 'float'
    ];

    public function price_code()
    {
        return $this->belongsTo(PriceCode::class);
    }

    public function item()
    {
        return $this->belongsTo(ItemVariant::class, 'sku', 'sku');
    }
}
