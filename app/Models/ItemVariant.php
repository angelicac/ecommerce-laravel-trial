<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ItemVariant
 *
 * @property string $sku
 * @property int $item_id
 * @property float $price
 * @property int $stock
 * @property-read \App\Models\Item $item
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ItemVariantMeta[] $meta
 * @property-read int|null $meta_count
 * @method static \Database\Factories\ItemVariantFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariant query()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariant whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariant wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariant whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemVariant whereStock($value)
 * @mixin \Eloquent
 */
class ItemVariant extends Model
{
    use HasFactory;

    public $fillable = ['sku', 'price', 'stock', 'weight'];

    public $timestamps = false;

    protected $primaryKey = 'sku';

    public $incrementing = false;

    protected $casts = [
        'price' => 'float'
    ];

    public function meta()
    {
        return $this->hasMany(ItemVariantMeta::class, 'sku', 'sku');
    }

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }
}
