<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

/**
 * App\Models\Cart
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $session_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ItemVariant[] $contents
 * @property-read int|null $contents_count
 * @method static \Database\Factories\CartFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Cart newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Cart query()
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereUserId($value)
 * @mixin \Eloquent
 */
class Cart extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'session_id'];

    // we expect the cart to always be loaded with its contents
    protected $with = [
        'contents'
    ];

    public function contents()
    {
        return $this->belongsToMany(
            ItemVariant::class,
            'cart_item_variant',
            'cart_id',
            'sku'
        )->withPivot(
            ['quantity', 'notes']
        )->with(
            ['meta', 'item']
        );
    }

    public function add(string $sku, int $quantity)
    {
        $item = $this->contents()->wherePivot('sku', $sku)->first();
        if (!$item) {
            $this->contents()->attach([$sku => ['quantity' => $quantity]]);
        } else {
            $this->contents()->updateExistingPivot($item, ['quantity' => $item->pivot->quantity + $quantity]);
        }
    }

    public function remove(string $sku, int $quantity)
    {
        $item = $this->contents()->wherePivot('sku', $sku)->first();
        if ($item) {
            $new_quantity = $item->pivot->quantity - $quantity;
            if ($new_quantity < 1) {
                $this->contents()->detach($sku);
            } else {
                $this->contents()->updateExistingPivot($item, ['quantity' => $new_quantity]);
            }
        }
    }

    public function remove_all(string $sku)
    {
        $this->contents()->detach($sku);
    }

    public function quantity(?string $sku = null)
    {
        $items = $this->contents();
        if ($sku) {
            $items = $items->wherePivot('sku', $sku);
        }
        return $items->get()->sum('pivot.quantity');
    }
}
