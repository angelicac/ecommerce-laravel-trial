<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ItemImage
 *
 * @property int $id
 * @property int $item_id
 * @property string $file_name
 * @method static \Database\Factories\ItemImageFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemImage query()
 * @method static \Illuminate\Database\Eloquent\Builder|ItemImage whereFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemImage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ItemImage whereItemId($value)
 * @mixin \Eloquent
 */
class ItemImage extends Model
{
    use HasFactory;

    public const UPLOAD_DIRECTORY = 'public/img/items/';
    public const PUBLIC_DIRECTORY = 'storage/img/items/';

    public $fillable = ['file_name'];
    public $timestamps = false;

    public function getFileNameAttribute($value)
    {
        return asset(static::PUBLIC_DIRECTORY . $value);
    }
}
