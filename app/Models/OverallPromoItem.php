<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;

/**
 * App\Models\OverallPromoItem
 *
 * @property int $id
 * @property string $sku
 * @property float $price
 * @property string $start_date
 * @property string $end_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ItemVariant $item
 * @method static \Database\Factories\OverallPromoItemFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|OverallPromoItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OverallPromoItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OverallPromoItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|OverallPromoItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OverallPromoItem whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OverallPromoItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OverallPromoItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OverallPromoItem whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OverallPromoItem whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OverallPromoItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OverallPromoItem extends Model
{
    use HasFactory;

    public $fillable = ['sku', 'price', 'start_date', 'end_date'];

    protected $casts = [
        'price' => 'float'
    ];

    /**
     * 
     * @param string $sku 
     * @return OverallPromoItem|null 
     * @throws InvalidArgumentException 
     */
    public static function get_item_active_promo(string $sku)
    {
        $today = (new \DateTime())->format('Y-m-d');
        return static::query()
            ->whereSku($sku)
            ->where('start_date', '<=', $today)
            ->where('end_date', '>=', $today)
            ->first();
    }

    public function item()
    {
        return $this->belongsTo(ItemVariant::class, 'sku', 'sku')->with('item');
    }
}
