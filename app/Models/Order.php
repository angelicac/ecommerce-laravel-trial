<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property string $number
 * @property float $total
 * @property float $original_total
 * @property float $shipping_fee
 * @property float $coupon_discount_total
 * @property int|null $customer_id
 * @property string $first_name
 * @property string $last_name
 * @property string $contact_number
 * @property string $email
 * @property string $street
 * @property string $region_id
 * @property string $province_id
 * @property string $city_id
 * @property string $barangay_id
 * @property string $zip_code
 * @property int $order_status
 * @property string $delivery_instructions
 * @property int|null $coupon_promo_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read float $discount
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ItemVariant[] $items
 * @property-read int|null $items_count
 * @property-read \App\Models\OrderStatus $status
 * @method static \Database\Factories\OrderFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereBarangayId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereContactNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCouponDiscountTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCouponPromoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeliveryInstructions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereOrderStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereOriginalTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereShippingFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereZipCode($value)
 * @mixin \Eloquent
 */
class Order extends Model
{
    use HasFactory;

    public $fillable = [
        'number',
        'total',
        'original_total',
        'shipping_fee',
        'coupon_discount_total',
        'customer_id',
        'first_name',
        'last_name',
        'contact_number',
        'email',
        'street',
        'region_id',
        'province_id',
        'city_id',
        'barangay_id',
        'zip_code',
        'order_status',
        'delivery_instructions',
    ];

    protected $casts = [
        'total' => 'float',
        'original_total' => 'float',
        'shipping_fee' => 'float',
        'coupon_discount_total' => 'float',
    ];

    protected $appends = ['discount'];

    public function items()
    {
        return $this->belongsToMany(
            ItemVariant::class,
            'order_items',
            'order_id',
            'sku',
            'id',
            'sku'
        )->withPivot(['price', 'original_price', 'quantity'])->with('item')->with('meta');
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'order_status', 'id');
    }

    public static function get_next_order_number()
    {
        return DB::select(
            "SELECT 
                LPAD(CAST(partial_number + 1 AS CHAR(10)), 10, '0') AS next_number 
            FROM (
                SELECT 
                partial_number 
                FROM ( 
                    SELECT 
                        partial_number 
                    FROM ( 
                        SELECT 
                        CAST(number AS CHAR(10)) AS partial_number 
                        FROM 
                        orders
                    ) a UNION ( 
                        SELECT 0 AS partial_number
                    )) b ORDER BY partial_number DESC LIMIT 1) c"
        )[0]->next_number;
    }

    /**
     * Computes miscellaneous discounts
     *
     * @return float
     */
    public function getDiscountAttribute()
    {
        return $this->original_total - $this->total - $this->coupon_discount_total + $this->shipping_fee;
    }
}
