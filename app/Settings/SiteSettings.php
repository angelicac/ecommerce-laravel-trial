<?php

declare(strict_types=1);

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class SiteSettings extends Settings
{
    public float $default_shipping_fee;

    public static function group(): string
    {
        return 'site';
    }
}
