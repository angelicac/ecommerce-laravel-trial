<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\Models\Cart;
use Illuminate\Database\QueryException;

class AssignCartToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        try {
            $event->user->cart()->save(new Cart());
        } catch (QueryException $e) {
            if ($e->getCode() === '42S02' && config('app.env') === 'testing') {
                // Likely happened during the CreateUserTypesTable migration
                // Attempting to assign a cart when the carts table hasn't been created yet
            } else {
                throw $e;
            }
        }
    }
}
