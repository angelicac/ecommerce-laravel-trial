<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    public function test_listener()
    {
        $user = new User();
        $user->first_name = 'John';
        $user->last_name = 'Smith';
        $user->email = 'john.smith@test.com';
        $user->password = Hash::make('customer');
        $user->user_type = UserType::CUSTOMER;
        $user->save();

        $this->assertNotNull($user->cart);
    }
}
