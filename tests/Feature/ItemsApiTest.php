<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Item;
use App\Models\ItemImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ItemsApiTest extends TestCase
{
    use RefreshDatabase;

    public function test_index()
    {
        $response = $this->get('/api/items');

        $response->assertStatus(200);
    }

    public function test_image()
    {
        Brand::factory()->create();
        Item::factory()->create();
        $item = Item::first();
        ItemImage::factory()->create(['item_id' => $item->id]);
        $response = $this->get('/api/item/image/' . $item->id);
        $response->assertStatus(200);
    }
    public function test_image_on_item_without_images()
    {
        Brand::factory()->create();
        Item::factory()->create();

        $item = Item::first();
        $response = $this->get('/api/item/image/' . $item->id);
        $response->assertStatus(200);
    }

    public function test_subcategory()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $grandparent_category = new Category(['name' => 'Grandparent']);
        $grandparent_category->save();
        $parent_category = new Category(['name' => 'Parent', 'parent_id' => $grandparent_category->id]);
        $parent_category->save();
        $subcategory = new Category(['name' => 'Subcategory', 'parent_id' => $parent_category->id]);
        $subcategory->save();
        $item1 = new Item(['name' => 'Test Item 1', 'description' => 'test', 'brand_id' => $brand->id]);
        $item1->save();
        $item2 = new Item(['name' => 'Test Item 2', 'description' => 'test', 'brand_id' => $brand->id]);
        $item2->save();
        $item3 = new Item(['name' => 'Test Item 3', 'description' => 'test', 'brand_id' => $brand->id]);
        $item3->save();

        $item1->categories()->attach([$parent_category->id]);
        $item2->categories()->attach([$subcategory->id]);
        $item3->categories()->attach([$grandparent_category->id]);


        $response = $this->get("/api/items?category_id={$subcategory->id}");
        $json = $response->json();
        $response->assertStatus(200);
        $this->assertEquals(1, count($json['data']));

        $response = $this->get("/api/items?category_id={$parent_category->id}");
        $json = $response->json();
        $response->assertStatus(200);
        $this->assertEquals(2, count($json['data']));

        $response = $this->get("/api/items?category_id={$grandparent_category->id}");
        $json = $response->json();
        $response->assertStatus(200);
        $this->assertEquals(3, count($json['data']));
    }
}
