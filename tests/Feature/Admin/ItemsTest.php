<?php

namespace Tests\Feature\Admin;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Item;
use App\Models\ItemImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ItemsTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        $this->post('/admin/login', [
            'email' => 'admin@admin.com',
            'password' => 'password'
        ]);
    }

    use RefreshDatabase;


    public function test_store()
    {
        Brand::factory()->count(1)->create();
        Category::factory()->count(3)->create();
        $response = $this->postJson('/api/admin/items', [
            'name' => 'Test Item',
            'description' => 'This is a test item',
            'brand_id' => Brand::query()->first()->id,

            'm2m' => [
                'categories' => [
                    Category::query()->first()->id,
                    Category::query()->offset(1)->first()->id,
                    Category::query()->offset(2)->first()->id,
                ]
            ],
            'one2m' => [
                'images' => [
                    'data' => [],
                    'delete' => [],
                ],
                'variants' => [
                    'data' => []
                ]
            ]
        ]);
        $response->assertStatus(201);
        $item = Item::query()->first();
        $this->assertEquals(3, $item->categories->count());
    }

    public function test_store_categories_syntax()
    {
        Brand::factory()->count(1)->create();
        Category::factory()->count(3)->create();
        $response = $this->postJson('/api/admin/items', [
            'name' => 'Test Item',
            'description' => 'This is a test item',
            'brand_id' => Brand::query()->first()->id,

            'm2m' => [
                'categories' => [
                    ['category_id' => Category::query()->first()->id],
                    ['category_id' => Category::query()->offset(1)->first()->id],
                    ['category_id' => Category::query()->offset(2)->first()->id],
                ]
            ],
            'one2m' => [
                'images' => [
                    'data' => [],
                    'delete' => [],
                ],
                'variants' => [
                    'data' => []
                ],
            ]
        ]);
        $response->assertStatus(201);
        $item = Item::query()->first();
        $this->assertEquals(3, $item->categories->count());
    }

    public function test_upload()
    {
        Storage::fake('local');
        Brand::factory()->count(1)->create();
        Category::factory()->count(3)->create();
        $item = new Item(['name' => 'Test Item', 'brand_id' => Brand::first()->id, 'description' => 'a',]);
        $item->save();
        foreach (Category::all() as $category) {
            $item->categories()->attach($category->id);
        }
        $this->assertEquals(3, $item->categories->count());
        $images = [
            UploadedFile::fake()->image('test1.png'),
            UploadedFile::fake()->image('test2.png'),
            UploadedFile::fake()->image('test3.png'),
        ];
        $response = $this->postJson('/api/admin/items/upload-images/' . $item->id, [
            'images' => $images
        ]);
        $response->assertStatus(200);
        $this->assertEquals(3, ItemImage::query()->count());
        Storage::disk('local')->assertExists("public/img/items/item-{$item->id}-1.png");
        Storage::disk('local')->assertExists("public/img/items/item-{$item->id}-2.png");
        Storage::disk('local')->assertExists("public/img/items/item-{$item->id}-3.png");

        $item->refresh();

        $response = $this->putJson('/api/admin/items/' . $item->id, [
            'name' => $item->name,
            'brand_id' => $item->brand_id,
            'description' => $item->description,
            'one2m' => [
                'images' => [
                    'data' => [],
                    'delete' => [$item->images()->first()->id]
                ],
                'variants' => [
                    'data' => [],
                    'delete' => []
                ]
            ]
        ]);

        $item->refresh();
        $this->assertEquals(2, ItemImage::query()->count());
    }

    public function test_store_variants()
    {
        Brand::factory()->count(1)->create();
        $response = $this->postJson('/api/admin/items', [
            'name' => 'Test Item',
            'description' => 'This is a test item',
            'brand_id' => Brand::query()->first()->id,

            'm2m' => [],
            'one2m' => [
                'variants' => [
                    'data' => [
                        ['sku' => 'sku-1', 'stock' => 10, 'price' => 10, 'meta' => [
                            'size' => 'big',
                            'color' => 'red'
                        ]],
                        ['sku' => 'sku-2', 'stock' => 1, 'price' => 20, 'meta' => [
                            'size' => 'small',
                            'color' => 'red'
                        ]]
                    ],
                    'delete' => [],
                ],
            ]
        ]);
        $response->assertStatus(201);
        $item = Item::query()->first();
        $this->assertEquals(2, $item->variants()->count());
        $response = $this->getJson('/api/admin/items/' . $item->id);
        $response->assertJson([
            'data' => [
                'name' => 'Test Item',
                'description' => 'This is a test item',
                'variants' => [
                    [
                        'sku' => 'sku-1', 'item_id' => $item->id, 'stock' => 10, 'price' => "10.00", 'meta' => [
                            ['name' => 'size', 'value' => 'big'],
                            ['name' => 'color', 'value' => 'red']
                        ],
                    ],
                    [
                        'sku' => 'sku-2', 'stock' => 1, 'price' => "20.00", 'meta' => [
                            ['name' => 'size', 'value' => 'small'],
                            ['name' => 'color', 'value' => 'red']
                        ],
                    ],
                ]
            ]
        ]);
    }
    public function test_update_variants()
    {
        Brand::factory()->create();
        Item::factory()->create();
        $item = Item::first();
        $item->variants()
            ->create(['sku' => 'sku-1', 'price' => 10])
            ->meta()
            ->create(['name' => 'color', 'value' => 'white']);
        $item->variants()
            ->create(['sku' => 'sku-2', 'price' => 10])
            ->meta()
            ->create(['name' => 'color', 'value' => 'black']);

        $response = $this->getJson('/api/admin/items/' . $item->id);
        $response->assertJson([
            'data' => [
                'variants' => [
                    [
                        'sku' => 'sku-1', 'price' => "10.00", 'meta' => [
                            ['name' => 'color', 'value' => 'white']
                        ],
                    ],
                    [
                        'sku' => 'sku-2', 'price' => "10.00", 'meta' => [
                            ['name' => 'color', 'value' => 'black']
                        ],
                    ],
                ]
            ]
        ]);


        $response = $this->putJson('/api/admin/items/' . $item->id, [
            'name' => $item->name,
            'brand_id' => $item->brand_id,
            'description' => $item->description,
            'one2m' => [
                'variants' => [
                    'data' => [
                        ['sku' => 'sku-1', 'price' => 20, 'meta' => [
                            'color' => 'red'
                        ]],
                        ['sku' => 'sku-2', 'price' => 20, 'meta' => [
                            'color' => 'blue'
                        ]]
                    ],
                    'delete' => [],
                ],
            ]
        ]);
        $response->assertStatus(200);

        $this->assertEquals(2, $item->variants()->count());
        $response = $this->getJson('/api/admin/items/' . $item->id);
        $response->assertJson([
            'data' => [
                'variants' => [
                    [
                        'sku' => 'sku-1', 'price' => "20.00", 'meta' => [
                            ['name' => 'color', 'value' => 'red']
                        ],
                    ],
                    [
                        'sku' => 'sku-2', 'price' => "20.00", 'meta' => [
                            ['name' => 'color', 'value' => 'blue']
                        ],
                    ],
                ]
            ]
        ]);
    }

    public function test_store_variants_duplicate_in_payload()
    {
        Brand::factory()->count(1)->create();
        $response = $this->postJson('/api/admin/items', [
            'name' => 'Test Item',
            'description' => 'This is a test item',
            'brand_id' => Brand::query()->first()->id,

            'm2m' => [],
            'one2m' => [
                'variants' => [
                    'data' => [
                        ['sku' => 'sku-1', 'price' => 10, 'meta' => [
                            'size' => 'small'
                        ]],
                        ['sku' => 'sku-1', 'price' => 20, 'meta' => [
                            'size' => 'big'
                        ]]
                    ],
                    'delete' => [],
                ]
            ]
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'variants' => [
                    [],
                    [
                        'Duplicate SKU in table'
                    ]
                ]
            ]
        ]);
    }

    public function test_store_variants_existing_sku()
    {
        Brand::factory()->create();
        Item::factory()->create();
        $item = Item::first();
        $item->variants()->create(['sku' => 'sku-1', 'price' => 10]);

        $response = $this->postJson('/api/admin/items', [
            'name' => 'Test Item',
            'description' => 'This is a test item',
            'brand_id' => Brand::query()->first()->id,

            'm2m' => [],
            'one2m' => [
                'variants' => [
                    'data' => [
                        ['sku' => 'sku-1', 'price' => 10, 'meta' => [
                            'size' => 'small'
                        ]],
                        ['sku' => 'sku-2', 'price' => 20, 'meta' => [
                            'size' => 'big'
                        ]]
                    ],
                    'delete' => [],
                ]
            ]
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'variants' => [
                    [
                        'SKU exists'
                    ],
                ]
            ]
        ]);
    }
    public function test_store_variants_duplicate_in_payload_and_existing_sku()
    {
        Brand::factory()->create();
        Item::factory()->create();
        $item = Item::first();
        $item->variants()->create(['sku' => 'sku-1', 'price' => 10]);

        $response = $this->postJson('/api/admin/items', [
            'name' => 'Test Item',
            'description' => 'This is a test item',
            'brand_id' => Brand::query()->first()->id,

            'm2m' => [],
            'one2m' => [
                'variants' => [
                    'data' => [
                        ['sku' => 'sku-1', 'price' => 10, 'meta' => [
                            'size' => 'small'
                        ]],
                        ['sku' => 'sku-1', 'price' => 20, 'meta' => [
                            'size' => 'big'
                        ]]
                    ],
                    'delete' => [],
                ]
            ]
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'variants' => [
                    [
                        'SKU exists'
                    ],
                    [
                        'SKU exists',
                        'Duplicate SKU in table'
                    ]
                ]
            ]
        ]);
    }
}
