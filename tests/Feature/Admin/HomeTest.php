<?php

namespace Tests\Feature\Admin;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Tests\TestCase;

class HomeTest extends TestCase
{

    use RefreshDatabase;

    public function test_redirect_from_admin_if_not_logged_in()
    {
        $routes = [
            '/admin',
            '/admin/brands'
        ];
        foreach ($routes as $route) {
            $response = $this->get($route);

            $response->assertStatus(302);
        }
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_accessible_after_login()
    {
        $this->post('/admin/login', [
            'email' => 'admin@admin.com',
            'password' => 'password'
        ]);

        $routes = [
            '/admin',
            '/admin/brands',
            '/admin/categories',
            '/admin/overall-promotions',
            '/admin/items',
            '/admin/price-codes',
            '/admin/coupon-promos',
            '/admin/customers',
            '/admin/orders',
        ];
        foreach ($routes as $route) {
            $response = $this->get($route);

            $response->assertStatus(200);
        }
    }

    public function test_inaccessible_non_admin_login()
    {

        $user = new User();
        $user->first_name = 'Customer';
        $user->last_name = 'Customer';
        $user->email = 'customer@test.com';
        $user->password = Hash::make('customer');
        $user->user_type = UserType::CUSTOMER;
        $user->save();

        $this->post('/login', [
            'email' => 'customer@test.com',
            'password' => 'customer'
        ]);

        $routes = [
            '/admin',
            '/admin/brands'
        ];
        foreach ($routes as $route) {
            $response = $this->get($route);

            $response->assertStatus(302);
        }
    }
}
