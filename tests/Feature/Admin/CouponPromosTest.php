<?php

namespace Tests\Feature\Admin;

use App\Models\Brand;
use App\Models\CouponPromo;
use App\Models\CouponPromoItem;
use App\Models\CouponPromoType;
use App\Models\Item;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CouponPromosTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->post('/admin/login', [
            'email' => 'admin@admin.com',
            'password' => 'password'
        ]);
    }

    use RefreshDatabase;

    public function test_index()
    {
        $response = $this->get('/admin/coupon-promos');

        $response->assertStatus(200);
    }

    public function test_store_flat()
    {
        $response = $this->postJson('/api/admin/coupon-promos', [
            'code' => 'CODE',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01'
        ]);

        $response->assertStatus(201);
        $this->assertEquals(CouponPromo::all()->count(), 1);
    }

    public function test_store_flat_items()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100]);
        $test_item->variants()->create(['sku' => 'sku-2', 'price' => 80]);

        $response = $this->postJson('/api/admin/coupon-promos', [
            'code' => 'CODE',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01',
            'm2m' => [
                'items' => [
                    'sku-1' => ['price' => 50],
                    'sku-2' => ['price' => 40],
                ]
            ]
        ]);

        $response->assertStatus(201);
        $this->assertEquals(CouponPromo::all()->count(), 1);
        $this->assertEquals(CouponPromoItem::all()->count(), 2);
    }

    public function test_store_invalid_type()
    {
        $response = $this->postJson('/api/admin/coupon-promos', [
            'code' => 'CODE',
            'type' => 0,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01'
        ]);

        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'type' => [
                    'The selected type is invalid.'
                ]
            ]
        ]);
    }

    public function test_store_reduction()
    {
        $response = $this->postJson('/api/admin/coupon-promos', [
            'code' => 'CODE',
            'type' => CouponPromoType::REDUCTION,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01',
            'rate_threshold' => 1000,
            'rate_reduction' => 500
        ]);

        $response->assertStatus(201);
        $this->assertEquals(CouponPromo::all()->count(), 1);
    }

    public function test_store_missing()
    {
        $response = $this->postJson('/api/admin/coupon-promos', []);
        $response->assertStatus(422);
        $this->assertEquals(CouponPromo::all()->count(), 0);
        $response->assertJson([
            'errors' => [
                'code' => [
                    'The code field is required.'
                ],
                'type' => [
                    'The type field is required.'
                ],
                'start_date' => [
                    'The start date field is required.'
                ],
                'end_date' => [
                    'The end date field is required.'
                ]
            ]
        ]);
    }

    public function test_store_reduction_missing()
    {
        $response = $this->postJson('/api/admin/coupon-promos', [
            'type' => CouponPromoType::REDUCTION
        ]);
        $response->assertStatus(422);
        $this->assertEquals(CouponPromo::all()->count(), 0);
        $response->assertJson([
            'errors' => [
                'code' => [
                    'The code field is required.'
                ],
                'start_date' => [
                    'The start date field is required.'
                ],
                'end_date' => [
                    'The end date field is required.'
                ],
                'rate_threshold' => [
                    'The rate threshold field is required.'
                ]
            ]
        ]);
    }

    public function test_store_unique_fail()
    {
        $promo = new CouponPromo([
            'code' => 'CODE',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01'
        ]);
        $promo->save();
        $response = $this->postJson('/api/admin/coupon-promos/', [
            'code' => 'CODE',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01'
        ]);
        $response->assertStatus(422);
    }

    public function test_update()
    {
        $promo = new CouponPromo([
            'code' => 'CODE',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01'
        ]);
        $promo->save();
        $response = $this->putJson('/api/admin/coupon-promos/' . $promo->id, [
            'code' => 'TEST',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01'
        ]);
        $response->assertStatus(200);
    }

    public function test_unique_fail()
    {
        $code_promo = new CouponPromo([
            'code' => 'CODE',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01'
        ]);
        $code_promo->save();
        $test_promo = new CouponPromo([
            'code' => 'TEST',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01'
        ]);
        $test_promo->save();
        $response = $this->putJson('/api/admin/coupon-promos/' . $code_promo->id, [
            'code' => 'TEST',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01'
        ]);
        $response->assertStatus(422);
    }
}
