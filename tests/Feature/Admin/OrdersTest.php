<?php

namespace Tests\Feature\Admin;

use App\Models\Brand;
use App\Models\Item;
use App\Models\ItemVariant;
use App\Models\Order;
use App\Models\OrderStatus;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class OrdersTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->post('/admin/login', [
            'email' => 'admin@admin.com',
            'password' => 'password'
        ]);
    }

    use RefreshDatabase;

    public function test_store()
    {
        $response = $this->postJson('/api/admin/orders');
        $response->assertStatus(405);
    }

    public function test_update()
    {
        Order::factory()->create();
        $order = Order::first();

        $this->assertEquals(OrderStatus::ORDERED, $order->order_status);

        $response = $this->putJson('/api/admin/orders/' . $order->id, [
            'order_status' => OrderStatus::SHIPPING
        ]);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Updated!']);
        $order->refresh();
        $this->assertEquals(OrderStatus::SHIPPING, $order->order_status);
    }

    public function test_update_invalid_order_status()
    {
        $order_number = '0000000001';

        $order = new Order([
            'number' => $order_number,
            'total' => 0,
            'original_total' => 0,
            'coupon_discount_total' => 0,
            'shipping_fee' => 0,
            'first_name' => 'Test',
            'last_name' => 'Test',
            'contact_number' => '09123456789',
            'email' => 'admin@admin.com',
            'delivery_instructions' => '',
            'street' => 'street',
            'region_id' => '',
            'province_id' => '',
            'city_id' => '',
            'barangay_id' => '',
            'zip_code' => '',
            'customer_id' => 1,
        ]);
        $order->save();
        $order->refresh();

        Order::factory()->create();
        $order = Order::first();

        $this->assertEquals(OrderStatus::ORDERED, $order->order_status);

        $response = $this->putJson('/api/admin/orders/' . $order->id, [
            'order_status' => 0
        ]);
        $response->assertStatus(422);
        $response->assertJson(['errors' => [
            'order_status' => [
                'The selected order status is invalid.'
            ]
        ]]);
        $order->refresh();
        $this->assertEquals(OrderStatus::ORDERED, $order->order_status);
    }

    public function test_cancel()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 10]);
        $test_item->variants()->create(['sku' => 'sku-2', 'price' => 80, 'stock' => 15]);

        Order::factory()->create();
        $order = Order::first();

        $order->items()->attach([
            'sku-1' => ['quantity' => 5, 'original_price' => 100, 'price' => 80],
            'sku-2' => ['quantity' => 5, 'original_price' => 100, 'price' => 80],
        ]);

        $this->assertEquals(OrderStatus::ORDERED, $order->order_status);

        $response = $this->postJson('/api/admin/orders/cancel/' . $order->id);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'Updated!']);
        $order->refresh();
        $this->assertEquals(OrderStatus::CANCELLED, $order->order_status);
        $variant = ItemVariant::find('sku-1');
        $this->assertEquals(15, $variant->stock);

        $variant = ItemVariant::find('sku-2');
        $this->assertEquals(20, $variant->stock);
    }

    public function test_invoice()
    {
        Storage::fake('local');
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 10]);
        $test_item->variants()->create(['sku' => 'sku-2', 'price' => 80, 'stock' => 15]);

        Order::factory()->create();
        $order = Order::first();

        $order->items()->attach([
            'sku-1' => ['quantity' => 5, 'original_price' => 100, 'price' => 80],
            'sku-2' => ['quantity' => 5, 'original_price' => 100, 'price' => 80],
        ]);

        $response = $this->get("/admin/orders/invoice/{$order->id}");
        Storage::disk('local')->assertExists("invoices/invoice-{$order->number}.pdf");
    }
}
