<?php

namespace Tests\Feature\Admin;

use App\Models\Brand;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class BrandsTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        $this->post('/admin/login', [
            'email' => 'admin@admin.com',
            'password' => 'password'
        ]);
    }

    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $response = $this->get('/admin/brands');

        $response->assertStatus(200);
    }

    public function test_store()
    {
        $response = $this->postJson('/api/admin/brands', [
            'name' => 'Test Brand'
        ]);

        $this->assertEquals(Brand::all()->count(), 1);
        $response->assertStatus(201);
    }

    public function test_store_unique_fails()
    {
        $response = $this->postJson('/api/admin/brands', [
            'name' => 'Test Brand'
        ]);
        $response->assertStatus(201);
        $this->assertEquals(Brand::all()->count(), 1);
        $response = $this->postJson('/api/admin/brands', [
            'name' => 'Test Brand'
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'name' => [
                    'The name has already been taken.'
                ]
            ]
        ]);
        $this->assertEquals(Brand::all()->count(), 1);
    }

    public function test_update()
    {
        $response = $this->postJson('/api/admin/brands', [
            'name' => 'Test Brand'
        ]);
        $response->assertStatus(201);
        $this->assertEquals(Brand::all()->count(), 1);
        $id = Brand::query()->first()->id;
        $response = $this->putJson("/api/admin/brands/$id", [
            'name' => 'New Brand'
        ]);
        $response->assertStatus(200);
        $this->assertEquals(Brand::query()->first()->name, 'New Brand');
    }

    public function test_update_fail_on_unique()
    {
        $response = $this->postJson('/api/admin/brands', [
            'name' => 'Test Brand'
        ]);
        $response->assertStatus(201);
        $this->assertEquals(Brand::all()->count(), 1);
        $id = Brand::query()->first()->id;

        $response = $this->postJson('/api/admin/brands', [
            'name' => 'New Brand'
        ]);
        $response->assertStatus(201);

        $this->assertEquals(Brand::all()->count(), 2);

        $response = $this->putJson("/api/admin/brands/$id", [
            'name' => 'New Brand'
        ]);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'name' => [
                    'The name has already been taken.'
                ]
            ]
        ]);
        $response->assertStatus(422);
    }

    public function test_update_fail_on_not_found()
    {
        $response = $this->putJson("/api/admin/brands/100", [
            'name' => 'New Brand'
        ]);
        $response->assertStatus(404);
    }

    public function test_index_search()
    {
        $brand_names = [
            'Test Brand 1',
            'Test Brand 2',
            'Test Brand 3',
        ];
        foreach ($brand_names as $brand_name) {
            $brand = new Brand(['name' => $brand_name]);
            $brand->save();
        }
        $this->assertEquals(Brand::all()->count(), 3);
        $response = $this->getJson('/api/admin/brands?page=0&rows=10&global_search=3', ['X-Requested-With' => 'XMLHttpRequest']);
        $response->assertJson([
            'totalRecords' => 1,
        ]);
        $response = $this->getJson('/api/admin/brands?page=0&rows=10&global_search=Test Brand', ['X-Requested-With' => 'XMLHttpRequest']);
        $response->assertJson([
            'totalRecords' => 3,
        ]);
    }
}
