<?php

namespace Tests\Feature\Admin;

use App\Models\Brand;
use App\Models\Item;
use App\Models\PriceCode;
use App\Models\PriceCodeItem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PriceCodeTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->post('/admin/login', [
            'email' => 'admin@admin.com',
            'password' => 'password'
        ]);
    }

    use RefreshDatabase;

    public function test_index()
    {
        $response = $this->get('/admin/price-codes');

        $response->assertStatus(200);
    }

    public function test_store()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id, 'stock' => 0]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100]);
        $test_item->variants()->create(['sku' => 'sku-2', 'price' => 80]);

        $response = $this->postJson('/api/admin/price-codes', [
            'name' => 'Sample Price Code',
            'm2m' => [
                'items' => [
                    'sku-1' => ['price' => 50],
                    'sku-2' => ['price' => 40],
                ]
            ]
        ]);

        $response->assertStatus(201);
        $this->assertEquals(PriceCode::all()->count(), 1);
        $this->assertEquals(PriceCodeItem::all()->count(), 2);
    }

    public function test_store_missing()
    {
        $response = $this->postJson('/api/admin/price-codes', []);
        $response->assertStatus(422);
        $this->assertEquals(PriceCode::all()->count(), 0);
        $response->assertJson([
            'errors' => [
                'name' => [
                    'The name field is required.'
                ],
            ]
        ]);
    }


    public function test_store_unique_fails()
    {
        $response = $this->postJson('/api/admin/price-codes', [
            'name' => 'Test Price Code'
        ]);
        $response->assertStatus(201);
        $this->assertEquals(PriceCode::all()->count(), 1);
        $response = $this->postJson('/api/admin/price-codes', [
            'name' => 'Test Price Code'
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'name' => [
                    'The name has already been taken.'
                ]
            ]
        ]);
        $this->assertEquals(PriceCode::all()->count(), 1);
    }

    public function test_update()
    {
        $response = $this->postJson('/api/admin/price-codes', [
            'name' => 'Test Price Code'
        ]);
        $response->assertStatus(201);
        $this->assertEquals(PriceCode::all()->count(), 1);
        $id = PriceCode::query()->first()->id;
        $response = $this->putJson("/api/admin/price-codes/$id", [
            'name' => 'New Price Code'
        ]);
        $response->assertStatus(200);
        $this->assertEquals(PriceCode::query()->first()->name, 'New Price Code');
    }

    public function test_update_fail_on_unique()
    {
        $response = $this->postJson('/api/admin/price-codes', [
            'name' => 'Test Price Code'
        ]);
        $response->assertStatus(201);
        $this->assertEquals(PriceCode::all()->count(), 1);
        $id = PriceCode::query()->first()->id;

        $response = $this->postJson('/api/admin/price-codes', [
            'name' => 'New Price Code'
        ]);
        $response->assertStatus(201);

        $this->assertEquals(PriceCode::all()->count(), 2);

        $response = $this->putJson("/api/admin/price-codes/$id", [
            'name' => 'New Price Code'
        ]);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'name' => [
                    'The name has already been taken.'
                ]
            ]
        ]);
        $response->assertStatus(422);
    }
}
