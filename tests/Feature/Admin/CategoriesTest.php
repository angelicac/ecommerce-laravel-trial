<?php

namespace Tests\Feature\Admin;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class CategoriesTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        $this->post('/admin/login', [
            'email' => 'admin@admin.com',
            'password' => 'password'
        ]);
    }

    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $response = $this->get('/admin/categories');

        $response->assertStatus(200);
    }

    public function test_store()
    {
        $response = $this->postJson('/api/admin/categories', [
            'name' => 'Test Category'
        ]);

        $this->assertEquals(Category::all()->count(), 1);
        $response->assertStatus(201);
    }

    public function test_store_unique_fails()
    {
        $response = $this->postJson('/api/admin/categories', [
            'name' => 'Test Category'
        ]);
        $response->assertStatus(201);
        $this->assertEquals(Category::all()->count(), 1);
        $response = $this->postJson('/api/admin/categories', [
            'name' => 'Test Category'
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'name' => [
                    'The name has already been taken.'
                ]
            ]
        ]);
        $this->assertEquals(Category::all()->count(), 1);
    }

    public function test_update()
    {
        $response = $this->postJson('/api/admin/categories', [
            'name' => 'Test Category'
        ]);
        $response->assertStatus(201);
        $this->assertEquals(Category::all()->count(), 1);
        $id = Category::query()->first()->id;
        $response = $this->putJson("/api/admin/categories/$id", [
            'name' => 'New Category'
        ]);
        $response->assertStatus(200);
        $this->assertEquals(Category::query()->first()->name, 'New Category');
    }

    public function test_update_fail_on_unique()
    {
        $response = $this->post('/api/admin/categories', [
            'name' => 'Test Category'
        ]);
        $response->assertStatus(201);
        $this->assertEquals(Category::all()->count(), 1);
        $id = Category::query()->first()->id;

        $response = $this->post('/api/admin/categories', [
            'name' => 'New Category'
        ]);
        $response->assertStatus(201);

        $this->assertEquals(Category::all()->count(), 2);

        $response = $this->putJson("/api/admin/categories/$id", [
            'name' => 'New Category'
        ]);
        $response->assertJson([
            'message' => 'The given data was invalid.',
            'errors' => [
                'name' => [
                    'The name has already been taken.'
                ]
            ]
        ]);
        $response->assertStatus(422);
    }

    public function test_update_fail_on_not_found()
    {
        $response = $this->putJson("/api/admin/categories/100", [
            'name' => 'New Category'
        ]);
        $response->assertStatus(404);
    }

    public function test_index_search()
    {
        $category_names = [
            'Test Category 1',
            'Test Category 2',
            'Test Category 3',
        ];
        foreach ($category_names as $category_name) {
            $category = new Category(['name' => $category_name]);
            $category->save();
        }
        $this->assertEquals(Category::all()->count(), 3);
        $response = $this->getJson('/api/admin/categories?page=0&rows=10&global_search=3', ['X-Requested-With' => 'XMLHttpRequest']);
        $response->assertJson([
            'totalRecords' => 1,
        ]);
        $response = $this->getJson('/api/admin/categories?page=0&rows=10&global_search=Test Category', ['X-Requested-With' => 'XMLHttpRequest']);
        $response->assertJson([
            'totalRecords' => 3,
        ]);
    }

    public function test_parent_category_no_infinite_loop()
    {
        $parent = new Category(['name' => 'Parent']);
        $parent->save();
        $child = new Category(['name' => 'Child', 'parent_id' => $parent->id]);
        $child->save();

        $response = $this->putJson("/api/admin/categories/{$parent->id}", [
            'name' => $parent->name,
            'parent_id' => $child->id
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'parent_id' => [
                    'Invalid parent category.'
                ]
            ]
        ]);
    }
    public function test_parent_category_no_self_parent()
    {
        $parent = new Category(['name' => 'Parent']);
        $parent->save();

        $response = $this->putJson("/api/admin/categories/{$parent->id}", [
            'name' => $parent->name,
            'parent_id' => $parent->id
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'parent_id' => [
                    'Invalid parent category.'
                ]
            ]
        ]);
    }
}
