<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\Item;
use App\Models\OverallPromoItem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class OverallPromotionsTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->post('/admin/login', [
            'email' => 'admin@admin.com',
            'password' => 'password'
        ]);
    }

    use RefreshDatabase;

    public function test_index()
    {
        $response = $this->get('/admin/overall-promotions');
        $response->assertStatus(200);
    }

    public function test_store()
    {
        Brand::factory()->create();
        $item = Item::factory()->create();
        $item->variants()->create(['sku' => 'sku-1', 'price' => 20]);
        $response = $this->postJson('/api/admin/overall-promotions', [
            'sku' => 'sku-1',
            'price' => 10,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01'
        ]);
        $response->assertStatus(201);
        $response->assertJson([
            'id' => 1
        ]);

        $response = $this->getJson('/api/admin/overall-promotions/1');

        $response->assertJson([
            'data' => [
                'id' => 1,
                'price' => 10,
                'start_date' => '2022-01-01',
                'end_date' => '2022-02-01',
                'sku' => 'sku-1',
                'item' => [
                    'sku' => 'sku-1'
                ]
            ]
        ]);
    }

    public function test_update()
    {
        Brand::factory()->create();
        $item = Item::factory()->create();
        $item->variants()->create(['sku' => 'sku-1', 'price' => 20]);
        $promo_item = new OverallPromoItem(['sku' => 'sku-1', 'price' => 10, 'start_date' => '2022-01-01', 'end_date' => '2022-02-01']);
        $promo_item->save();

        $response = $this->putJson('/api/admin/overall-promotions/' . $promo_item->id, [
            'sku' => 'sku-1',
            'price' => 15,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01'
        ]);
        $response->assertStatus(200);

        $response = $this->getJson('/api/admin/overall-promotions/' . $promo_item->id);

        $response->assertJson([
            'data' => [
                'id' => $promo_item->id,
                'price' => 15,
                'start_date' => '2022-01-01',
                'end_date' => '2022-02-01',
                'sku' => 'sku-1',
                'item' => [
                    'sku' => 'sku-1'
                ]
            ]
        ]);
    }

    public function test_store_sku_nonexistant()
    {
        $response = $this->postJson('/api/admin/overall-promotions', [
            'sku' => 'sku-1',
            'price' => 10,
            'start_date' => '2022-01-01',
            'end_date' => '2022-02-01'
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'sku' => [
                    'The selected sku is invalid.'
                ]
            ]
        ]);
    }

    public function test_store_dates_invalid()
    {
        Brand::factory()->create();
        $item = Item::factory()->create();
        $item->variants()->create(['sku' => 'sku-1', 'price' => 20]);
        $response = $this->postJson('/api/admin/overall-promotions', [
            'sku' => 'sku-1',
            'price' => 10,
            'start_date' => '2022-02-01',
            'end_date' => '2022-01-01'
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'start_date' => [
                    'The start date must be a date before or equal to end date.'
                ],
                'end_date' => [
                    'The end date must be a date after or equal to start date.'
                ],
            ]
        ]);
    }

    public function test_update_dates_invalid()
    {
        Brand::factory()->create();
        $item = Item::factory()->create();
        $item->variants()->create(['sku' => 'sku-1', 'price' => 20]);
        $promo_item = new OverallPromoItem(['sku' => 'sku-1', 'price' => 10, 'start_date' => '2022-01-01', 'end_date' => '2022-02-01']);
        $promo_item->save();

        $response = $this->putJson('/api/admin/overall-promotions/' . $promo_item->id, [
            'sku' => 'sku-1',
            'price' => 10,
            'start_date' => '2022-02-01',
            'end_date' => '2022-01-01'
        ]);
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'start_date' => [
                    'The start date must be a date before or equal to end date.'
                ],
                'end_date' => [
                    'The end date must be a date after or equal to start date.'
                ],
            ]
        ]);
    }

    public function test_store_fields_missing()
    {
        Brand::factory()->create();
        $item = Item::factory()->create();
        $item->variants()->create(['sku' => 'sku-1', 'price' => 20]);
        $response = $this->postJson('/api/admin/overall-promotions');
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'sku' => [
                    'The sku field is required.',
                ],
                'price' => [
                    'The price field is required.'
                ],
                'start_date' => [
                    'The start date field is required.'
                ],
                'end_date' => [
                    'The end date field is required.'
                ],
            ]
        ]);
    }

    public function test_update_fields_missing()
    {
        Brand::factory()->create();
        $item = Item::factory()->create();
        $item->variants()->create(['sku' => 'sku-1', 'price' => 20]);
        $promo_item = new OverallPromoItem(['sku' => 'sku-1', 'price' => 10, 'start_date' => '2022-01-01', 'end_date' => '2022-02-01']);
        $promo_item->save();

        $response = $this->putJson('/api/admin/overall-promotions/' . $promo_item->id);
        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'sku' => [
                    'The sku field is required.',
                ],
                'price' => [
                    'The price field is required.'
                ],
                'start_date' => [
                    'The start date field is required.'
                ],
                'end_date' => [
                    'The end date field is required.'
                ],
            ]
        ]);
    }
}
