<?php

namespace Tests\Feature\Cart;

use App\Models\Brand;
use App\Models\Cart;
use App\Models\CouponPromo;
use App\Models\CouponPromoType;
use App\Models\Item;
use App\Models\ItemVariant;
use App\Models\Order;
use App\Models\OverallPromoItem;
use App\Models\PriceCode;
use App\Models\PriceCodeItem;
use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class GuestTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        Config::set('app.shop.allow_guests', true);
    }

    use RefreshDatabase;

    public function test_add()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $response = $this->post('/api/cart/add/sku-1/2');
        $response->assertStatus(200);
    }
}
