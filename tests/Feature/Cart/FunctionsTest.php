<?php

namespace Tests\Feature\Cart;

use App\Models\Brand;
use App\Models\Cart;
use App\Models\CouponPromo;
use App\Models\CouponPromoType;
use App\Models\Item;
use App\Models\ItemVariant;
use App\Models\Order;
use App\Models\OverallPromoItem;
use App\Models\PriceCode;
use App\Models\PriceCodeItem;
use App\Models\ShippingFee;
use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class FunctionsTest extends TestCase
{
    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $user = new User();
        $user->first_name = 'Customer';
        $user->last_name = 'Customer';
        $user->email = 'customer@test.com';
        $user->password = Hash::make('customer');
        $user->user_type = UserType::CUSTOMER;
        $user->save();

        $this->user = $user;

        $this->post('/login', [
            'email' => $user->email,
            'password' => 'customer'
        ]);
    }

    use RefreshDatabase;

    public function test_index()
    {
        $response = $this->get('/api/cart');
        $response->assertJson([
            'data' => [],
            'total' => 0,
            'discount_total' => 0,
            'coupon' => [],
            'original_total' => 0,
            'total_quantity' => 0,
            'shipping_fee' => 0
        ]);
    }

    public function test_index_with_items()
    {
        $response = $this->get('/api/cart');

        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();

        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 5]);

        $cart = Cart::query()->firstOrCreate(['user_id' => $this->user->id]);
        $cart->contents()->attach(['sku-1' => ['quantity' => 20]]);

        $response = $this->get('/api/cart');
        $response->assertJson([
            'data' => [
                [
                    'sku' => 'sku-1',
                    'stock' => 5,
                    'pivot' => [
                        'cart_id' => $cart->id,
                        'sku' => 'sku-1',
                        'quantity' => 5,
                        'notes' => 'Quantity has been reduced from 20 to 5 due to low stock.'
                    ]
                ]
            ],
            'total' => 500,
            'discount_total' => 0,
            'coupon' => [],
            'original_total' => 500,
            'total_quantity' => 5,
            'shipping_fee' => 0
        ]);
    }

    public function test_dismiss_notes()
    {

        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();

        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 5]);

        $cart = Cart::query()->firstOrCreate(['user_id' => $this->user->id]);
        $cart->contents()->attach(['sku-1' => ['quantity' => 20]]);

        $this->get('/api/cart');

        $cart->refresh();
        $cart_item = $cart->contents[0];


        $this->assertEquals("Quantity has been reduced from 20 to 5 due to low stock.", $cart_item->pivot->notes);

        $response = $this->postJson('/api/cart/dismiss-notes/sku-1');

        $response->assertJson([
            'message' => 'Note dismissed'
        ]);

        $response->assertStatus(200);
        $cart->refresh();
        $cart_item = $cart->contents[0];
        $this->assertEquals(null, $cart_item->pivot->notes);
    }

    public function test_coupon()
    {
        $promo = new CouponPromo([
            'code' => 'CODE',
            'type' => CouponPromoType::REDUCTION,
            'start_date' => '2022-01-01',
            'end_date' => '2023-01-01',
            'rate_threshold' => '1000',
            'rate_reduction' => '500'
        ]);
        $promo->save();
        $response = $this->postJson('/api/cart/set-coupon', [
            'coupon_code' => 'CODE'
        ]);
        $response->assertSessionHas('coupon', 'CODE');
    }

    public function test_reduction_coupon()
    {
        $promo = new CouponPromo([
            'code' => 'CODE',
            'type' => CouponPromoType::REDUCTION,
            'start_date' => '2022-01-01',
            'end_date' => '2023-01-01',
            'rate_threshold' => '1000',
            'rate_reduction' => '500'
        ]);
        $promo->save();
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $cart = Cart::query()->firstOrCreate(['user_id' => $this->user->id]);
        $cart->contents()->attach(['sku-1' => ['quantity' => 20]]);
        $response = $this->withSession(['coupon' => 'CODE'])->get('/api/cart');
        $response->assertJson([
            'original_total' => 2000,
            'total' => 1500,
            'discount_total' => 500
        ]);
    }

    public function test_flat_coupon()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $promo = new CouponPromo([
            'code' => 'CODE',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2023-01-01',
        ]);
        $promo->save();
        $promo->items()->attach(['sku-1' => ['price' => 50]]);
        $cart = Cart::query()->firstOrCreate(['user_id' => $this->user->id]);
        $cart->contents()->attach(['sku-1' => ['quantity' => 20]]);
        $response = $this->withSession(['coupon' => 'CODE'])->get('/api/cart');
        $response->assertJson([
            'original_total' => 2000,
            'total' => 1000,
            'discount_total' => 1000
        ]);
    }

    public function test_flat_coupon_multiple_eligible_items()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $test_item->variants()->create(['sku' => 'sku-2', 'price' => 80, 'stock' => 100]);
        $promo = new CouponPromo([
            'code' => 'CODE',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2023-01-01',
        ]);
        $promo->save();
        $promo->items()->attach(['sku-1' => ['price' => 50], 'sku-2' => ['price' => 40]]);
        $cart = Cart::query()->firstOrCreate(['user_id' => $this->user->id]);
        $cart->contents()->attach(
            [
                'sku-1' => ['quantity' => 10],
                'sku-2' => ['quantity' => 5]
            ]
        );
        $response = $this->withSession(['coupon' => 'CODE'])->get('/api/cart');
        $response->assertJson([
            'original_total' => 1400,
            'total' => 700,
            'discount_total' => 700
        ]);
    }

    public function test_overall_promo_item()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $promo = new OverallPromoItem([
            'sku' => 'sku-1',
            'price' => 60,
            'start_date' => '2022-01-01',
            'end_date' => '2023-01-01',
        ]);
        $promo->save();
        $cart = Cart::query()->firstOrCreate(['user_id' => $this->user->id]);
        $cart->contents()->attach(
            [
                'sku-1' => ['quantity' => 10],
            ]
        );
        $response = $this->withSession(['coupon' => 'CODE'])->get('/api/cart');
        $response->assertJson([
            'original_total' => 1000,
            'total' => 600,
            'discount_total' => 0
        ]);
    }

    public function test_overall_promo_item_and_coupon_promo()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $test_item->variants()->create(['sku' => 'sku-2', 'price' => 80, 'stock' => 100]);
        $promo = new CouponPromo([
            'code' => 'CODE',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2023-01-01',
        ]);
        $promo->save();
        $promo->items()->attach(['sku-2' => ['price' => 40]]);

        (new OverallPromoItem([
            'sku' => 'sku-1',
            'price' => 60,
            'start_date' => '2022-01-01',
            'end_date' => '2023-01-01',
        ]))->save();
        $cart = Cart::query()->firstOrCreate(['user_id' => $this->user->id]);
        $cart->contents()->attach(
            [
                'sku-1' => ['quantity' => 10],
                'sku-2' => ['quantity' => 20],
            ]
        );
        $response = $this->withSession(['coupon' => 'CODE'])->get('/api/cart');
        $response->assertJson([
            'original_total' => 2600,
            'total' => 1400,
            'discount_total' => 800
        ]);
    }

    public function test_price_code()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $price_code = new PriceCode([
            'name' => 'Price Code',
        ]);
        $price_code->save();
        $price_code->items()->attach(['sku-1' => ['price' => 50]]);
        $this->user->price_code_id = $price_code->id;
        $this->user->save();
        $cart = Cart::query()->firstOrCreate(['user_id' => $this->user->id]);
        $cart->contents()->attach(['sku-1' => ['quantity' => 20]]);
        $response = $this->actingAs($this->user)->get('/api/cart');
        $response->assertJson([
            'original_total' => 2000,
            'total' => 1000,
            'discount_total' => 0
        ]);
    }

    public function test_add()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $response = $this->post('/api/cart/add/sku-1/2');
        $response->assertStatus(200);
        $this->assertNotNull($cart = Cart::whereUserId($this->user->id)->first());
        $this->assertEquals(1, $cart->contents()->count());
        $this->assertEquals(1, $cart->contents()->count());
    }

    public function test_checkout()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 10]);
        $test_item->variants()->create(['sku' => 'sku-2', 'price' => 80, 'stock' => 15]);
        $this->user->cart->contents()->attach(['sku-1' => ['quantity' => 2]]);
        $this->user->cart->contents()->attach(['sku-2' => ['quantity' => 4]]);
        $this->user->addresses()->create([
            'name' => 'Home Address',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1'
        ]);

        $response = $this->postJson('/api/cart/checkout', [
            'first_name' => 'John',
            'last_name' => 'Smith',
            'email' => 'john.smith@example.com',
            'contact_number' => '09123456789',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1',
        ]);

        $number = $response->json('number');
        $order = Order::query()->whereNumber($number)->first();

        $response->assertStatus(201);

        $this->assertEquals(1, Order::count());

        $variant = ItemVariant::whereSku('sku-1')->first();
        $this->assertEquals(8, $variant->stock);

        $variant = ItemVariant::whereSku('sku-2')->first();
        $this->assertEquals(11, $variant->stock);

        $this->assertEquals(570, $order->total);
        $this->assertEquals(50, $order->shipping_fee);
    }

    public function test_checkout_with_set_shipping_fee()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 10]);
        $test_item->variants()->create(['sku' => 'sku-2', 'price' => 80, 'stock' => 15]);

        $fee = new ShippingFee(['name' => 'Ilocos', 'region_id' => '01', 'shipping_fee' => 100]);
        $fee->save();
        $this->user->cart->contents()->attach(['sku-1' => ['quantity' => 2]]);
        $this->user->cart->contents()->attach(['sku-2' => ['quantity' => 4]]);
        $this->user->addresses()->create([
            'name' => 'Home Address',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1'
        ]);

        $response = $this->postJson('/api/cart/checkout', [
            'first_name' => 'John',
            'last_name' => 'Smith',
            'email' => 'john.smith@example.com',
            'contact_number' => '09123456789',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1',
        ]);

        $number = $response->json('number');
        $order = Order::query()->whereNumber($number)->first();

        $response->assertStatus(201);

        $this->assertEquals(1, Order::count());

        $variant = ItemVariant::whereSku('sku-1')->first();
        $this->assertEquals(8, $variant->stock);

        $variant = ItemVariant::whereSku('sku-2')->first();
        $this->assertEquals(11, $variant->stock);

        $this->assertEquals(620, $order->total);
        $this->assertEquals(100, $order->shipping_fee);
    }

    public function test_checkout_with_set_shipping_fee_province()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 10]);
        $test_item->variants()->create(['sku' => 'sku-2', 'price' => 80, 'stock' => 15]);

        $fee = new ShippingFee(['name' => 'Ilocos', 'region_id' => '01', 'shipping_fee' => 100]);
        $fee->save();

        $fee = new ShippingFee(['name' => 'Ilocos Sur', 'region_id' => '01', 'province_id' => '0129', 'shipping_fee' => 120]);
        $fee->save();
        $this->user->cart->contents()->attach(['sku-1' => ['quantity' => 2]]);
        $this->user->cart->contents()->attach(['sku-2' => ['quantity' => 4]]);
        $this->user->addresses()->create([
            'name' => 'Home Address',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1'
        ]);

        $response = $this->postJson('/api/cart/checkout', [
            'first_name' => 'John',
            'last_name' => 'Smith',
            'email' => 'john.smith@example.com',
            'contact_number' => '09123456789',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1',
        ]);

        $number = $response->json('number');
        $order = Order::query()->whereNumber($number)->first();

        $response->assertStatus(201);

        $this->assertEquals(1, Order::count());

        $variant = ItemVariant::whereSku('sku-1')->first();
        $this->assertEquals(8, $variant->stock);

        $variant = ItemVariant::whereSku('sku-2')->first();
        $this->assertEquals(11, $variant->stock);

        $this->assertEquals(640, $order->total);
        $this->assertEquals(120, $order->shipping_fee);
    }

    public function test_checkout_with_set_shipping_fee_city()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 10]);
        $test_item->variants()->create(['sku' => 'sku-2', 'price' => 80, 'stock' => 15]);

        $fee = new ShippingFee(['name' => 'Ilocos', 'region_id' => '01', 'shipping_fee' => 100]);
        $fee->save();

        $fee = new ShippingFee(['name' => 'Ilocos Sur', 'region_id' => '01', 'province_id' => '0129', 'shipping_fee' => 120]);
        $fee->save();

        $fee = new ShippingFee(['name' => 'Alilem', 'region_id' => '01', 'province_id' => '0129', 'city_id' => '012901', 'shipping_fee' => 150]);
        $fee->save();

        $this->user->cart->contents()->attach(['sku-1' => ['quantity' => 2]]);
        $this->user->cart->contents()->attach(['sku-2' => ['quantity' => 4]]);
        $this->user->addresses()->create([
            'name' => 'Home Address',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1'
        ]);

        $response = $this->postJson('/api/cart/checkout', [
            'first_name' => 'John',
            'last_name' => 'Smith',
            'email' => 'john.smith@example.com',
            'contact_number' => '09123456789',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1',
        ]);

        $number = $response->json('number');
        $order = Order::query()->whereNumber($number)->first();

        $response->assertStatus(201);

        $this->assertEquals(1, Order::count());

        $variant = ItemVariant::whereSku('sku-1')->first();
        $this->assertEquals(8, $variant->stock);

        $variant = ItemVariant::whereSku('sku-2')->first();
        $this->assertEquals(11, $variant->stock);

        $this->assertEquals(670, $order->total);
        $this->assertEquals(150, $order->shipping_fee);
    }

    public function test_checkout_with_flat_coupon()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 10]);
        $test_item->variants()->create(['sku' => 'sku-2', 'price' => 80, 'stock' => 15]);

        $promo = new CouponPromo([
            'code' => 'CODE',
            'type' => CouponPromoType::FLAT,
            'start_date' => '2022-01-01',
            'end_date' => '2023-01-01',
        ]);
        $promo->save();
        $promo->items()->attach(['sku-1' => ['price' => 50]]);

        $this->user->cart->contents()->attach(['sku-1' => ['quantity' => 2]]);
        $this->user->cart->contents()->attach(['sku-2' => ['quantity' => 4]]);
        $this->user->addresses()->create([
            'name' => 'Home Address',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1'
        ]);

        $response = $this->withSession(['coupon' => 'CODE'])->postJson('/api/cart/checkout', [
            'first_name' => 'John',
            'last_name' => 'Smith',
            'email' => 'john.smith@example.com',
            'contact_number' => '09123456789',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1',
        ]);

        $response->assertStatus(201);

        $this->assertEquals(1, Order::count());

        $order = Order::query()->first();

        $this->assertEquals(100, $order->items->first()->pivot->original_price);
        $this->assertEquals(50, $order->items->first()->pivot->price);
        $this->assertEquals(80, $order->items->skip(1)->first()->pivot->price);
        $this->assertEquals(80, $order->items->skip(1)->first()->pivot->original_price);
    }

    public function test_checkout_with_out_of_stock_item()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 10]);
        $test_item->variants()->create(['sku' => 'sku-2', 'price' => 80, 'stock' => 0]);
        $this->user->cart->contents()->attach(['sku-1' => ['quantity' => 2]]);
        $this->user->cart->contents()->attach(['sku-2' => ['quantity' => 4]]);
        $this->user->addresses()->create([
            'name' => 'Home Address',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1'
        ]);

        $response = $this->postJson('/api/cart/checkout', [
            'first_name' => 'John',
            'last_name' => 'Smith',
            'email' => 'john.smith@example.com',
            'contact_number' => '09123456789',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1',
        ]);

        $response->assertStatus(201);

        $this->assertEquals(1, Order::count());

        $order = Order::first();

        $this->assertEquals(1, $order->items->count());
    }
}
