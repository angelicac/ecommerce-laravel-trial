<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\Item;
use App\Models\OverallPromoItem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class OverallPromoItemTest extends TestCase
{

    use RefreshDatabase;
    public function test_get_item_active_promo()
    {
        Brand::factory()->create();
        $item = Item::factory()->create();
        $item->variants()->create(['sku' => 'sku-1', 'price' => 20]);
        $today = new \DateTime();
        $today->setTime(0, 0, 0, 0);

        $start_date = clone $today;
        $start_date->sub(new \DateInterval("P7D"));

        $promo = OverallPromoItem::insert([
            'start_date' => $start_date,
            'end_date' => $today,
            'sku' => 'sku-1',
            'price' => 10
        ]);

        $this->assertEquals(1, OverallPromoItem::count());
        $this->assertNotNull(OverallPromoItem::get_item_active_promo('sku-1'));
    }
}
