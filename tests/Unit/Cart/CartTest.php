<?php

namespace Tests\Unit\Cart;

use App\Models\Brand;
use Tests\TestCase;
use App\Models\User;
use App\Models\Item;
use App\Models\ItemVariant;
use App\Models\ItemVariantMeta;
use App\Models\Cart;
use App\Models\Category;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartTest extends TestCase
{
    use RefreshDatabase;
    protected $test_user;
    protected function setUp(): void
    {
        parent::setUp();
        $this->test_user = User::factory(1)->create();
        Brand::factory()->count(10)->create();
        $category_factory = Category::factory();
        for ($i = 0; $i < 10; $i++) {
            try {
                $category_factory->create();
            } catch (Exception $e) {
            }
        }

        $item = Item::factory()->create();
        $item->variants()->createMany([
            ['sku' => 'sku-1', 'price' => 10],
            ['sku' => 'sku-2', 'price' => 20]
        ]);
    }

    public function test_cart()
    {
        $user = User::first();
        $cart = Cart::firstOrCreate(['user_id' => $user->id]);
        $this->assertEquals(0, count($cart->contents));
        $cart->add('sku-1', 10);
        $cart->refresh();
        $this->assertEquals(1, count($cart->contents));
        $this->assertEquals('sku-1', $cart->contents[0]->sku);
        $this->assertEquals(10, $cart->contents[0]->pivot->quantity);

        $cart->add('sku-1', 5);
        $cart->refresh();
        $this->assertEquals(1, count($cart->contents));
        $this->assertEquals(15, $cart->contents[0]->pivot->quantity);

        $cart->remove('sku-1', 7);
        $cart->refresh();
        $this->assertEquals(1, count($cart->contents));
        $this->assertEquals(8, $cart->contents[0]->pivot->quantity);

        $cart->remove('sku-1', 10);
        $cart->refresh();
        $this->assertEquals(0, count($cart->contents));

        $cart->add('sku-2', 20);
        $cart->refresh();
        $this->assertEquals(1, count($cart->contents));
        $this->assertEquals(20, $cart->contents[0]->pivot->quantity);

        $cart->remove_all('sku-2');
        $cart->refresh();
        $this->assertEquals(0, count($cart->contents));

        $cart->add('sku-1', 10);
        $cart->add('sku-2', 20);
        $cart->refresh();
        $this->assertEquals(10, $cart->quantity('sku-1'));
        $this->assertEquals(20, $cart->quantity('sku-2'));
        $this->assertEquals(30, $cart->quantity());
    }
}
