<?php

namespace Tests\Unit;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoriesTest extends TestCase
{
    use RefreshDatabase;
    public function test_all_subcategories_one_level()
    {
        $parent = new Category(['name' => 'Parent']);
        $parent->save();
        $child1 = new Category(['name' => 'Child 1', 'parent_id' => $parent->id]);
        $child1->save();
        $child2 = new Category(['name' => 'Child 2', 'parent_id' => $parent->id]);
        $child2->save();

        $parent->refresh();

        $this->assertEquals(2, $parent->all_subcategories()->count());
        $this->assertEquals('Child 1', $parent->all_subcategories()->first()->name);
        $this->assertEquals('Child 2', $parent->all_subcategories()->skip(1)->first()->name);
    }

    public function test_all_subcategories_two_levels()
    {
        $grandparent = new Category(['name' => 'Grandparent']);
        $grandparent->save();

        $parent1 = new Category(['name' => 'Parent 1', 'parent_id' => $grandparent->id]);
        $parent1->save();
        $parent2 = new Category(['name' => 'Parent 2', 'parent_id' => $grandparent->id]);
        $parent2->save();

        $child1 = new Category(['name' => 'Child 1', 'parent_id' => $parent1->id]);
        $child1->save();
        $child2 = new Category(['name' => 'Child 2', 'parent_id' => $parent1->id]);
        $child2->save();

        $child3 = new Category(['name' => 'Child 3', 'parent_id' => $parent2->id]);
        $child3->save();
        $child4 = new Category(['name' => 'Child 4', 'parent_id' => $parent2->id]);
        $child4->save();

        $grandparent->refresh();

        $this->assertEquals(6, $grandparent->all_subcategories()->count());
        $this->assertEquals('Parent 1', $grandparent->all_subcategories()->first()->name);
        $this->assertEquals('Parent 2', $grandparent->all_subcategories()->skip(1)->first()->name);
    }

    public function test_get_path_to_root()
    {
        $grandparent = new Category(['name' => 'Grandparent']);
        $grandparent->save();

        $parent = new Category(['name' => 'Parent 1', 'parent_id' => $grandparent->id]);
        $parent->save();

        $child = new Category(['name' => 'Child 1', 'parent_id' => $parent->id]);
        $child->save();

        $this->assertEquals([$child->id, $parent->id, $grandparent->id], $child->get_path_to_root());
    }
}
