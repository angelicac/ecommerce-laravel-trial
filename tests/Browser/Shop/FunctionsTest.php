<?php

namespace Tests\Browser\Shop;

use App\Models\Brand;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Item;
use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class FunctionsTest extends DuskTestCase
{
    use DatabaseMigrations;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $user = new User();
        $user->first_name = 'Customer';
        $user->last_name = 'Customer';
        $user->email = 'customer@test.com';
        $user->password = Hash::make('customer');
        $user->user_type = UserType::CUSTOMER;
        $user->save();

        $this->user = $user;
    }

    public function test_index()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'weight' => 1]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/shop')
                ->assertScript('typeof window.vm', 'object')
                ->waitUntil('window.vm.loading === false')
                ->waitForText('Test Item')
                ->assertSee('Test Item');
        });
    }

    public function test_link()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'weight' => 1]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/shop')
                ->assertScript('typeof window.vm', 'object')
                ->waitUntil('window.vm.loading === false')
                ->waitForText('Test Item')
                ->clickLink('Test Item')
                ->assertPathIs('/shop/item/1');
        });
    }

    public function test_add_to_cart()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);

        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user->id)
                ->visit('/shop/item/1')
                ->assertScript('typeof window.vm', 'object')
                ->waitUntil('typeof window.vm.entity.name === "string"')
                ->pressAndWaitFor('Add To Cart')
                ->waitForText('Added to Cart!')
                ->assertSee('Added to Cart!');
            $this->assertEquals(1, Cart::find(1)->contents()->count());
        });
    }

    public function test_add_to_cart_not_logged_in()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);

        $this->browse(function (Browser $browser) {
            $browser
                ->visit('/shop/item/1')
                ->assertScript('typeof window.vm', 'object')
                ->waitUntil('typeof window.vm.entity.name === "string"')
                ->pressAndWaitFor('Add To Cart')
                ->waitForText('You need to be logged in first')
                ->assertSee('You need to be logged in first');
        });
    }

    /**
     * Tests if selecting an item in the cateogy correctly sets search parameters
     *
     * @return void
     */
    public function test_shop_tree_category_select()
    {
        Category::query()->delete();
        Category::factory(5)->create();
        $this->browse(function (Browser $browser) {
            $category = Category::query()->orderBy('name')->first();
            $browser->visit('/shop')
                ->waitUntil('window.vm.categories.length === 5')
                ->assertQueryStringHas('category', '0')
                ->click('.p-treenode-label')
                ->pause(500)
                ->assertQueryStringHas('category', $category->id)
                ->assertSee('Shop');
        });
    }
}
