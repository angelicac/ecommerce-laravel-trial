<?php

namespace Tests\Browser;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;
    public function test_customer_login()
    {
        $this->browse(function (Browser $browser) {
            $user = new User([
                'first_name' => 'Customer',
                'last_name' => 'Customer',
                'email' => 'customer@test.com',
                'password' => Hash::make('customer'),
                'user_type' => UserType::CUSTOMER,
            ]);

            $user->save();

            $browser->logout()
                ->visit('/login')
                ->type('email', $user->email)
                ->type('password', 'customer')
                ->press('Login')
                ->assertPathIs('/');
        });
    }

    public function test_customer_login_fail_as_admin()
    {
        $this->browse(function (Browser $browser) {
            $browser->logout()
                ->visit('/login')
                ->type('email', 'admin@admin.com')
                ->type('password', 'password')
                ->press('Login')
                ->assertPathIs('/login');
        });
    }
    public function test_admin_login()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/login')
                ->type('email', 'admin@admin.com')
                ->type('password', 'password')
                ->press('Login')
                ->assertPathIs('/admin');
        });
    }

    public function test_admin_login_fail_as_customer()
    {
        $this->browse(function (Browser $browser) {
            $user = new User([
                'first_name' => 'Customer',
                'last_name' => 'Customer',
                'email' => 'customer@test.com',
                'password' => Hash::make('customer'),
                'user_type' => UserType::CUSTOMER,
            ]);

            $user->save();

            $browser->logout()
                ->visit('/admin/login')
                ->type('email', $user->email)
                ->type('password', 'customer')
                ->press('Login')
                ->assertPathIs('/admin/login');
        });
    }
}
