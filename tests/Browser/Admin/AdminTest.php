<?php

namespace Tests\Browser\Admin;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class AdminTest extends DuskTestCase
{
    use DatabaseMigrations;
    public function testDashboard()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin')
                ->assertSee('Dashboard');
        });
    }
}
