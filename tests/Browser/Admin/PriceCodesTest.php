<?php

namespace Tests\Browser\Admin;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class PriceCodesTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function test_price_codes()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/price-codes')
                ->assertScript('typeof window.vm', 'object')
                ->assertSee('Price Codes')
                ->waitUntil('window.vm.loading === false')
                ->assertSeeIn('.p-column-title', 'Name');
        });
    }

    public function test_price_codes_show()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/price-codes/show')
                ->assertSee('New Price Code')
                ->assertScript('typeof window.vm', 'object');
        });
    }

    public function test_price_codes_store()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/price-codes/show')
                ->type('name', 'Price Code')
                ->pressAndWaitFor('Submit')
                ->assertPathIs('/admin/price-codes/1');
        });
    }
}
