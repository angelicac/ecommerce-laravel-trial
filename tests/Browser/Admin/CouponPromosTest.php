<?php

namespace Tests\Browser\Admin;

use App\Models\User;
use App\Models\UserType;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class CouponPromosTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function test_coupon_promos()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/coupon-promos')
                ->assertScript('typeof window.vm', 'object')
                ->assertSee('Coupon Promos')
                ->waitUntil('window.vm.loading === false')
                ->assertSeeIn('.p-column-title', 'Code');
        });
    }

    public function test_coupon_promos_show()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/coupon-promos/show')
                ->assertSee('New Coupon Promo')
                ->assertScript('typeof window.vm', 'object');
        });
    }

    public function test_coupon_promos_store()
    {
        $this->browse(function (Browser $browser) {
            $start_date = new Carbon('2022-01-01');
            $end_date = new Carbon('2023-01-01');
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/coupon-promos/show')
                ->type('code', 'CODE')
                ->select('type', '1')

                // Setting input[type="datetime"] values
                ->type('#start_date', $start_date->format(config('app.testing.dusk.date_format')))
                ->type('#end_date', $end_date->format(config('app.testing.dusk.date_format')))

                ->assertScript('window.vm.entity.code', 'CODE')
                ->pressAndWaitFor('Submit')
                ->assertPathIs('/admin/coupon-promos/1');
        });
    }
}
