<?php

namespace Tests\Browser\Admin;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class CategoriesTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function test_categories()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/categories')
                ->assertScript('typeof window.vm', 'object')
                ->assertSee('Categories')
                ->waitUntil('window.vm.loading === false')
                ->assertSeeIn('.p-column-title', 'Name');
        });
    }

    public function test_categories_show()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/categories/show')
                ->assertSee('New Category')
                ->assertScript('typeof window.vm', 'object');
        });
    }

    public function test_categories_store()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/categories/show')
                ->type('name', 'New Test Category')
                ->pressAndWaitFor('Submit')
                ->assertPathIs('/admin/categories/1');
        });
    }
}
