<?php

namespace Tests\Browser\Admin;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Item;
use App\Models\ItemVariant;
use App\Models\User;
use App\Models\UserType;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ItemsTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function test_items()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/items')
                ->assertScript('typeof window.vm', 'object')
                ->assertSee('Items')
                ->waitUntil('window.vm.loading === false')
                ->assertSeeIn('.p-column-title', 'Name');
        });
    }

    public function test_items_show()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/items/show')
                ->assertSee('New Item')
                ->assertScript('typeof window.vm', 'object');
        });
    }

    public function test_items_store()
    {
        $this->browse(function (Browser $browser) {
            Brand::factory()->create();
            Category::factory()->create();
            $brand = Brand::first();
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/items/show')

                ->type('name', 'Test Item')

                ->click('#brand_id')
                ->waitForText($brand->name)
                ->click('.p-dropdown-item')

                ->type('description', 'Test description')

                ->type('#new_meta_input', 'size')
                ->keys('#new_meta_input', '{enter}')

                ->type('#sku-input-0', 'sku-1')

                ->type('#price-input-0', 10)


                ->type('#stock-input-0', 100)

                ->type('#weight-input-0', 1)

                ->type('#size-input-0', 'big')

                ->scrollToBottom()
                ->pressAndWaitFor('Submit')
                ->assertPathIs('/admin/items/1');
            $item = Item::first();
            $this->assertEquals('Test Item', $item->name);
            $this->assertEquals($brand->id, $item->brand_id);
            $this->assertEquals('Test description', $item->description);

            $variant = ItemVariant::whereSku('sku-1')->first();
            $this->assertNotNull($variant);
            $this->assertEquals(10, $variant->price);
            $this->assertEquals(100, $variant->stock);
            $this->assertEquals('big', $variant->meta->first()->value);
        });
    }
}
