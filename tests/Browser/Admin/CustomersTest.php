<?php

namespace Tests\Browser\Admin;

use App\Models\Brand;
use App\Models\Item;
use App\Models\PriceCode;
use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class CustomersTest extends DuskTestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $user = new User();
        $user->first_name = 'John';
        $user->last_name = 'Smith';
        $user->email = 'john.smith@test.com';
        $user->password = Hash::make('customer');
        $user->user_type = UserType::CUSTOMER;
        $user->save();

        $this->user = $user;

        $this->post('/login', [
            'email' => $user->email,
            'password' => 'customer'
        ]);
    }

    public function test_customers()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/customers')
                ->assertScript('typeof window.vm', 'object')
                ->waitUntil('window.vm.loading === false')
                ->assertSeeIn('.p-column-title', 'Name')
                ->assertSee('john.smith@test.com')
                ->assertDontSee('admin@admin.com');
        });
    }

    public function test_customers_show()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/customers/2')
                ->waitUntil('window.vm.loading === false')
                ->assertSee('John')
                ->assertSee('Smith')
                ->assertSee('john.smith@test.com');
        });
    }

    public function test_customers_update()
    {
        Brand::factory()->create();
        $item = new Item(['name' => 'Test Item', 'description' => 'a', 'brand_id' => Brand::first()->id]);
        $item->save();
        $item->variants()->create(['sku' => 'sku-1', 'price' => 10, 'weight' => 1]);
        $price_code = new PriceCode([
            'name' => 'New Price Code',
        ]);
        $price_code->save();
        $price_code->items()->attach(['sku-1' => ['price' => 50]]);
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/customers/2')
                ->waitUntil('window.vm.loading === false')

                ->click('#price_code_id')
                ->waitForText('New Price Code')
                ->click('.p-dropdown-item')
                ->pressAndWaitFor('Save')
                ->assertSee('Saved!');
            $user = User::find(2);
            $this->assertEquals(1, $user->price_code_id);
        });
    }
}
