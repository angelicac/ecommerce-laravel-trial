<?php

namespace Tests\Browser\Admin;

use App\Models\ShippingFee;
use App\Models\ShippingLocation;
use App\Models\User;
use App\Models\UserType;
use App\Settings\SiteSettings;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ShippingTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function test_shipping_locations()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/shipping')
                ->assertScript('typeof window.vm', 'object')
                ->assertSee('Shipping')
                ->waitUntil('window.vm.loading === false')
                ->assertSeeIn('.p-column-title', 'Name');
        });
    }

    public function test_delete_existing()
    {
        $location = new ShippingLocation(['name' => 'ILOCOS', 'region_id' => '01']);
        $location->save();
        $location->fees()->create([
            'shipping_method_id' => 1,
            'fee' => 1000
        ]);
        $location->fees()->create([
            'shipping_method_id' => 2,
            'minimum_price' => 2000
        ]);

        $this->browse(function (Browser $browser) use ($location) {
            $this->assertEquals(2, $location->fees()->count());
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/shipping/' . $location->id)
                ->waitForText('Minimum Price:')
                ->click('#delete-fee-1')
                ->pressAndWaitFor('Submit');
            $location->refresh();
            $this->assertEquals(1, $location->fees()->count());
        });
    }
}
