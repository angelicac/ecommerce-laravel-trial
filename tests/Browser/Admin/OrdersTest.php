<?php

namespace Tests\Browser\Admin;

use App\Models\Order;
use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class OrdersTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function test_orders()
    {
        Order::factory()->create();

        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/orders')
                ->assertScript('typeof window.vm', 'object')
                ->assertSee('Orders')
                ->waitUntil('window.vm.loading === false')
                ->assertSee('0000000001')
                ->assertSeeIn('.p-column-title', 'Number');
        });
    }

    public function test_orders_update()
    {
        Order::factory()->create();
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/orders/1')
                ->waitUntil('window.vm.loading === false')
                ->waitUntil('window.vm.orderStatusOptions.length === 5')
                ->select('order_status', 4)
                ->assertScript('window.vm.entity.order_status', 4)
                ->screenshot('orders-before')
                ->pressAndWaitFor('Update')
                ->screenshot('orders');
            $order = Order::first();
            $this->assertEquals(4, $order->order_status);
        });
    }

    public function test_download_invoice_link()
    {
        Order::factory()->create();
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/orders/1')
                ->waitUntil('window.vm.loading === false')
                ->assertSeeLink('Download Invoice');
            $order = Order::first();
            $this->assertEquals(route('invoice', [$order->id]), $browser->attribute('#invoice_download', 'href'));
        });
    }
}
