<?php

namespace Tests\Browser\Admin;

use App\Models\Brand;
use App\Models\Item;
use App\Models\User;
use App\Models\UserType;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class OverallPromotionsTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function test_overall_promotions()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/overall-promotions')
                ->assertScript('typeof window.vm', 'object')
                ->assertSee('Overall Promotions')
                ->waitUntil('window.vm.loading === false')
                ->assertSeeIn('.p-column-title', 'Name');
        });
    }

    public function test_overall_promotions_show()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/overall-promotions/show')
                ->assertSee('New Overall Promotion')
                ->assertScript('typeof window.vm', 'object');
        });
    }

    public function test_overall_promotions_store()
    {
        $brand = Brand::factory()->create();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);

        $this->browse(function (Browser $browser) {
            $start_date = new Carbon('2022-01-01');
            $end_date = new Carbon('2023-01-01');
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/overall-promotions/show')
                ->click('#sku')
                ->type('> .p-dropdown-panel input[type="text"]', 'T')
                ->waitForText('Test Item')
                ->click('.p-dropdown-item')
                ->click('#sku')
                ->type('price', '50')
                ->type('#start_date', $start_date->format(config('app.testing.dusk.date_format')))
                ->type('#end_date', $end_date->format(config('app.testing.dusk.date_format')))
                ->pressAndWaitFor('Submit')
                ->assertPathIs('/admin/overall-promotions/1');
        });
    }
}
