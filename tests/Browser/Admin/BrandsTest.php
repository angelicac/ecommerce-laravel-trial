<?php

namespace Tests\Browser\Admin;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class BrandsTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function test_brands()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/brands')
                ->assertScript('typeof window.vm', 'object')
                ->assertSee('Brands')
                ->waitUntil('window.vm.loading === false')
                ->assertSeeIn('.p-column-title', 'Name');
        });
    }

    public function test_brands_show()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/brands/show')
                ->assertSee('New Brand')
                ->assertScript('typeof window.vm', 'object');
        });
    }

    public function test_brands_store()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::where('user_type', '=', UserType::ADMIN)->first()->id)
                ->visit('/admin/brands/show')
                ->type('name', 'New Test Brand')
                ->pressAndWaitFor('Submit')
                ->assertPathIs('/admin/brands/1');
        });
    }
}
