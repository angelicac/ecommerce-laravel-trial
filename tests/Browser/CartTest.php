<?php

namespace Tests\Browser;

use App\Models\Brand;
use App\Models\Cart;
use App\Models\CouponPromo;
use App\Models\CouponPromoType;
use App\Models\Item;
use App\Models\Order;
use App\Models\ShippingFee;
use App\Models\ShippingLocation;
use App\Models\ShippingMethod;
use App\Models\User;
use App\Models\UserType;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class CartTest extends DuskTestCase
{
    use DatabaseMigrations;

    protected int $user_id;

    protected function setUp(): void
    {
        parent::setUp();

        $user = new User();
        $user->first_name = 'John';
        $user->last_name = 'Smith';
        $user->email = 'john.smith@test.com';
        $user->contact_number = '09123456789';
        $user->password = Hash::make('customer');
        $user->user_type = UserType::CUSTOMER;
        $user->save();

        $this->user_id = $user->id;
    }

    protected function user(): User
    {
        return User::find($this->user_id);
    }

    public function test_index()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $this->user()->cart->contents()->attach(['sku-1' => ['quantity' => 2]]);

        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user_id)
                ->visit('/cart')
                ->assertScript('typeof window.vm', 'object')
                ->waitUntil('window.vm.cartState.cart.length === 1')
                ->assertSee('Test Item (sku-1)')
                ->assertSee('₱100.00')
                ->assertSee('Total ₱200.00');
            $this->assertEquals(1, Cart::find(1)->contents()->count());
        });
    }

    public function test_quantity_buttons()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $this->user()->cart->contents()->attach(['sku-1' => ['quantity' => 2]]);

        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user_id)
                ->visit('/cart')
                ->assertScript('typeof window.vm', 'object')
                ->waitUntil('window.vm.cartState.cart.length === 1')
                ->assertSee('Total ₱200.00')
                ->press('.add-quantity-button')
                ->waitForText('Total ₱300.00')
                ->assertSee('Total ₱300.00')
                ->pressAndWaitFor('.sub-quantity-button')
                ->pressAndWaitFor('.sub-quantity-button')
                ->waitForText('Total ₱100.00')
                ->assertSee('Total ₱100.00');
            $this->assertEquals(1, Cart::find(1)->contents()->count());
            $this->assertEquals(1, Cart::find(1)->contents[0]->pivot->quantity);
        });
    }

    public function test_coupon_input()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $this->user()->cart->contents()->attach(['sku-1' => ['quantity' => 2]]);

        $promo = new CouponPromo([
            'code' => 'CODE',
            'type' => CouponPromoType::REDUCTION,
            'start_date' => '2022-01-01',
            'end_date' => '2023-01-01',
            'rate_threshold' => '1000',
            'rate_reduction' => '500'
        ]);
        $promo->save();
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user_id)
                ->visit('/cart')
                ->assertScript('typeof window.vm', 'object')
                ->waitForText('Apply Coupon')
                ->type('#coupon_code', 'CODE')
                ->press('#apply_coupon_button')
                ->waitForText('Coupon Applied!')
                ->assertSee('Coupon Applied!')
                ->assertSee('You save 500.00 on spending 1000.00');
        });
    }

    public function test_no_items()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user_id)
                ->visit('/cart')
                ->assertSee('You have no items in your cart right now');
        });
    }

    public function test_checkout_link()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $this->user()->cart->contents()->attach(['sku-1' => ['quantity' => 2]]);

        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user_id)
                ->visit('/cart')
                ->assertScript('typeof window.vm', 'object')
                ->waitUntil('window.vm.cartState.cart.length === 1')
                ->clickLink('Checkout')
                ->assertPathIs('/cart/checkout');
        });
    }

    public function test_checkout()
    {
        ShippingFee::query()->delete();
        $location = new ShippingLocation(['name' => 'ILOCOS', 'region_id' => '01']);
        $location->save();
        $location->fees()->create([
            'shipping_method_id' => ShippingMethod::FLAT_RATE,
            'fee' => '50',
        ]);
        $location->refresh();
        $fee = $location->fees()->first();
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 100, 'weight' => 1]);
        $this->user()->cart->contents()->attach(['sku-1' => ['quantity' => 2]]);
        $this->user()->addresses()->create([
            'name' => 'Home Address',
            'barangay_id' => '012901001',
            'city_id' => '012901',
            'province_id' => '0129',
            'region_id' => '01',
            'street' => '1st Street',
            'zip_code' => '1'
        ]);

        $this->browse(function (Browser $browser) use ($fee) {
            $order_number = Order::get_next_order_number();
            $browser->loginAs($this->user_id)
                ->visit('/cart/checkout')
                ->assertScript('typeof window.vm', 'object')
                ->waitForText('ILOCOS REGION')
                ->click('#address-button-0')
                ->waitUntilMissingText('Select option')
                ->waitForText('Flat Rate')
                ->scrollToBottom()
                ->pause(100)
                ->radio('shipping_method', $fee->id)
                ->waitForText('₱250.00')
                ->assertSee('₱250.00')
                ->screenshot('checkout')
                ->press('Checkout')
                ->waitForRoute('order.track', [$order_number])
                ->assertPathIs('/order/track/' . $order_number);
        });
    }

    public function test_cart_reduce_quantity_from_low_stock()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 5, 'weight' => 1]);
        $this->user()->cart->contents()->attach(['sku-1' => ['quantity' => 10]]);

        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user_id)
                ->visit('/cart')
                ->assertScript('typeof window.vm', 'object')
                ->waitUntil('window.vm.cartState.cart.length === 1')
                ->assertSee('Test Item (sku-1)')
                ->assertSee('Quantity has been reduced from 10 to 5 due to low stock.');
            $this->assertEquals(1, Cart::find(1)->contents()->count());
        });
    }

    public function test_cart_remove_from_low_stock()
    {
        $brand = new Brand(['name' => 'Test Brand']);
        $brand->save();
        $test_item = new Item(['name' => 'Test Item', 'description' => 'test', 'brand_id' => $brand->id]);
        $test_item->save();
        $test_item->variants()->create(['sku' => 'sku-1', 'price' => 100, 'stock' => 0, 'weight' => 1]);
        $this->user()->cart->contents()->attach(['sku-1' => ['quantity' => 10]]);

        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user_id)
                ->visit('/cart')
                ->assertScript('typeof window.vm', 'object')
                ->waitUntil('window.vm.cartState.cart.length === 1')
                ->assertSee('Test Item (sku-1)')
                ->assertSee('Out of stock');
            $this->assertEquals(1, Cart::find(1)->contents()->count());
        });
    }
}
